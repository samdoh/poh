-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2018 at 06:11 WB
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oh`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `calcAmount` ()  BEGIN
    DECLARE v_id INT;
	DECLARE v_amount DOUBLE;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cur CURSOR FOR SELECT BLL_ID, AMOUNT FROM 
							(SELECT BLL_ID, BLL_AMOUNT, SUM(BLI_ITEM_AMOUNT * BLI_QTY) AS AMOUNT
							FROM BILLS JOIN BILLITEMS ON BLL_ID = BLI_ID_BILL
							WHERE BLI_ITEM_AMOUNT > 0
							GROUP BY BLL_ID
							ORDER BY BLL_ID) BILLS
							WHERE BLL_AMOUNT <> AMOUNT;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO v_id, v_amount;
        IF done THEN
            LEAVE read_loop;
        END IF;
        UPDATE BILLS SET BLL_AMOUNT = v_amount WHERE BLL_ID = v_id;
    END LOOP;
  CLOSE cur;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ADMISSION`
--

CREATE TABLE `ADMISSION` (
  `ADM_ID` int(11) NOT NULL,
  `ADM_IN` int(11) NOT NULL DEFAULT '0',
  `ADM_TYPE` char(1) NOT NULL DEFAULT 'N',
  `ADM_WRD_ID_A` char(1) NOT NULL DEFAULT '',
  `ADM_YPROG` int(11) NOT NULL DEFAULT '0',
  `ADM_PAT_ID` int(11) NOT NULL DEFAULT '0',
  `ADM_DATE_ADM` datetime NOT NULL,
  `ADM_ADMT_ID_A_ADM` varchar(10) NOT NULL DEFAULT '',
  `ADM_FHU` varchar(50) DEFAULT NULL,
  `ADM_IN_DIS_ID_A` varchar(10) DEFAULT NULL,
  `ADM_OUT_DIS_ID_A` varchar(10) DEFAULT NULL,
  `ADM_OUT_DIS_ID_A_2` varchar(10) DEFAULT NULL,
  `ADM_OUT_DIS_ID_A_3` varchar(10) DEFAULT NULL,
  `ADM_OPE_ID_A` varchar(10) DEFAULT NULL,
  `ADM_DATE_OP` datetime DEFAULT NULL,
  `ADM_RESOP` varchar(10) DEFAULT NULL,
  `ADM_DATE_DIS` datetime DEFAULT NULL,
  `ADM_DIST_ID_A` varchar(10) DEFAULT NULL,
  `ADM_NOTE` text,
  `ADM_TRANS` float DEFAULT '0',
  `ADM_PRG_DATE_VIS` datetime DEFAULT NULL,
  `ADM_PRG_PTT_ID_A` varchar(10) DEFAULT NULL,
  `ADM_PRG_DATE_DEL` datetime DEFAULT NULL,
  `ADM_PRG_DLT_ID_A` char(1) DEFAULT NULL,
  `ADM_PRG_DRT_ID_A` char(1) DEFAULT NULL,
  `ADM_PRG_WEIGHT` float DEFAULT NULL,
  `ADM_PRG_DATE_CTRL1` datetime DEFAULT NULL,
  `ADM_PRG_DATE_CTRL2` datetime DEFAULT NULL,
  `ADM_PRG_DATE_ABORT` datetime DEFAULT NULL,
  `ADM_USR_ID_A` varchar(50) NOT NULL DEFAULT 'admin',
  `ADM_LOCK` int(11) NOT NULL DEFAULT '0',
  `ADM_DELETED` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ADMISSIONTYPE`
--

CREATE TABLE `ADMISSIONTYPE` (
  `ADMT_ID_A` varchar(10) NOT NULL,
  `ADMT_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ADMISSIONTYPE`
--

INSERT INTO `ADMISSIONTYPE` (`ADMT_ID_A`, `ADMT_DESC`) VALUES
('A', 'AMBULANCE\r'),
('I', 'SELF\r'),
('R', 'REFERRAL\r');

-- --------------------------------------------------------

--
-- Table structure for table `AGETYPE`
--

CREATE TABLE `AGETYPE` (
  `AT_CODE` varchar(4) NOT NULL DEFAULT '',
  `AT_FROM` int(11) NOT NULL DEFAULT '0',
  `AT_TO` int(11) NOT NULL DEFAULT '0',
  `AT_DESC` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `AGETYPE`
--

INSERT INTO `AGETYPE` (`AT_CODE`, `AT_FROM`, `AT_TO`, `AT_DESC`) VALUES
('d0', 0, 0, 'angal.agetype.newborn'),
('d1', 1, 5, 'angal.agetype.earlychildhood'),
('d2', 6, 12, 'angal.agetype.latechildhood'),
('d3', 13, 24, 'angal.agetype.adolescents'),
('d4', 25, 59, 'angal.agetype.adult'),
('d5', 60, 99, 'angal.agetype.elderly');

-- --------------------------------------------------------

--
-- Table structure for table `BILLITEMS`
--

CREATE TABLE `BILLITEMS` (
  `BLI_ID` int(11) NOT NULL,
  `BLI_ID_BILL` int(11) DEFAULT NULL,
  `BLI_IS_PRICE` tinyint(1) NOT NULL,
  `BLI_ID_PRICE` varchar(10) DEFAULT NULL,
  `BLI_ITEM_DESC` varchar(100) DEFAULT NULL,
  `BLI_ITEM_AMOUNT` double NOT NULL,
  `BLI_QTY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `BILLPAYMENTS`
--

CREATE TABLE `BILLPAYMENTS` (
  `BLP_ID` int(11) NOT NULL,
  `BLP_ID_BILL` int(11) DEFAULT NULL,
  `BLP_DATE` datetime NOT NULL,
  `BLP_AMOUNT` double NOT NULL,
  `BLP_USR_ID_A` varchar(50) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `BILLS`
--

CREATE TABLE `BILLS` (
  `BLL_ID` int(11) NOT NULL,
  `BLL_DATE` datetime NOT NULL,
  `BLL_UPDATE` datetime NOT NULL,
  `BLL_IS_LST` tinyint(1) NOT NULL,
  `BLL_ID_LST` int(11) DEFAULT NULL,
  `BLL_LST_NAME` varchar(50) DEFAULT NULL,
  `BLL_IS_PAT` tinyint(1) NOT NULL,
  `BLL_ID_PAT` int(11) DEFAULT NULL,
  `BLL_PAT_NAME` varchar(100) DEFAULT NULL,
  `BLL_STATUS` varchar(1) DEFAULT NULL,
  `BLL_AMOUNT` double DEFAULT NULL,
  `BLL_BALANCE` double DEFAULT NULL,
  `BLL_USR_ID_A` varchar(50) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERYRESULTTYPE`
--

CREATE TABLE `DELIVERYRESULTTYPE` (
  `DRT_ID_A` char(1) NOT NULL,
  `DRT_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DELIVERYRESULTTYPE`
--

INSERT INTO `DELIVERYRESULTTYPE` (`DRT_ID_A`, `DRT_DESC`) VALUES
('A', 'LIVE BIRTH\r'),
('B', 'MACERATED STILLBIRTH\r'),
('M', 'MATERNAL DEATH\r'),
('N', 'NEWBORN DEATH\r'),
('S', 'FRESH STILL BIRTH\r');

-- --------------------------------------------------------

--
-- Table structure for table `DELIVERYTYPE`
--

CREATE TABLE `DELIVERYTYPE` (
  `DLT_ID_A` char(1) NOT NULL,
  `DLT_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DELIVERYTYPE`
--

INSERT INTO `DELIVERYTYPE` (`DLT_ID_A`, `DLT_DESC`) VALUES
('C', 'DELIVERY ASSISTED BY CESARIAN SECTION\r'),
('N', 'NORMAL DELIVERY\r'),
('V', 'DELIVERY ASSISTED BY VACUUM EXTRACTION\r');

-- --------------------------------------------------------

--
-- Table structure for table `DICOM`
--

CREATE TABLE `DICOM` (
  `DM_PAT_ID` int(11) NOT NULL,
  `DM_DATA` longblob,
  `DM_FILE_ID` bigint(20) NOT NULL,
  `DM_FILE_NOME` varchar(255) NOT NULL,
  `DM_FILE_ACCESSION_NUMBER` varchar(255) DEFAULT NULL,
  `DM_FILE_INSTITUTION_NAME` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_UID` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_NAME` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_ADDR` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_AGE` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_SEX` varchar(255) DEFAULT NULL,
  `DM_FILE_PAT_BIRTHDATE` varchar(255) DEFAULT NULL,
  `DM_FILE_ST_UID` varchar(255) NOT NULL,
  `DM_FILE_ST_DATE` varchar(255) DEFAULT NULL,
  `DM_FILE_ST_DESCR` varchar(255) DEFAULT NULL,
  `DM_FILE_SER_UID` varchar(255) NOT NULL,
  `DM_FILE_SER_INST_UID` varchar(255) NOT NULL,
  `DM_FILE_SER_NUMBER` varchar(255) DEFAULT NULL,
  `DM_FILE_SER_DESC_COD_SEQ` varchar(255) DEFAULT NULL,
  `DM_FILE_SER_DATE` varchar(255) DEFAULT NULL,
  `DM_FILE_SER_DESC` varchar(255) DEFAULT NULL,
  `DM_FILE_INST_UID` varchar(255) NOT NULL,
  `DM_FILE_MODALIITY` varchar(255) DEFAULT NULL,
  `DM_THUMBNAIL` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `DISCHARGETYPE`
--

CREATE TABLE `DISCHARGETYPE` (
  `DIST_ID_A` varchar(10) NOT NULL,
  `DIST_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DISCHARGETYPE`
--

INSERT INTO `DISCHARGETYPE` (`DIST_ID_A`, `DIST_DESC`) VALUES
('B', 'REFERRED\r'),
('D', 'DEAD\r'),
('EQ', 'NORMAL DISCHARGE\r'),
('ES', 'ESCAPE\r');

-- --------------------------------------------------------

--
-- Table structure for table `DISEASE`
--

CREATE TABLE `DISEASE` (
  `DIS_ID_A` varchar(10) NOT NULL,
  `DIS_DESC` varchar(100) NOT NULL,
  `DIS_DCL_ID_A` char(2) NOT NULL,
  `DIS_LOCK` int(11) NOT NULL DEFAULT '0',
  `DIS_OPD_INCLUDE` int(11) NOT NULL DEFAULT '0',
  `DIS_IPD_IN_INCLUDE` int(11) NOT NULL DEFAULT '0',
  `DIS_IPD_OUT_INCLUDE` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DISEASE`
--

INSERT INTO `DISEASE` (`DIS_ID_A`, `DIS_DESC`, `DIS_DCL_ID_A`, `DIS_LOCK`, `DIS_OPD_INCLUDE`, `DIS_IPD_IN_INCLUDE`, `DIS_IPD_OUT_INCLUDE`) VALUES
('1', 'Acute Flaccid Paralysis', 'ND', 0, 1, 1, 1),
('10', 'Viral Haemorragic Fever', 'ND', 0, 1, 1, 1),
('100', 'Other malignant neoplasm', 'NC', 0, 1, 1, 1),
('101', 'Curable Ulcers', 'NC', 0, 1, 1, 1),
('102', 'Cerebro-vascular event', 'NC', 0, 1, 1, 1),
('103', 'Cardiac arrest', 'NC', 0, 1, 1, 1),
('104', 'Gastro-intestinal bleeding', 'NC', 0, 1, 1, 1),
('105', 'Respiratory distress', 'NC', 0, 1, 1, 1),
('106', 'Acute renal failure', 'NC', 1, 0, 1, 1),
('107', 'Acute sepsis', 'NC', 1, 0, 1, 1),
('108', 'Schistosomiasis', 'NC', 0, 1, 1, 1),
('11', 'Yellow Fever', 'ND', 0, 1, 1, 1),
('110', 'Pelvic Inflammatory DISEASEs (PID)', 'OC', 0, 1, 1, 1),
('111', 'Tetanus (over 28 days age)', 'OC', 1, 1, 1, 1),
('112', 'Obstructed Labour', 'MP', 0, 1, 1, 1),
('113', 'Other types of meningitis', 'OC', 0, 1, 1, 1),
('114', 'Schistosomiasis', 'OC', 0, 1, 1, 1),
('115', 'Sleeping sickness', 'OC', 0, 1, 1, 1),
('116', 'Malaria in pregnancy', 'MP', 0, 1, 1, 1),
('117', 'Injuries - (road traffic accident)', 'NC', 0, 1, 1, 1),
('12', 'Anaemia', 'NC', 0, 1, 1, 1),
('13', 'Dental DISEASE and conditions', 'NC', 0, 1, 1, 1),
('14', 'Diabetes Mellitus', 'NC', 0, 1, 1, 1),
('15', 'Gastro-intestinal DISEASEss (non infective)', 'NC', 1, 1, 1, 1),
('16', 'Hypertension', 'NC', 0, 1, 1, 1),
('17', 'Mental Illness', 'NC', 0, 1, 1, 1),
('18', 'Epilepsy', 'NC', 0, 1, 1, 1),
('19', 'Other cardio-vascular DISEASEs', 'NC', 0, 1, 1, 1),
('2', 'Cholera', 'ND', 0, 1, 1, 1),
('20', 'Severe malnutrition (marasmus,kwashiorkor,marasmic-kwash)', 'NC', 6, 1, 0, 0),
('22', 'Trauma-Domestic Violence', 'NC', 0, 1, 1, 1),
('23', 'Trauma-other intensional', 'NC', 0, 1, 1, 1),
('24', 'Trauma Road traffic accidents', 'NC', 0, 1, 1, 1),
('25', 'Trauma Other Non intensional', 'NC', 0, 1, 1, 1),
('26', 'Other complications of pregnancy', 'MP', 1, 0, 1, 1),
('27', 'Perinatal conditions', 'MP', 0, 1, 1, 1),
('28', 'Abortions', 'MP', 0, 1, 1, 1),
('29', 'AIDS', 'OC', 0, 1, 1, 1),
('3', 'Diarrhoea-Dysentry', 'ND', 2, 1, 1, 1),
('30', 'Diarrhoea-Not bloody', 'OC', 0, 1, 1, 1),
('31', 'Diarrhoea-Persistent', 'OC', 0, 1, 1, 1),
('32', 'Ear Infection', 'OC', 1, 0, 1, 1),
('33', 'Eye Infection', 'OC', 0, 1, 1, 1),
('34', 'Genital Inf.-Urethral discharge', 'OC', 1, 0, 1, 1),
('35', 'Genital Inf.-Vaginal discharge', 'OC', 1, 0, 1, 1),
('36', 'Genital Inf.-Ulcerative', 'OC', 1, 0, 1, 1),
('37', 'Intestinal Worms', 'OC', 0, 1, 1, 1),
('38', 'Leprosy', 'OC', 0, 1, 1, 1),
('39', 'Malaria', 'OC', 0, 1, 1, 1),
('4', 'Guinea Worm', 'ND', 0, 1, 1, 1),
('40', 'No pneumonia-cold or cough', 'OC', 0, 1, 1, 1),
('41', 'Onchocerciasis', 'OC', 0, 1, 1, 1),
('42', 'Pneumonia', 'OC', 0, 1, 1, 1),
('43', 'Skin DISEASEs', 'OC', 0, 1, 1, 1),
('44', 'Tuberculosis', 'OC', 2, 1, 1, 1),
('46', 'Typhoid Fever', 'OC', 0, 1, 1, 1),
('47', 'Urinary Tract Infections (UTI)', 'OC', 2, 1, 1, 1),
('48', 'Others(non specified)', 'OC', 1, 0, 1, 1),
('49', 'All Other DISEASEs', 'AO', 2, 1, 1, 1),
('5', 'Measles', 'ND', 0, 1, 1, 1),
('50', 'Other emerging infectious DISEASE', 'ND', 0, 1, 1, 1),
('56', 'Death in OPD', 'OC', 0, 1, 1, 1),
('57', 'ENT Conditions', 'OC', 0, 1, 1, 1),
('58', 'Eye Conditions', 'OC', 0, 1, 1, 1),
('59', 'Sexually transmited infections (STI)', 'OC', 1, 1, 1, 1),
('6', 'Meningitis', 'ND', 0, 1, 1, 1),
('60', 'Hepatitis', 'OC', 1, 0, 1, 1),
('61', 'Osteomyelitis', 'OC', 1, 0, 1, 1),
('62', 'Peritonitis', 'OC', 1, 0, 1, 1),
('63', 'Pyrexiaof unknown origin (PUO)', 'OC', 1, 0, 1, 1),
('64', 'Septicaemia', 'OC', 1, 0, 1, 1),
('65', 'High blood pressure in pregnancy', 'MP', 0, 1, 1, 1),
('66', 'Haemorrhage related to pregnancy (APH/PPH)', 'MP', 0, 1, 1, 1),
('67', 'Sepsis related to pregnancy', 'MP', 1, 0, 1, 1),
('68', 'Asthma', 'NC', 0, 1, 1, 1),
('69', 'Oral DISEASEs and condition', 'NC', 0, 1, 1, 1),
('7', 'Tetanus neonatal (less 28 days age)', 'ND', 3, 1, 1, 1),
('70', 'Endocrine and metabolic disorders (other)', 'NC', 0, 1, 1, 1),
('71', 'Anxiety disorders', 'NC', 0, 1, 1, 1),
('72', 'Mania', 'NC', 0, 1, 1, 1),
('73', 'Depression', 'NC', 0, 1, 1, 1),
('74', 'Schizophrenia', 'NC', 0, 1, 1, 1),
('75', 'Alcohol and drug abuse', 'NC', 0, 1, 1, 1),
('76', 'Childhood and mental disorders', 'NC', 0, 1, 1, 1),
('77', 'Severe malnutrition (kwashiorkor)', 'NC', 2, 0, 1, 1),
('78', 'Severe malnutrition (marasmus)', 'NC', 3, 0, 1, 1),
('79', 'Severe malnutrition (marasmic-kwash)', 'NC', 2, 0, 1, 1),
('8', 'Plague', 'ND', 0, 1, 1, 1),
('80', 'Low weight for age', 'NC', 0, 1, 1, 1),
('81', 'Injuries - (trauma due to other causes)', 'NC', 0, 1, 1, 1),
('82', 'Animal/snake bite', 'NC', 0, 1, 1, 1),
('83', 'Poisoning', 'NC', 1, 0, 1, 1),
('84', 'Liver cirrhosis', 'NC', 0, 1, 1, 1),
('85', 'Hepatocellular carcinoma', 'NC', 0, 1, 1, 1),
('86', 'Liver DISEASE s (other)', 'NC', 0, 1, 1, 1),
('87', 'Hernias', 'NC', 0, 1, 1, 1),
('88', 'DISEASEs of the appendix', 'NC', 0, 1, 1, 1),
('89', 'Musculo skeletal and connective tissue DISEASE', 'NC', 0, 1, 1, 1),
('9', 'Rabies', 'ND', 0, 1, 1, 1),
('90', 'Genito urinary system DISEASEs ( non infective )', 'NC', 0, 1, 1, 1),
('91', 'Congenital malformations and chromosome abnormalities', 'ND', 1, 0, 1, 1),
('92', 'Complication and surgical care', 'NC', 0, 1, 1, 1),
('93', 'Benine neoplasm''s ( all type )', 'NC', 16, 1, 1, 1),
('94', 'Cancer of the breast', 'NC', 0, 1, 1, 1),
('95', 'Cancer of the prostate', 'ND', 1, 0, 1, 1),
('96', 'Malignant neoplasm of the digestive organs', 'NC', 0, 1, 1, 1),
('97', 'Malignant of the lungs', 'NC', 0, 1, 1, 1),
('98', 'Kaposis and other skin cancers', 'NC', 0, 1, 1, 1),
('99', 'Malignant neoplasm of Hemopoietic tissue', 'NC', 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `DISEASETYPE`
--

CREATE TABLE `DISEASETYPE` (
  `DCL_ID_A` char(2) NOT NULL,
  `DCL_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DISEASETYPE`
--

INSERT INTO `DISEASETYPE` (`DCL_ID_A`, `DCL_DESC`) VALUES
('AO', '5.All Other'),
('MP', '3.MATERNAL AND PERINATAL DISEASES\r'),
('NC', '4.NON-COMMUNICABLE DISEASES\r'),
('ND', '1.NOTIFIABLE DISEASES\r'),
('OC', '2.OTHER INFECTIOUS/COMMUNICABLE DISEASES\r');

-- --------------------------------------------------------

--
-- Table structure for table `EXAM`
--

CREATE TABLE `EXAM` (
  `EXA_ID_A` varchar(10) NOT NULL,
  `EXA_DESC` varchar(100) NOT NULL,
  `EXA_EXC_ID_A` char(2) NOT NULL,
  `EXA_PROC` int(11) NOT NULL,
  `EXA_DEFAULT` varchar(50) DEFAULT NULL,
  `EXA_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EXAM`
--

INSERT INTO `EXAM` (`EXA_ID_A`, `EXA_DESC`, `EXA_EXC_ID_A`, `EXA_PROC`, `EXA_DEFAULT`, `EXA_LOCK`) VALUES
('01.01', '1.1 HB', 'HB', 1, '>=12 (NORMAL)', 1),
('01.02', '1.2 WBC Count', 'HB', 1, '4000 - 7000 (NORMAL)', 1),
('01.03', '1.3 Differential ', 'HB', 1, '', 0),
('01.04', '1.4 Film Comment', 'HB', 1, '', 0),
('01.05', '1.5 ESR', 'HB', 1, 'NORMAL', 1),
('01.06', '1.6 Sickling Test', 'HB', 1, 'NEGATIVE', 1),
('02.01', '2.1 Grouping', 'BT', 1, '', 2),
('02.02', '2.2 Comb''s Test', 'BT', 1, 'NEGATIVE', 3),
('03.01', '3.1 Blood Slide (Malaria)', 'PA', 1, 'NEGATIVE', 1),
('03.02', '3.2 Blood Slide (OTHERS, E.G. TRIUPHANOSOMIAS, MICRIFILARIA, LEISHMANIA, BORRELIA)', 'PA', 1, 'NEGATIVE', 2),
('03.02.1', '3.21 Trypanosomiasis', 'PA', 1, 'NEGATIVE', 2),
('03.02.2', '3.22 MICROFILARIA', 'PA', 1, 'NEGATIVE', 1),
('03.02.3', '3.23 LEISHMANIA', 'PA', 1, 'NEGATIVE', 1),
('03.02.4', '3.24 BORRELIA', 'PA', 1, 'NEGATIVE', 1),
('03.03', '3.3 STOOL MICROSCOPY', 'PA', 2, '', 2),
('03.04', '3.4 URINE MICROSCOPY', 'PA', 1, 'NEGATIVE', 1),
('03.05', '3.5 TISSUE MICROSCOPY', 'PA', 1, 'NEGATIVE', 1),
('03.06', '3.6 CSF WET PREP', 'PA', 1, 'NEGATIVE', 1),
('04.01', '4.1 CULTURE AND SENSITIVITY (C&S) FOR HAEMOPHILUS INFUENZA TYPE B', 'BA', 1, 'NEGATIVE', 2),
('04.02', '4.2 C&S FOR SALMONELA TYPHI', 'BA', 1, 'NEGATIVE', 1),
('04.03', '4.3 C&S FOR VIBRO CHOLERA', 'BA', 1, 'NEGATIVE', 1),
('04.04', '4.4 C&S FOR SHIGELLA DYSENTERIAE', 'BA', 1, 'NEGATIVE', 1),
('04.05', '4.5 C&S FOR NEISSERIA MENINGITIDES', 'BA', 1, 'NEGATIVE', 1),
('04.06', '4.6 OTHER C&S ', 'BA', 1, 'NEGATIVE', 1),
('05.01', '5.1 WET PREP', 'MC', 1, 'NEGATIVE', 1),
('05.02', '5.2 GRAM STAIN', 'MC', 1, 'NEGATIVE', 1),
('05.03', '5.3 INDIA INK', 'MC', 1, 'NEGATIVE', 1),
('05.04', '5.4 LEISMANIA', 'MC', 1, 'NEGATIVE', 1),
('05.05', '5.5 ZN', 'MC', 1, 'NEGATIVE', 1),
('05.06', '5.6 WAYSON', 'MC', 1, 'NEGATIVE', 1),
('05.07', '5.7 PAP SMEAR', 'MC', 1, 'NEGATIVE', 1),
('06.01', '6.1 VDRL/RPR', 'SE', 1, 'NEGATIVE', 1),
('06.02', '6.2 TPHA', 'SE', 1, 'NEGATIVE', 1),
('06.03', '6.3 HIV', 'SE', 1, 'NEGATIVE', 1),
('06.04', '6.4 HEPATITIS', 'SE', 1, 'NEGATIVE', 1),
('06.05', '6.5 OTHERS E.G BRUCELLA, RHEUMATOID FACTOR, WEIL FELIX', 'SE', 1, 'NEGATIVE', 1),
('06.06', '6.6 PREGANCY TEST', 'SE', 1, 'NEGATIVE', 1),
('07.01', '7.1 PROTEIN', 'CH', 1, 'NEGATIVE', 1),
('07.02', '7.2 SUGAR', 'CH', 1, 'NORMAL', 1),
('07.03', '7.3 LFTS', 'CH', 1, 'NORMAL', 1),
('07.03.1', '7.3.1 BILIRUBIN TOTAL', 'CH', 1, '< 1.2 (NORMAL)', 3),
('07.03.2', '7.3.2 BILIRUBIN DIRECT', 'CH', 1, '< 1.2 (NORMAL)', 7),
('07.03.3', '7.3.3 GOT', 'CH', 1, '<= 50 (NORMAL)', 2),
('07.03.4', '7.3.4 ALT/GPT', 'CH', 1, '<= 50 (NORMAL)', 1),
('07.04', '7.4 RFTS', 'CH', 1, 'NORMAL', 1),
('07.04.1', '7.4.1 CREATININA', 'CH', 1, '< 1.4 (NORMAL)', 1),
('07.04.2', '7.4.2 UREA', 'CH', 1, '10-55 (NORMAL)', 1),
('08.01', '8.1 OCCULT BLOOD', 'OC', 1, 'NEGATIVE', 1),
('09.01', '9.1 URINALYSIS', 'OT', 2, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `EXAMROW`
--

CREATE TABLE `EXAMROW` (
  `EXR_ID` int(11) NOT NULL,
  `EXR_EXA_ID_A` varchar(10) NOT NULL,
  `EXR_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EXAMROW`
--

INSERT INTO `EXAMROW` (`EXR_ID`, `EXR_EXA_ID_A`, `EXR_DESC`) VALUES
(1, '01.01', '6 - 12\r'),
(2, '01.01', '< 6\r'),
(3, '01.01', '>=12 (NORMAL)\r'),
(4, '01.02', '4000 - 7000 (NORMAL)\r'),
(5, '01.02', '> 7000 (HIGH)\r'),
(6, '01.02', '< 4000 (LOW)\r'),
(7, '01.05', 'HIGH\r'),
(8, '01.05', 'NORMAL\r'),
(9, '01.05', 'LOW\r'),
(10, '01.06', 'POSITIVE\r'),
(11, '01.06', 'NEGATIVE\r'),
(12, '02.01', 'A RH+\r'),
(13, '02.01', 'A RH-\r'),
(14, '02.01', 'B RH+\r'),
(15, '02.01', 'B RH-\r'),
(16, '02.01', 'AB RH+\r'),
(17, '02.01', 'AB RH-\r'),
(18, '02.01', 'O RH+\r'),
(19, '02.01', 'O RH-\r'),
(20, '02.02', 'NEGATIVE\r'),
(21, '02.02', 'POSITIVE\r'),
(22, '03.01', 'NEGATIVE\r'),
(23, '03.01', 'FEW\r'),
(24, '03.01', '+\r'),
(25, '03.01', '++\r'),
(26, '03.01', '+++\r'),
(27, '03.01', '++++\r'),
(28, '03.02', 'POSITIVE\r'),
(29, '03.02', 'NEGATIVE\r'),
(30, '03.02.1', 'NEGATIVE\r'),
(31, '03.02.1', 'POSITIVE\r'),
(32, '03.02.2', 'POSITIVE\r'),
(33, '03.02.2', 'NEGATIVE\r'),
(34, '03.02.3', 'NEGATIVE\r'),
(35, '03.02.3', 'POSITIVE\r'),
(36, '03.02.4', 'NEGATIVE\r'),
(37, '03.02.4', 'POSITIVE\r'),
(38, '03.03', 'A..LUMBRICOIDES\r'),
(39, '03.03', 'E.COLI CYSTS\r'),
(40, '03.03', 'E.HISTOLYTICA\r'),
(41, '03.03', 'E.VERMICULARIS\r'),
(42, '03.03', 'G.LAMBLIA\r'),
(43, '03.03', 'T.HOMINIS\r'),
(44, '03.03', 'HOOK WORM\r'),
(45, '03.03', 'S.MANSONI\r'),
(46, '03.03', 'S.STERCORALIS\r'),
(47, '03.03', 'TAENIA SAGINATA\r'),
(48, '03.03', 'TAENIA SOLIUM\r'),
(49, '03.03', 'TRICHURISI TRICHURA\r'),
(50, '03.03', 'HYMENOLEPIS NANA\r'),
(51, '03.04', 'POSITIVE\r'),
(52, '03.04', 'NEGATIVE\r'),
(53, '03.05', 'POSITIVE\r'),
(54, '03.05', 'NEGATIVE\r'),
(55, '03.06', 'POSITIVE\r'),
(56, '03.06', 'NEGATIVE\r'),
(57, '04.01', 'POSITIVE\r'),
(58, '04.01', 'NEGATIVE\r'),
(59, '04.02', 'POSITIVE\r'),
(60, '04.02', 'NEGATIVE\r'),
(61, '04.03', 'POSITIVE\r'),
(62, '04.03', 'NEGATIVE\r'),
(63, '04.04', 'POSITIVE\r'),
(64, '04.04', 'NEGATIVE\r'),
(65, '04.05', 'POSITIVE\r'),
(66, '04.05', 'NEGATIVE\r'),
(67, '04.06', 'NORMAL\r'),
(68, '05.01', 'POSITIVE\r'),
(69, '05.01', 'NEGATIVE\r'),
(70, '05.02', 'NEGATIVE\r'),
(71, '05.02', 'PNEUMOCOCCI\r'),
(72, '05.02', 'MENINGOCOCCI\r'),
(73, '05.02', 'HEMOPHILLUS INFL.\r'),
(74, '05.02', 'CRYPTOCOCCI\r'),
(75, '05.02', 'PLEAMORPHIC BACILLI\r'),
(76, '05.02', 'STAPHYLOCOCCI\r'),
(77, '05.03', 'POSITIVE\r'),
(78, '05.03', 'NEGATIVE\r'),
(79, '05.04', 'POSITIVE\r'),
(80, '05.04', 'NEGATIVE\r'),
(81, '05.05', 'NEGATIVE\r'),
(82, '05.05', '+\r'),
(83, '05.05', '++\r'),
(84, '05.05', '+++\r'),
(85, '05.06', 'POSITIVE\r'),
(86, '05.06', 'NEGATIVE\r'),
(87, '05.07', 'POSITIVE \r'),
(88, '05.07', 'NEGATIVE\r'),
(89, '06.01', 'POSITIVE\r'),
(90, '06.01', 'NEGATIVE\r'),
(91, '06.02', 'POSITIVE\r'),
(92, '06.02', 'NEGATIVE\r'),
(93, '06.03', 'POSITIVE\r'),
(94, '06.03', 'NEGATIVE\r'),
(95, '06.04', 'POSITIVE\r'),
(96, '06.04', 'NEGATIVE\r'),
(97, '06.05', 'POSITIVE\r'),
(98, '06.05', 'NEGATIVE\r'),
(99, '06.06', 'POSITIVE\r'),
(100, '06.06', 'NEGATIVE\r'),
(101, '07.01', 'POSITIVE\r'),
(102, '07.01', 'NEGATIVE\r'),
(103, '07.02', 'HIGH\r'),
(104, '07.02', 'LOW\r'),
(105, '07.02', 'NORMAL\r'),
(106, '07.03', 'HIGH\r'),
(107, '07.03', 'LOW\r'),
(108, '07.03', 'NORMAL\r'),
(109, '07.03.1', '<1.2 (NORMAL)\r'),
(110, '07.03.1', '1.2 - 5\r'),
(111, '07.03.1', '> 5\r'),
(112, '07.03.2', '< 1.2 (NORMAL)\r'),
(113, '07.03.2', '1.2 - 5\r'),
(114, '07.03.2', '> 5\r'),
(115, '07.03.3', '<= 50 (NORMAL)\r'),
(116, '07.03.3', '> 50\r'),
(117, '07.03.4', '<= 50 (NORMAL)\r'),
(118, '07.03.4', '> 50\r'),
(119, '07.04', 'HIGH\r'),
(120, '07.04', 'LOW\r'),
(121, '07.04', 'NORMAL\r'),
(122, '07.04.1', '< 1.4 (NORMAL)\r'),
(123, '07.04.1', '1.4 - 2.5\r'),
(124, '07.04.1', '> 2.5\r'),
(125, '07.04.2', '> 10\r'),
(126, '07.04.2', '10-55 (NORMAL)\r'),
(127, '07.04.2', '< 55\r'),
(128, '08.01', 'POSITIVE\r'),
(129, '08.01', 'NEGATIVE\r'),
(130, '09.01', 'SEDIMENTS\r'),
(131, '09.01', 'SUGAR\r'),
(132, '09.01', 'UROBILINOGEN\r'),
(133, '09.01', 'BILIRUBIN\r'),
(134, '09.01', 'PROTEIN\r'),
(135, '09.01', 'HCG\r');

-- --------------------------------------------------------

--
-- Table structure for table `EXAMTYPE`
--

CREATE TABLE `EXAMTYPE` (
  `EXC_ID_A` char(2) NOT NULL,
  `EXC_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EXAMTYPE`
--

INSERT INTO `EXAMTYPE` (`EXC_ID_A`, `EXC_DESC`) VALUES
('BA', '4.Bacteriology\r'),
('BT', '2.Blood transfusion\r'),
('CH', '7.Chemistry\r'),
('HB', '1.Haematology\r'),
('MC', '5.Microscopy\r'),
('OC', '8.Occult Blood\r'),
('OT', 'OTHER'),
('PA', '3.Parasitology\r'),
('SE', '6.Serology\r');

-- --------------------------------------------------------

--
-- Table structure for table `GROUPMENU`
--

CREATE TABLE `GROUPMENU` (
  `GM_ID` int(11) NOT NULL,
  `GM_UG_ID_A` varchar(50) NOT NULL DEFAULT '',
  `GM_MNI_ID_A` varchar(50) NOT NULL DEFAULT '',
  `GM_ACTIVE` char(1) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `GROUPMENU`
--

INSERT INTO `GROUPMENU` (`GM_ID`, `GM_UG_ID_A`, `GM_MNI_ID_A`, `GM_ACTIVE`) VALUES
(1, 'admin', 'admtype', 'Y'),
(2, 'admin', 'disctype', 'Y'),
(44, 'admin', 'medicalsward', 'Y'),
(4, 'admin', 'admission', 'Y'),
(7, 'admin', 'disease', 'Y'),
(8, 'admin', 'exams', 'Y'),
(9, 'admin', 'exit', 'Y'),
(10, 'admin', 'file', 'Y'),
(11, 'admin', 'generaldata', 'Y'),
(12, 'admin', 'groups', 'Y'),
(13, 'admin', 'help', 'Y'),
(14, 'admin', 'hospital', 'Y'),
(43, 'admin', 'agetype', 'Y'),
(16, 'admin', 'laboratory', 'Y'),
(17, 'admin', 'medicals', 'Y'),
(18, 'admin', 'medicalstock', 'Y'),
(19, 'admin', 'operation', 'Y'),
(21, 'admin', 'pharmacy', 'Y'),
(22, 'admin', 'statistics', 'Y'),
(26, 'admin', 'opd', 'Y'),
(27, 'admin', 'users', 'Y'),
(28, 'admin', 'usersusers', 'Y'),
(29, 'admin', 'vaccine', 'Y'),
(30, 'admin', 'ward', 'Y'),
(31, 'admin', 'types', 'Y'),
(32, 'admin', 'pretreatmenttype', 'Y'),
(33, 'admin', 'diseasetype', 'Y'),
(34, 'admin', 'medstockmovtype', 'Y'),
(35, 'admin', 'examtype', 'Y'),
(36, 'admin', 'operationtype', 'Y'),
(37, 'admin', 'deliverytype', 'Y'),
(38, 'admin', 'medicalstype', 'Y'),
(39, 'admin', 'delresulttype', 'Y'),
(40, 'admin', 'printing', 'Y'),
(41, 'admin', 'examlist1', 'Y'),
(42, 'admin', 'diseaselist', 'Y'),
(45, 'admin', 'priceslists', 'Y'),
(46, 'admin', 'otherprices', 'Y'),
(47, 'admin', 'accounting', 'Y'),
(48, 'admin', 'newbill', 'Y'),
(49, 'admin', 'billsmanager', 'Y'),
(50, 'admin', 'data', 'Y'),
(51, 'admin', 'btnadmdel', 'Y'),
(52, 'admin', 'btnadmadm', 'Y'),
(53, 'admin', 'btnadmedit', 'Y'),
(54, 'admin', 'btnadmnew', 'Y'),
(55, 'admin', 'btnadmopd', 'Y'),
(56, 'admin', 'btnadmmer', 'Y'),
(57, 'admin', 'btnlaboratorydel', 'Y'),
(58, 'admin', 'btnlaboratoryedit', 'Y'),
(59, 'admin', 'btnlaboratorynew', 'Y'),
(60, 'admin', 'btnopdedit', 'Y'),
(61, 'admin', 'btnopddel', 'Y'),
(62, 'admin', 'btnopdnew', 'Y'),
(63, 'admin', 'btndatamalnut', 'Y'),
(64, 'admin', 'btndatadel', 'Y'),
(65, 'admin', 'btndataedit', 'Y'),
(66, 'admin', 'btndataeditpat', 'Y'),
(67, 'admin', 'btnpharmaceuticaldel', 'Y'),
(68, 'admin', 'btnpharmaceuticaledit', 'Y'),
(69, 'admin', 'btnpharmaceuticalnew', 'Y'),
(70, 'guest', 'admtype', 'N'),
(71, 'guest', 'disctype', 'N'),
(72, 'guest', 'admission', 'Y'),
(73, 'guest', 'disease', 'N'),
(74, 'guest', 'exams', 'N'),
(75, 'guest', 'exit', 'Y'),
(76, 'guest', 'file', 'Y'),
(77, 'guest', 'generaldata', 'N'),
(78, 'guest', 'groups', 'N'),
(79, 'guest', 'help', 'N'),
(80, 'guest', 'hospital', 'N'),
(81, 'guest', 'laboratory', 'Y'),
(82, 'guest', 'medicals', 'Y'),
(83, 'guest', 'medicalstock', 'N'),
(84, 'guest', 'operation', 'N'),
(85, 'guest', 'pharmacy', 'Y'),
(86, 'guest', 'statistics', 'Y'),
(87, 'guest', 'opd', 'Y'),
(88, 'guest', 'users', 'N'),
(89, 'guest', 'usersusers', 'N'),
(90, 'guest', 'vaccine', 'N'),
(91, 'guest', 'ward', 'N'),
(92, 'guest', 'types', 'N'),
(93, 'guest', 'pretreatmenttype', 'N'),
(94, 'guest', 'diseasetype', 'N'),
(95, 'guest', 'medstockmovtype', 'N'),
(96, 'guest', 'examtype', 'N'),
(97, 'guest', 'operationtype', 'N'),
(98, 'guest', 'deliverytype', 'N'),
(99, 'guest', 'medicalstype', 'N'),
(100, 'guest', 'delresulttype', 'N'),
(101, 'guest', 'printing', 'Y'),
(102, 'guest', 'examlist1', 'Y'),
(103, 'guest', 'diseaselist', 'Y'),
(104, 'admin', 'btnadmtherapy', 'Y'),
(105, 'admin', 'btnbillnew', 'Y'),
(106, 'admin', 'btnbilledit', 'Y'),
(107, 'admin', 'btnbilldelete', 'Y'),
(108, 'admin', 'btnbillreport', 'Y'),
(109, 'admin', 'vaccinetype', 'Y'),
(110, 'admin', 'patientvaccine', 'Y'),
(111, 'admin', 'btnpatientvaccinenew', 'Y'),
(112, 'admin', 'btnpatientvaccineedit', 'Y'),
(113, 'admin', 'btnpatientvaccinedel', 'Y'),
(115, 'admin', 'communication', 'Y'),
(116, 'guest', 'communication', 'Y'),
(117, 'admin', 'btnadmbill', 'Y'),
(118, 'admin', 'btnbillreceipt', 'Y'),
(119, 'admin', 'btnadmpatientfolder', 'Y'),
(120, 'admin', 'btnopdnewexamination', 'Y'),
(121, 'admin', 'btnopdeditexamination', 'Y'),
(122, 'admin', 'btnadmadmexamination', 'Y'),
(123, 'admin', 'btnadmexamination', 'Y'),
(124, 'admin', 'supplier', 'Y'),
(125, 'admin', 'btnpatfoldopdrpt', 'Y'),
(126, 'admin', 'btnpatfoldadmrpt', 'Y'),
(127, 'admin', 'btnpatfoldpatrpt', 'Y'),
(128, 'admin', 'btnpatfolddicom', 'Y'),
(129, 'admin', 'smsmanager', 'Y'),
(130, 'admin', 'btnpharmstockcharge', 'Y'),
(131, 'admin', 'btnpharmstockdischarge', 'Y'),
(132, 'admin', 'btnmedicalswardreport', 'Y'),
(133, 'admin', 'btnmedicalswardexcel', 'Y'),
(134, 'admin', 'btnmedicalswardrectify', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `HELP`
--

CREATE TABLE `HELP` (
  `HL_ID` int(11) NOT NULL,
  `HL_MASK` int(11) NOT NULL,
  `HL_FIELD` int(11) NOT NULL,
  `HL_LANG` char(2) DEFAULT NULL,
  `HL_MSG` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `HOSPITAL`
--

CREATE TABLE `HOSPITAL` (
  `HOS_ID_A` varchar(10) NOT NULL,
  `HOS_NAME` varchar(255) NOT NULL,
  `HOS_ADDR` varchar(255) NOT NULL,
  `HOS_CITY` varchar(255) NOT NULL,
  `HOS_TELE` varchar(50) DEFAULT NULL,
  `HOS_FAX` varchar(50) DEFAULT NULL,
  `HOS_EMAIL` varchar(50) DEFAULT NULL,
  `HOS_CURR_COD` varchar(3) DEFAULT NULL,
  `HOS_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HOSPITAL`
--

INSERT INTO `HOSPITAL` (`HOS_ID_A`, `HOS_NAME`, `HOS_ADDR`, `HOS_CITY`, `HOS_TELE`, `HOS_FAX`, `HOS_EMAIL`, `HOS_CURR_COD`, `HOS_LOCK`) VALUES
('STLUKE', 'St. Luke HOSPITAL - Angal', 'P.O. BOX 85 - NEBBI', 'ANGAL', '+256 0472621076', '+256 0', 'angal@ucmb.ug.co.', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `LABORATORY`
--

CREATE TABLE `LABORATORY` (
  `LAB_ID` int(11) NOT NULL,
  `LAB_EXA_ID_A` varchar(10) NOT NULL,
  `LAB_DATE` datetime NOT NULL,
  `LAB_RES` varchar(50) NOT NULL,
  `LAB_NOTE` varchar(255) DEFAULT NULL,
  `LAB_PAT_ID` int(11) DEFAULT NULL,
  `LAB_PAT_NAME` varchar(100) DEFAULT NULL,
  `LAB_CROSS1` int(11) DEFAULT NULL,
  `LAB_CROSS2` int(11) DEFAULT NULL,
  `LAB_CROSS3` int(11) DEFAULT NULL,
  `LAB_CROSS4` int(11) DEFAULT NULL,
  `LAB_CROSS5` int(11) DEFAULT NULL,
  `LAB_CROSS6` int(11) DEFAULT NULL,
  `LAB_CROSS7` int(11) DEFAULT NULL,
  `LAB_CROSS8` int(11) DEFAULT NULL,
  `LAB_CROSS9` int(11) DEFAULT NULL,
  `LAB_CROSS10` int(11) DEFAULT NULL,
  `LAB_CROSS11` int(11) DEFAULT NULL,
  `LAB_CROSS12` int(11) DEFAULT NULL,
  `LAB_CROSS13` int(11) DEFAULT NULL,
  `LAB_LOCK` int(11) NOT NULL DEFAULT '0',
  `LAB_AGE` int(11) DEFAULT NULL,
  `LAB_SEX` char(1) DEFAULT NULL,
  `LAB_MATERIAL` varchar(25) DEFAULT NULL,
  `LAB_EXAM_DATE` date DEFAULT NULL,
  `LAB_PAT_INOUT` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `LABORATORYROW`
--

CREATE TABLE `LABORATORYROW` (
  `LABR_ID` int(11) NOT NULL,
  `LABR_LAB_ID` int(11) NOT NULL,
  `LABR_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `LOG`
--

CREATE TABLE `LOG` (
  `LOG_ID` int(11) NOT NULL,
  `LOG_TYPE` int(11) NOT NULL,
  `LOG_CLASS` varchar(100) DEFAULT NULL,
  `LOG_METHOD` varchar(64) DEFAULT NULL,
  `LOG_TIME` datetime NOT NULL,
  `LOG_MESS` varchar(1024) DEFAULT NULL,
  `LOG_USER` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MALNUTRITIONCONTROL`
--

CREATE TABLE `MALNUTRITIONCONTROL` (
  `MLN_ID` int(11) NOT NULL,
  `MLN_DATE_SUPP` datetime NOT NULL,
  `MNL_DATE_CONF` datetime DEFAULT NULL,
  `MLN_ADM_ID` int(11) NOT NULL,
  `MLN_HEIGHT` float NOT NULL,
  `MLN_WEIGHT` float NOT NULL,
  `MLN_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSR`
--

CREATE TABLE `MEDICALDSR` (
  `MDSR_ID` int(11) NOT NULL,
  `MDSR_MDSRT_ID_A` char(1) NOT NULL,
  `MDSR_CODE` varchar(5) NOT NULL,
  `MDSR_DESC` varchar(100) NOT NULL,
  `MDSR_MIN_STOCK_QTI` float NOT NULL DEFAULT '0',
  `MDSR_INI_STOCK_QTI` float NOT NULL DEFAULT '0',
  `MDSR_PCS_X_PCK` int(11) NOT NULL,
  `MDSR_IN_QTI` float NOT NULL DEFAULT '0',
  `MDSR_OUT_QTI` float NOT NULL DEFAULT '0',
  `MDSR_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MEDICALDSR`
--

INSERT INTO `MEDICALDSR` (`MDSR_ID`, `MDSR_MDSRT_ID_A`, `MDSR_CODE`, `MDSR_DESC`, `MDSR_MIN_STOCK_QTI`, `MDSR_INI_STOCK_QTI`, `MDSR_PCS_X_PCK`, `MDSR_IN_QTI`, `MDSR_OUT_QTI`, `MDSR_LOCK`) VALUES
(1, 'K', '', 'Glucose Test Strip\r', 0, 0, 0, 0, 0, 0),
(2, 'L', '', 'Acetic Acid Glacial 1 ltr\r', 0, 0, 0, 0, 0, 0),
(3, 'L', '', 'Aceton 99% 1ltr\r', 0, 0, 0, 0, 0, 0),
(4, 'L', '', 'Copper 11 Sulphate 500g\r', 0, 0, 0, 0, 0, 0),
(5, 'L', '', 'EDTA Di- sodium salt 100g\r', 0, 0, 0, 0, 0, 0),
(6, 'L', '', 'Ethanol Absolute 1ltr\r', 0, 0, 0, 0, 0, 0),
(7, 'L', '', 'Formaldehyde solution 35-38% 1ltr\r', 0, 0, 0, 0, 0, 0),
(8, 'L', '', 'Hydrochloric Acid 30-33% 1ltr\r', 0, 0, 0, 0, 0, 0),
(9, 'L', '', 'Iodine Crystal 100g\r', 0, 0, 0, 0, 0, 0),
(10, 'L', '', 'Methanol 99% 1ltr\r', 0, 0, 0, 0, 0, 0),
(11, 'L', '', 'Phenol crystals 1kg\r', 0, 0, 0, 0, 0, 0),
(12, 'L', '', 'Potassium iodide 100g\r', 0, 0, 0, 0, 0, 0),
(13, 'L', '', 'Sodium Carbonate Anhydrous 500g\r', 0, 0, 0, 0, 0, 0),
(14, 'L', '', 'Sodium Citrate 100g\r', 0, 0, 0, 0, 0, 0),
(15, 'L', '', 'Sodium Sulphate 500g\r', 0, 0, 0, 0, 0, 0),
(16, 'L', '', 'Sodium Nitrate 25g\r', 0, 0, 0, 0, 0, 0),
(17, 'L', '', 'Sulphosalicylic Acid 500g\r', 0, 0, 0, 0, 0, 0),
(18, 'L', '', 'Sulphuric Acid Conc 1ltr\r', 0, 0, 0, 0, 0, 0),
(19, 'L', '', 'Xylene 2.5 ltrs\r', 0, 0, 0, 0, 0, 0),
(20, 'L', '', 'Sodium Fluoride\r', 0, 0, 0, 0, 0, 0),
(21, 'L', '', 'Potassium Oxalate\r', 0, 0, 0, 0, 0, 0),
(22, 'L', '', 'Brilliant Cresyl Blue\r', 0, 0, 0, 0, 0, 0),
(23, 'L', '', 'Ammonium Oxalate\r', 0, 0, 0, 0, 0, 0),
(24, 'L', '', '4 Dimethyl Aminobenzaldelyde\r', 0, 0, 0, 0, 0, 0),
(25, 'L', '', 'Trichloro acetic Acid\r', 0, 0, 0, 0, 0, 0),
(26, 'L', '', 'Non 111 Chloride (Ferric chloride)\r', 0, 0, 0, 0, 0, 0),
(27, 'L', '', 'Sodium Carbonate Anhydrous\r', 0, 0, 0, 0, 0, 0),
(28, 'L', '', 'Trisodium Citrate\r', 0, 0, 0, 0, 0, 0),
(29, 'L', '', 'Crystal Violet\r', 0, 0, 0, 0, 0, 0),
(30, 'K', '', 'GPT (ALT) 200ml ( Does not have NAOH)\r', 0, 0, 0, 0, 0, 0),
(31, 'K', '', 'GOT ( AST) 200ml has no NAOH) AS 101\r', 0, 0, 0, 0, 0, 0),
(32, 'K', '', 'GOT ( AST) 200ml (Calorimetric) AS 147\r', 0, 0, 0, 0, 0, 0),
(33, 'K', '', 'HIV 1/2 Capillus Kit 100Tests\r', 0, 0, 0, 0, 0, 0),
(34, 'K', '', 'HIV Buffer for determine Kit\r', 0, 0, 0, 0, 0, 0),
(35, 'K', '', 'HIV Determine 1/11 (Abbott) 100Tests\r', 0, 0, 0, 0, 0, 0),
(36, 'K', '', 'HIV UNIGOLD 1/11 Test Kits 20 Tests\r', 0, 0, 0, 0, 0, 0),
(37, 'K', '', 'Pregnacy ( HGG Latex) 50 tests\r', 0, 0, 0, 0, 0, 0),
(38, 'K', '', 'RPR 125mm x 75mm\r', 0, 0, 0, 0, 0, 0),
(39, 'K', '', 'RPR ( VDRL Carbon ) Antigen 5ml\r', 0, 0, 0, 0, 0, 0),
(40, 'D', '', 'Adrenaline 1mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(41, 'D', '', 'Aminophylline 25mg/ml,10ml Amp\r', 0, 0, 0, 0, 0, 0),
(42, 'D', '', 'Amphotericin B 50mg Vial\r', 0, 0, 0, 0, 0, 0),
(43, 'D', '', 'Ampicillin 500mg Vial\r', 0, 0, 0, 0, 0, 0),
(44, 'D', '', 'Atropine 1mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(45, 'D', '', 'Benzathine Penicillin 2.4 MIU Vial\r', 0, 0, 0, 0, 0, 0),
(46, 'D', '', 'Benzyl Penicillin 1 MIU Vial\r', 0, 0, 0, 0, 0, 0),
(47, 'D', '', 'Benzyl Penicillin 5 MIU Vial\r', 0, 0, 0, 0, 0, 0),
(48, 'D', '', 'Chloramphenicol 1g Vial\r', 0, 0, 0, 0, 0, 0),
(49, 'D', '', 'Chloroquine 40mg Base/ml 5ml Amp\r', 0, 0, 0, 0, 0, 0),
(50, 'D', '', 'Chlorpromazine 25mg/ml/2ml Amp\r', 0, 0, 0, 0, 0, 0),
(51, 'D', '', 'Cloxacillin 500mg Vial\r', 0, 0, 0, 0, 0, 0),
(52, 'D', '', 'Cyclophosphamide 200mg Vial\r', 0, 0, 0, 0, 0, 0),
(53, 'D', '', 'Cyclophosphamide 500mg Vial\r', 0, 0, 0, 0, 0, 0),
(54, 'D', '', 'Diazepam 5mg / ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(55, 'D', '', 'Diclofenac 25mg/ml 3ml Amp\r', 0, 0, 0, 0, 0, 0),
(56, 'D', '', 'Digoxin 0.25 mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(57, 'D', '', 'Furosemide 10mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(58, 'D', '', 'Gentamicin 40mg/ml 2ml\r', 0, 0, 0, 0, 0, 0),
(59, 'D', '', 'Haloperidol 5mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(60, 'D', '', 'Haloperidol Decanoate 50mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(61, 'D', '', 'Hydralazine 20mg Vial\r', 0, 0, 0, 0, 0, 0),
(62, 'D', '', 'Hydrocortisone 100mg Vial\r', 0, 0, 0, 0, 0, 0),
(63, 'D', '', 'Hyoscine Butyl Bromide 20mg/ml/ Amp\r', 0, 0, 0, 0, 0, 0),
(64, 'D', '', 'Insulin Soluble 100IU/ml 10ml Vial\r', 0, 0, 0, 0, 0, 0),
(65, 'D', '', 'Insulin Isophane 100IU/ml 10ml Vial\r', 0, 0, 0, 0, 0, 0),
(66, 'D', '', 'Insulin Mixtard 30/70 100IU/ml 10ml Vial\r', 0, 0, 0, 0, 0, 0),
(67, 'D', '', 'Insulin Mixtard 30/70 100IU/ml 5x3ml catridges\r', 0, 0, 0, 0, 0, 0),
(68, 'D', '', 'Insulin Isophane 40IU/ml 10ml\r', 0, 0, 0, 0, 0, 0),
(69, 'D', '', 'Iron Dextran 10mg/ml 2ml Vial\r', 0, 0, 0, 0, 0, 0),
(70, 'D', '', 'Ketamine 10mg/ml 20ml Vial\r', 0, 0, 0, 0, 0, 0),
(71, 'D', '', 'Ketamine 10mg/ml 10ml Vial\r', 0, 0, 0, 0, 0, 0),
(72, 'D', '', 'Lignocaine 2% 20ml Vial\r', 0, 0, 0, 0, 0, 0),
(73, 'D', '', 'Lignocaine 2% Adrenaline Dent.cartridges\r', 0, 0, 0, 0, 0, 0),
(74, 'D', '', 'Lignocaine spinal 50mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(75, 'D', '', 'Methylergomeatrine 0.2mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(76, 'D', '', 'Methylergomeatrine 0.5mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(77, 'D', '', 'Metoclopramide 5mg/ml 100ml Amp\r', 0, 0, 0, 0, 0, 0),
(78, 'D', '', 'Metronidazole 5mg/ml 2ml IV\r', 0, 0, 0, 0, 0, 0),
(79, 'D', '', 'Morphine 15mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(80, 'D', '', 'Oxytocin 10 IU/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(81, 'D', '', 'Pethidine 100mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(82, 'D', '', 'Pethidine 50mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(83, 'D', '', 'Phenobarbital 100mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(84, 'D', '', 'Phytomenadione 10mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(85, 'D', '', 'Phytomenadione 1mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(86, 'D', '', 'Procaine Penicillin Fortified 4 MIU Vial\r', 0, 0, 0, 0, 0, 0),
(87, 'D', '', 'Promethazine 25mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(88, 'D', '', 'Quinine Di-HCI 300mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(89, 'D', '', 'Ranitidine 25mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(90, 'D', '', 'Streptomycin 1g Vial\r', 0, 0, 0, 0, 0, 0),
(91, 'D', '', 'Suxamethonium 500mg Vial\r', 0, 0, 0, 0, 0, 0),
(92, 'D', '', 'Suxamethonium 500mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(93, 'D', '', 'tetanus Antitoxin 1500 IU 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(94, 'D', '', 'Thiopental Sodium 500mg Vial\r', 0, 0, 0, 0, 0, 0),
(95, 'D', '', 'Water for Injection 10ml Vial\r', 0, 0, 0, 0, 0, 0),
(96, 'D', '', 'Sodium Chloride 0.9% IV 500ml\r', 0, 0, 0, 0, 0, 0),
(97, 'D', '', 'Sodium Lactate Compound IV 500ml\r', 0, 0, 0, 0, 0, 0),
(98, 'D', '', 'Acetazolamide 250mg Tab\r', 0, 0, 0, 0, 0, 0),
(99, 'D', '', 'Acyclovir 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(100, 'D', '', 'Aciclovir\r', 0, 0, 0, 0, 0, 0),
(101, 'D', '', 'Aminophylline 100mg Tab\r', 0, 0, 0, 0, 0, 0),
(102, 'D', '', 'Albendazole 400mg Tab\r', 0, 0, 0, 0, 0, 0),
(103, 'D', '', 'Albendazole 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(104, 'D', '', 'Amitriptyline 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(105, 'D', '', 'Amoxicillin 250mg Caps\r', 0, 0, 0, 0, 0, 0),
(106, 'D', '', 'Amoxicillin /Clavulanate 375mg Tab\r', 0, 0, 0, 0, 0, 0),
(107, 'D', '', 'Ascorbic Acid 100mg tab\r', 0, 0, 0, 0, 0, 0),
(108, 'D', '', 'Aspirin 300mg Tab\r', 0, 0, 0, 0, 0, 0),
(109, 'D', '', 'Atenolol 50mg Tab\r', 0, 0, 0, 0, 0, 0),
(110, 'D', '', 'Atenolol 100mg Tab\r', 0, 0, 0, 0, 0, 0),
(111, 'D', '', 'Bendrofluazide 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(112, 'D', '', 'Benzhexol 2mg Tab\r', 0, 0, 0, 0, 0, 0),
(113, 'D', '', 'Benzhexol 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(114, 'D', '', 'Bisacodyl 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(115, 'D', '', 'Calcium Lactate 300mg Tab\r', 0, 0, 0, 0, 0, 0),
(116, 'D', '', 'Carbamazepine 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(117, 'D', '', 'Carbimazole 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(118, 'D', '', 'Charcoal 250mg Tab\r', 0, 0, 0, 0, 0, 0),
(119, 'D', '', 'Chloramphenicol 250mg Caps\r', 0, 0, 0, 0, 0, 0),
(120, 'D', '', 'Chloroquine Uncoated 150mg Tab\r', 0, 0, 0, 0, 0, 0),
(121, 'D', '', 'Chloroquine Coated 150mg Tab\r', 0, 0, 0, 0, 0, 0),
(122, 'K', '', 'UREA Calorimetric 300 Tests\r', 0, 0, 0, 0, 0, 0),
(123, 'D', '', 'Chlorphenimine 4mg Tab\r', 0, 0, 0, 0, 0, 0),
(124, 'D', '', 'Chlorpromazine 100mg Tab\r', 0, 0, 0, 0, 0, 0),
(125, 'D', '', 'Chlorpromazine 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(126, 'D', '', 'Cimetidine 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(127, 'D', '', 'Cimetidine 400mg Tab\r', 0, 0, 0, 0, 0, 0),
(128, 'D', '', 'Ciprofloxacine 500mg Tab\r', 0, 0, 0, 0, 0, 0),
(129, 'D', '', 'Ciprofloxacine 250mg Tab\r', 0, 0, 0, 0, 0, 0),
(130, 'D', '', 'Cloxacillin 250mg Tab\r', 0, 0, 0, 0, 0, 0),
(131, 'D', '', 'Codein Phosphate 30mg Tab\r', 0, 0, 0, 0, 0, 0),
(132, 'D', '', 'Cotrimoxazole 100/20mg Paed Tab\r', 0, 0, 0, 0, 0, 0),
(133, 'D', '', 'Cotrimoxazole 400/80mg Tab\r', 0, 0, 0, 0, 0, 0),
(134, 'D', '', 'Darrows Half Strength 500ml\r', 0, 0, 0, 0, 0, 0),
(135, 'D', '', 'Dexamethasone 4mg/ml 2ml Amp\r', 0, 0, 0, 0, 0, 0),
(136, 'D', '', 'Dexamethasone 4mg/ml 1ml Amp\r', 0, 0, 0, 0, 0, 0),
(137, 'D', '', 'Dextrose 5% IV 500ml\r', 0, 0, 0, 0, 0, 0),
(138, 'D', '', 'Dextrose 30% IV 100ml\r', 0, 0, 0, 0, 0, 0),
(139, 'D', '', 'Dextrose 50% IV 100ml\r', 0, 0, 0, 0, 0, 0),
(140, 'D', '', 'Dexamethasone 0.5mg Tab\r', 0, 0, 0, 0, 0, 0),
(141, 'D', '', 'Diazepam 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(142, 'D', '', 'Diclofenac 50mg Tab\r', 0, 0, 0, 0, 0, 0),
(143, 'D', '', 'Diethylcarbamazine 50mg Tab\r', 0, 0, 0, 0, 0, 0),
(144, 'D', '', 'Digoxin 0.25 mg Tab\r', 0, 0, 0, 0, 0, 0),
(145, 'D', '', 'Doxycycline 100mg Tab\r', 0, 0, 0, 0, 0, 0),
(146, 'D', '', 'Ephedrine 30mg Tab\r', 0, 0, 0, 0, 0, 0),
(147, 'D', '', 'Erythromycin 250mg Tab\r', 0, 0, 0, 0, 0, 0),
(148, 'D', '', 'Fansidar 500/25mg Tab (50dosesx3)\r', 0, 0, 0, 0, 0, 0),
(149, 'D', '', 'Fansidar 500/25mg Tab\r', 0, 0, 0, 0, 0, 0),
(150, 'D', '', 'Ferrous Sulphate 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(151, 'D', '', 'Fluconazole 100mg Tab\r', 0, 0, 0, 0, 0, 0),
(152, 'D', '', 'Fluconazole 100mg 24 Caps\r', 0, 0, 0, 0, 0, 0),
(153, 'D', '', 'Folic Acid 1mg Tab\r', 0, 0, 0, 0, 0, 0),
(154, 'D', '', 'Folic Acid 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(155, 'D', '', 'Folic Acid/Ferrous Sulp 0.5/200mg Tab\r', 0, 0, 0, 0, 0, 0),
(156, 'D', '', 'Folic Acid 15mg Tab\r', 0, 0, 0, 0, 0, 0),
(157, 'D', '', 'Frusemide 40mg Tab\r', 0, 0, 0, 0, 0, 0),
(158, 'D', '', 'Glibenclamide 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(159, 'D', '', 'Griseofulvin 500mg Tab\r', 0, 0, 0, 0, 0, 0),
(160, 'D', '', 'Haloperidol 5mg Tab\r', 0, 0, 0, 0, 0, 0),
(161, 'D', '', 'Haloperidol 5mg\r', 0, 0, 0, 0, 0, 0),
(162, 'D', '', 'Hydralazine 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(163, 'D', '', 'Hyoscine 10mg Tab\r', 0, 0, 0, 0, 0, 0),
(164, 'D', '', 'Ibuprofen 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(165, 'D', '', 'Imipramine 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(166, 'D', '', 'Indomethacin 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(167, 'D', '', 'Isoniazid 300mg Tab\r', 0, 0, 0, 0, 0, 0),
(168, 'D', '', 'Ketoconazole 200mg Tab\r', 0, 0, 0, 0, 0, 0),
(169, 'D', '', 'Salbutamol 4mg Tab\r', 0, 0, 0, 0, 0, 0),
(170, 'D', '', 'Spironolactone 25mg Tab\r', 0, 0, 0, 0, 0, 0),
(171, 'D', '', 'Tolbutamide 500mg tab\r', 0, 0, 0, 0, 0, 0),
(172, 'D', '', 'Vitamin A 200.000 IU Caps\r', 0, 0, 0, 0, 0, 0),
(173, 'D', '', 'Vitamin B Complex Tab\r', 0, 0, 0, 0, 0, 0),
(174, 'D', '', 'Oral Rehydration Salt (ORS)\r', 0, 0, 0, 0, 0, 0),
(175, 'D', '', 'Paracetamol 120mg/5ml Syrup\r', 0, 0, 0, 0, 0, 0),
(176, 'D', '', 'Paracetamol 120mg/5ml 100ml\r', 0, 0, 0, 0, 0, 0),
(177, 'D', '', 'Quinine 100mg/5ml Syrup 100ml\r', 0, 0, 0, 0, 0, 0),
(178, 'D', '', 'Alcohol 95% not denatured 20Ltrs\r', 0, 0, 0, 0, 0, 0),
(179, 'D', '', 'Chlorhexidine/Cetrimide 1.5/15% 1Ltr\r', 0, 0, 0, 0, 0, 0),
(180, 'D', '', 'Chlorhexidine/Cetrimide 1.5/15% 5Ltr\r', 0, 0, 0, 0, 0, 0),
(181, 'D', '', 'Gentian Violet 25g Tin\r', 0, 0, 0, 0, 0, 0),
(182, 'D', '', 'Hydrogen Peroxide 6% 250ml\r', 0, 0, 0, 0, 0, 0),
(183, 'D', '', 'Iodine Solution 2% 500ml\r', 0, 0, 0, 0, 0, 0),
(184, 'D', '', 'Sodium Hypochlorite solution 0.75 Ltr\r', 0, 0, 0, 0, 0, 0),
(185, 'D', '', 'liquid detergent 5Ltr Perfumed\r', 0, 0, 0, 0, 0, 0),
(186, 'D', '', 'Soap Blue Bar 550g\r', 0, 0, 0, 0, 0, 0),
(187, 'D', '', 'Liquid detergent 20Ltr\r', 0, 0, 0, 0, 0, 0),
(188, 'D', '', 'Soap Powder Hand wash 5kg\r', 0, 0, 0, 0, 0, 0),
(189, 'D', '', 'Sodium Hypochlorite solution 5Ltr\r', 0, 0, 0, 0, 0, 0),
(190, 'D', '', 'Betamethasone 0.1% eye/ear/nose drops\r', 0, 0, 0, 0, 0, 0),
(191, 'D', '', 'Betamethasone 0.1% Neomycin 0.35 %eye drops 7.5ml\r', 0, 0, 0, 0, 0, 0),
(192, 'D', '', 'Chloramphenicol 0.5% Eye Drops 10ml\r', 0, 0, 0, 0, 0, 0),
(193, 'D', '', 'Cloramphenicol 1% Eye Ointment 3.5g\r', 0, 0, 0, 0, 0, 0),
(194, 'D', '', 'Gentamicin 0.3% eye/ear drops 10ml\r', 0, 0, 0, 0, 0, 0),
(195, 'D', '', 'Hydrocortisone 1% eye drops 5ml\r', 0, 0, 0, 0, 0, 0),
(196, 'D', '', 'Tetracycline eye ointment 1% 3.5g\r', 0, 0, 0, 0, 0, 0),
(197, 'D', '', 'Beclomethasone 50mcg Inhaler\r', 0, 0, 0, 0, 0, 0),
(198, 'K', '', 'Urine Test Strips 3 Parameters 100 tests\r', 0, 0, 0, 0, 0, 0),
(199, 'D', '', 'Salbutamol solution for inhalation 5ml\r', 0, 0, 0, 0, 0, 0),
(200, 'D', '', 'Salbutamol Inhaler 10ml\r', 0, 0, 0, 0, 0, 0),
(201, 'D', '', 'Clotrimazole 500mg Pessaries\r', 0, 0, 0, 0, 0, 0),
(202, 'D', '', 'Clotrimazole 100mg Pessaries\r', 0, 0, 0, 0, 0, 0),
(203, 'D', '', 'Diazepam 2mg/ml 2.5ml Rectal Tube\r', 0, 0, 0, 0, 0, 0),
(204, 'D', '', 'Antihaemorrhoid suppositories\r', 0, 0, 0, 0, 0, 0),
(205, 'D', '', 'Nystatin 100.000 IU Pessaries\r', 0, 0, 0, 0, 0, 0),
(206, 'D', '', 'Dextrose Monohydrate Apyrogen 25Kg\r', 0, 0, 0, 0, 0, 0),
(207, 'D', '', 'Amoxicillin 125mg/5ml Powd. Susp 100ml\r', 0, 0, 0, 0, 0, 0),
(208, 'D', '', 'Chloramphenicol 125mg/5ml Susp 3Ltr\r', 0, 0, 0, 0, 0, 0),
(209, 'D', '', 'Chloramphenicol 125mg/5ml Susp 100ml\r', 0, 0, 0, 0, 0, 0),
(210, 'D', '', 'Cotrimoxazole 200+40mg/5ml Susp 3Ltr\r', 0, 0, 0, 0, 0, 0),
(211, 'D', '', 'Cotrimoxazole 200+40mg/5ml Susp 100ml\r', 0, 0, 0, 0, 0, 0),
(212, 'D', '', 'Nystatin 500.000IU/ Susp/ Drops\r', 0, 0, 0, 0, 0, 0),
(213, 'D', '', 'Pyridoxine 50mg Tab\r', 0, 0, 0, 0, 0, 0),
(214, 'D', '', 'Quinine 300mg Tab\r', 0, 0, 0, 0, 0, 0),
(215, 'D', '', 'Ranitidine 150mg Tab\r', 0, 0, 0, 0, 0, 0),
(216, 'D', '', 'Rifampicin/Isoniazid 150/100 Tab\r', 0, 0, 0, 0, 0, 0),
(217, 'D', '', 'Sodium Chloride Apyrogen 50kg\r', 0, 0, 0, 0, 0, 0),
(218, 'K', '', 'Field stain A and B\r', 0, 0, 0, 0, 0, 0),
(219, 'K', '', 'Genitian Violet\r', 0, 0, 0, 0, 0, 0),
(220, 'K', '', 'Neutral Red\r', 0, 0, 0, 0, 0, 0),
(221, 'K', '', 'Eosin Yellowish\r', 0, 0, 0, 0, 0, 0),
(222, 'K', '', 'Giemsa Stain\r', 0, 0, 0, 0, 0, 0),
(223, 'K', '', 'Anti Serum A 10ml\r', 0, 0, 0, 0, 0, 0),
(224, 'S', '', 'Blood giving set Disposable\r', 0, 0, 0, 0, 0, 0),
(225, 'S', '', 'Blood Transfer Bag 300ml\r', 0, 0, 0, 0, 0, 0),
(226, 'S', '', 'Insulin Syringe 100IU with Needle G26/29\r', 0, 0, 0, 0, 0, 0),
(227, 'S', '', 'IV Cannula G16 with Port\r', 0, 0, 0, 0, 0, 0),
(228, 'S', '', 'IV Cannula G18 with Port\r', 0, 0, 0, 0, 0, 0),
(229, 'S', '', 'IV Cannula G20 with Port\r', 0, 0, 0, 0, 0, 0),
(230, 'S', '', 'IV Cannula G22 with Port\r', 0, 0, 0, 0, 0, 0),
(231, 'S', '', 'IV Cannula G24 without Port\r', 0, 0, 0, 0, 0, 0),
(232, 'S', '', 'IV Cannula G24 with Port\r', 0, 0, 0, 0, 0, 0),
(233, 'S', '', 'IV Giving set Disposable\r', 0, 0, 0, 0, 0, 0),
(234, 'S', '', 'Needle container disposable of contaminated\r', 0, 0, 0, 0, 0, 0),
(235, 'S', '', 'Needles Luer G20 Disposable\r', 0, 0, 0, 0, 0, 0),
(236, 'S', '', 'Needles Luer G21 Disposable\r', 0, 0, 0, 0, 0, 0),
(237, 'S', '', 'Needles Luer G22 Disposable\r', 0, 0, 0, 0, 0, 0),
(238, 'S', '', 'Needles Luer G23 Disposable\r', 0, 0, 0, 0, 0, 0),
(239, 'S', '', 'Needles Spinal G20x75-120mm\r', 0, 0, 0, 0, 0, 0),
(240, 'S', '', 'Needles Spinal G22x75-120mm\r', 0, 0, 0, 0, 0, 0),
(241, 'S', '', 'Needles Spinal G25x75-120mm\r', 0, 0, 0, 0, 0, 0),
(242, 'S', '', 'Needles Spinal G22x40mm\r', 0, 0, 0, 0, 0, 0),
(243, 'S', '', 'Scalp Vein G19 Infusion set\r', 0, 0, 0, 0, 0, 0),
(244, 'S', '', 'Scalp Vein G21 Infusion set\r', 0, 0, 0, 0, 0, 0),
(245, 'S', '', 'Scalp Vein G23 Infusion set\r', 0, 0, 0, 0, 0, 0),
(246, 'S', '', 'Scalp Vein G25 Infusion set\r', 0, 0, 0, 0, 0, 0),
(247, 'S', '', 'Syringe Feeding/Irrigation 50/60ml\r', 0, 0, 0, 0, 0, 0),
(248, 'S', '', 'Syringe Luer 2ml With Needle Disposable\r', 0, 0, 0, 0, 0, 0),
(249, 'S', '', 'Syringe Luer 10ml With Needle Disposable\r', 0, 0, 0, 0, 0, 0),
(250, 'S', '', 'Syringe Luer 20ml With Needle Disposable\r', 0, 0, 0, 0, 0, 0),
(251, 'S', '', 'Syringe Luer 5ml With Needle Disposable\r', 0, 0, 0, 0, 0, 0),
(252, 'S', '', 'Airway Guedel Size 00\r', 0, 0, 0, 0, 0, 0),
(253, 'S', '', 'Airway Guedel Size 0\r', 0, 0, 0, 0, 0, 0),
(254, 'S', '', 'Airway Guedel Size 1\r', 0, 0, 0, 0, 0, 0),
(255, 'S', '', 'Airway Guedel Size 2\r', 0, 0, 0, 0, 0, 0),
(256, 'S', '', 'Airway Guedel Size 3\r', 0, 0, 0, 0, 0, 0),
(257, 'S', '', 'Eye Pad Sterile\r', 0, 0, 0, 0, 0, 0),
(258, 'S', '', 'Adhesive Tape 2.5cm x 5m\r', 0, 0, 0, 0, 0, 0),
(259, 'S', '', 'Adhesive Tape 7.5cm x 5m\r', 0, 0, 0, 0, 0, 0),
(260, 'S', '', 'cotton Wool 500G\r', 0, 0, 0, 0, 0, 0),
(261, 'S', '', 'Cotton Wool 200G\r', 0, 0, 0, 0, 0, 0),
(262, 'S', '', 'Elastic Bandage 10cm x 4.5m\r', 0, 0, 0, 0, 0, 0),
(263, 'S', '', 'Elastic Bandage 7.5cm x 4.5m\r', 0, 0, 0, 0, 0, 0),
(264, 'S', '', 'Gauze Bandage 7.5cm x 3.65-4m\r', 0, 0, 0, 0, 0, 0),
(265, 'S', '', 'Gauze Bandage 10cm x 4m\r', 0, 0, 0, 0, 0, 0),
(266, 'S', '', 'Gauze Pads Non Sterile 10cm x 10cm\r', 0, 0, 0, 0, 0, 0),
(267, 'S', '', 'Gauze Pads  Sterile 10cm x 10cm\r', 0, 0, 0, 0, 0, 0),
(268, 'S', '', 'Gauze Hydrophylic 90cm x 91cm\r', 0, 0, 0, 0, 0, 0),
(269, 'S', '', 'Plaster of Paris 10cm\r', 0, 0, 0, 0, 0, 0),
(270, 'S', '', 'Plaster of Paris 15cm\r', 0, 0, 0, 0, 0, 0),
(271, 'S', '', 'Catheter Foley CH20 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(272, 'S', '', 'Catheter Foley CH8 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(273, 'S', '', 'Catheter Foley CH10 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(274, 'S', '', 'Catheter Foley CH12 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(275, 'S', '', 'Catheter Foley CH14 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(276, 'S', '', 'Catheter Foley CH16 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(277, 'S', '', 'Catheter Foley CH18 3 Way Balloon\r', 0, 0, 0, 0, 0, 0),
(278, 'S', '', 'Catheter Stopper for All sizes\r', 0, 0, 0, 0, 0, 0),
(279, 'S', '', 'Nasogastric Tube G5 (Children)\r', 0, 0, 0, 0, 0, 0),
(280, 'S', '', 'Nasogastric Tube G8 (Children)\r', 0, 0, 0, 0, 0, 0),
(281, 'S', '', 'Nasogastric Tube G6 (Children)\r', 0, 0, 0, 0, 0, 0),
(282, 'S', '', 'Nasogastric Tube G10 (Children)\r', 0, 0, 0, 0, 0, 0),
(283, 'S', '', 'Nasogastric Tube G14 (Children)\r', 0, 0, 0, 0, 0, 0),
(284, 'S', '', 'Nasogastric Tube G16 (Children)\r', 0, 0, 0, 0, 0, 0),
(285, 'S', '', 'Rectal Tube CH24\r', 0, 0, 0, 0, 0, 0),
(286, 'S', '', 'Rectal Tube CH26\r', 0, 0, 0, 0, 0, 0),
(287, 'S', '', 'Suction Catheter Size 6 Disposable\r', 0, 0, 0, 0, 0, 0),
(288, 'S', '', 'Suction Catheter Size 8 Disposable\r', 0, 0, 0, 0, 0, 0),
(289, 'S', '', 'Suction Catheter Size 16 Disposable\r', 0, 0, 0, 0, 0, 0),
(290, 'S', '', 'Suction Catheter Size 12 Disposable\r', 0, 0, 0, 0, 0, 0),
(291, 'S', '', 'Suction Catheter Size 14 Disposable\r', 0, 0, 0, 0, 0, 0),
(292, 'S', '', 'Suction Catheter Size 10 Disposable\r', 0, 0, 0, 0, 0, 0),
(293, 'S', '', 'Gloves Domestic\r', 0, 0, 0, 0, 0, 0),
(294, 'S', '', 'Gloves High risk non sterile Medium\r', 0, 0, 0, 0, 0, 0),
(295, 'S', '', 'Gloves Gynaecological 7.5-8\r', 0, 0, 0, 0, 0, 0),
(296, 'S', '', 'Gloves Non Sterile Medium Disposable\r', 0, 0, 0, 0, 0, 0),
(297, 'S', '', 'Gloves Non Sterile Large Disposable\r', 0, 0, 0, 0, 0, 0),
(298, 'S', '', 'Gloves Surgical Sterile 6\r', 0, 0, 0, 0, 0, 0),
(299, 'S', '', 'Gloves Surgical Sterile 6.5\r', 0, 0, 0, 0, 0, 0),
(300, 'S', '', 'Gloves Surgical Sterile 7\r', 0, 0, 0, 0, 0, 0),
(301, 'S', '', 'Gloves Surgical Sterile 7.5\r', 0, 0, 0, 0, 0, 0),
(302, 'S', '', 'Gloves Surgical Sterile 8\r', 0, 0, 0, 0, 0, 0),
(303, 'S', '', 'Gloves Surgical Sterile 8.5\r', 0, 0, 0, 0, 0, 0),
(304, 'S', '', 'Tongue depressor Disposable\r', 0, 0, 0, 0, 0, 0),
(305, 'S', '', 'Bedpan Plastic Autoclavable\r', 0, 0, 0, 0, 0, 0),
(306, 'S', '', 'Bedpan Stainless Steel\r', 0, 0, 0, 0, 0, 0),
(307, 'S', '', 'Body Bag 70 x 215cm  (Adult)\r', 0, 0, 0, 0, 0, 0),
(308, 'S', '', 'Bowl Without Lid 7 x 12cm stainless steel\r', 0, 0, 0, 0, 0, 0),
(309, 'S', '', 'Bowl Without Lid 8 x 16cm stainless steel\r', 0, 0, 0, 0, 0, 0),
(310, 'S', '', 'Bowl Without Lid 10 x 24cm stainless steel\r', 0, 0, 0, 0, 0, 0),
(311, 'S', '', 'Air ring set 43x15cm, rubber with pump\r', 0, 0, 0, 0, 0, 0),
(312, 'S', '', 'Colostomy Bag closed 30mm size 2\r', 0, 0, 0, 0, 0, 0),
(313, 'S', '', 'Colostomy Bag closed 30mm size 3\r', 0, 0, 0, 0, 0, 0),
(314, 'S', '', 'Colostomy Bag open re-usable 35mm\r', 0, 0, 0, 0, 0, 0),
(315, 'S', '', 'Ear syringe rubber 60ml\r', 0, 0, 0, 0, 0, 0),
(316, 'S', '', 'First Aid kit\r', 0, 0, 0, 0, 0, 0),
(317, 'S', '', 'Gallipot stainless steel 300ml/15cm\r', 0, 0, 0, 0, 0, 0),
(318, 'S', '', 'Gallipot stainless steel 200ml/10cm\r', 0, 0, 0, 0, 0, 0),
(319, 'S', '', 'Hot water Bottle 2Ltr\r', 0, 0, 0, 0, 0, 0),
(320, 'S', '', 'Instrument Box With Lid 20x10x5cm\r', 0, 0, 0, 0, 0, 0),
(321, 'S', '', 'Instrument Tray 30 x 20 x 2cm\r', 0, 0, 0, 0, 0, 0),
(322, 'S', '', 'Irrigation can with accessories\r', 0, 0, 0, 0, 0, 0),
(323, 'S', '', 'Kidney Dish stainless Steel 24cm\r', 0, 0, 0, 0, 0, 0),
(324, 'S', '', 'Kidney Dish stainless Steel 20cm\r', 0, 0, 0, 0, 0, 0),
(325, 'S', '', 'Kidney Dish Polypropylene 24cm\r', 0, 0, 0, 0, 0, 0),
(326, 'S', '', 'Mackintosh Plastic (Apron) per 1m\r', 0, 0, 0, 0, 0, 0),
(327, 'S', '', 'Mackintosh Rubber Brown (sheeting) per 1m\r', 0, 0, 0, 0, 0, 0),
(328, 'S', '', 'Measuring Cup Graduated 25ml\r', 0, 0, 0, 0, 0, 0),
(329, 'S', '', 'Neck Support Small\r', 0, 0, 0, 0, 0, 0),
(330, 'S', '', 'Neck Support Medium\r', 0, 0, 0, 0, 0, 0),
(331, 'S', '', 'Neck Support Large\r', 0, 0, 0, 0, 0, 0),
(332, 'S', '', 'Spoon Medicine 5ml\r', 0, 0, 0, 0, 0, 0),
(333, 'S', '', 'Apron Plastic Re-usable\r', 0, 0, 0, 0, 0, 0),
(334, 'S', '', 'Apron Plastic Re-usable local\r', 0, 0, 0, 0, 0, 0),
(335, 'S', '', 'Apron Polythene Disp Non Sterile\r', 0, 0, 0, 0, 0, 0),
(336, 'S', '', 'Razor Blades Disposable 5pc\r', 0, 0, 0, 0, 0, 0),
(337, 'S', '', 'Stethoscope Foetal Metal\r', 0, 0, 0, 0, 0, 0),
(338, 'S', '', 'Stethoscope Foetal Wood\r', 0, 0, 0, 0, 0, 0),
(339, 'S', '', 'Surgical Brush (Scrubbing)\r', 0, 0, 0, 0, 0, 0),
(340, 'S', '', 'Surgical Mop 12 x 15\r', 0, 0, 0, 0, 0, 0),
(341, 'S', '', 'Tablet Counting Tray\r', 0, 0, 0, 0, 0, 0),
(342, 'S', '', 'Toilet Paper Rolls\r', 0, 0, 0, 0, 0, 0),
(343, 'S', '', 'Traction Kit Children\r', 0, 0, 0, 0, 0, 0),
(344, 'S', '', 'Traction Kit Adult\r', 0, 0, 0, 0, 0, 0),
(345, 'S', '', 'Thermometer Clinical Flat Type\r', 0, 0, 0, 0, 0, 0),
(346, 'S', '', 'Thermometer Clinical Prismatic Type\r', 0, 0, 0, 0, 0, 0),
(347, 'S', '', 'Umbilical Cord Tie non sterile 100m\r', 0, 0, 0, 0, 0, 0),
(348, 'S', '', 'Umbilical Cord Tie sterile 22m\r', 0, 0, 0, 0, 0, 0),
(349, 'S', '', 'Urinal 1Ltr / 2Ltr\r', 0, 0, 0, 0, 0, 0),
(350, 'S', '', 'Urine Collecting Bag sterile 2Ltr\r', 0, 0, 0, 0, 0, 0),
(351, 'S', '', 'Insectcide Spray 400g\r', 0, 0, 0, 0, 0, 0),
(352, 'S', '', 'Mosquito Net Impregnated Medium\r', 0, 0, 0, 0, 0, 0),
(353, 'S', '', 'Mosquito Net Impregnated Large\r', 0, 0, 0, 0, 0, 0),
(354, 'S', '', 'Mosquito Net Non Impregnated Medium\r', 0, 0, 0, 0, 0, 0),
(355, 'S', '', 'Mosquito Net Non Impregnated Large\r', 0, 0, 0, 0, 0, 0),
(356, 'S', '', 'Mosquito Net Impregnation Tablet\r', 0, 0, 0, 0, 0, 0),
(357, 'S', '', 'Mosquito Net Impregnation Liquid 500ml\r', 0, 0, 0, 0, 0, 0),
(358, 'S', '', 'Mosquito Wall spray Powder 80g\r', 0, 0, 0, 0, 0, 0),
(359, 'S', '', 'Handle for surgical blade No 3\r', 0, 0, 0, 0, 0, 0),
(360, 'S', '', 'Handle for surgical blade No 4\r', 0, 0, 0, 0, 0, 0),
(361, 'S', '', 'Surgical Blades No 20\r', 0, 0, 0, 0, 0, 0),
(362, 'S', '', 'Surgical Blades No 21\r', 0, 0, 0, 0, 0, 0),
(363, 'S', '', 'Surgical Blades No 22\r', 0, 0, 0, 0, 0, 0),
(364, 'S', '', 'Surgical Blades No 23\r', 0, 0, 0, 0, 0, 0),
(365, 'S', '', 'Needle suture No 5 round\r', 0, 0, 0, 0, 0, 0),
(366, 'S', '', 'Needle suture No 5 cutting\r', 0, 0, 0, 0, 0, 0),
(367, 'S', '', 'Needle suture No 6 Round\r', 0, 0, 0, 0, 0, 0),
(368, 'S', '', 'Suture Cutgut Chromic (0)\r', 0, 0, 0, 0, 0, 0),
(369, 'S', '', 'Suture Cutgut Chromic (2) RN22240TH\r', 0, 0, 0, 0, 0, 0),
(370, 'S', '', 'Suture Cutgut Chromic (2/0) RN22230TH\r', 0, 0, 0, 0, 0, 0),
(371, 'S', '', 'Suture Cutgut Chromic (3/0) RN2325TF\r', 0, 0, 0, 0, 0, 0),
(372, 'S', '', 'Suture Cutgut Plain (2/0) RN1230TF\r', 0, 0, 0, 0, 0, 0),
(373, 'S', '', 'Suture Silk (1) S595\r', 0, 0, 0, 0, 0, 0),
(374, 'S', '', 'Suture Silk (2/0) RN5230TF\r', 0, 0, 0, 0, 0, 0),
(375, 'S', '', 'Suture PGA (3/0) RN3330TF\r', 0, 0, 0, 0, 0, 0),
(376, 'S', '', 'Lead Apron 100cmx60cm\r', 0, 0, 0, 0, 0, 0),
(377, 'S', '', 'X-Ray Developer 2.6kg for 22.5Ltr\r', 0, 0, 0, 0, 0, 0),
(378, 'S', '', 'X-Ray Film 18x24cm\r', 0, 0, 0, 0, 0, 0),
(379, 'S', '', 'X-Ray Film 20x40cm\r', 0, 0, 0, 0, 0, 0),
(380, 'S', '', 'X-Ray Film 24x30cm\r', 0, 0, 0, 0, 0, 0),
(381, 'S', '', 'X-Ray Film 30x40cm\r', 0, 0, 0, 0, 0, 0),
(382, 'S', '', 'X-Ray Film 35x35cm\r', 0, 0, 0, 0, 0, 0),
(383, 'S', '', 'X-Ray Film 43x35cm\r', 0, 0, 0, 0, 0, 0),
(384, 'S', '', 'X-Ray Film Cassette  18x24cm with screen\r', 0, 0, 0, 0, 0, 0),
(385, 'S', '', 'X-Ray Film Cassette  24x30cm with screen\r', 0, 0, 0, 0, 0, 0),
(386, 'S', '', 'X-Ray Film Cassette  30x40cm with screen\r', 0, 0, 0, 0, 0, 0),
(387, 'S', '', 'X-Ray Film Cassette  35x35cm with screen\r', 0, 0, 0, 0, 0, 0),
(388, 'S', '', 'X-Ray Film Cassette  35x43cm with screen\r', 0, 0, 0, 0, 0, 0),
(389, 'S', '', 'X-Ray Fixer 3.3kg for 22.5 Ltr\r', 0, 0, 0, 0, 0, 0),
(390, 'S', '', 'Barium Sulphate for X-Ray 1kg\r', 0, 0, 0, 0, 0, 0),
(391, 'S', '', 'Diatrizoate Meglumin Sod 76% 20ml Amp\r', 0, 0, 0, 0, 0, 0),
(392, 'K', '', 'Anti Serum B 10ml\r', 0, 0, 0, 0, 0, 0),
(393, 'K', '', 'Anti Serum AB 10ml\r', 0, 0, 0, 0, 0, 0),
(394, 'K', '', 'Anti Serum D 10ml\r', 0, 0, 0, 0, 0, 0),
(395, 'K', '', 'Creatinine 200ml (Calorimetric)\r', 0, 0, 0, 0, 0, 0),
(396, 'K', '', 'Glucose GOD PAD  6 x 100ml (Colorimetric)\r', 0, 0, 0, 0, 0, 0),
(397, 'K', '', 'Glucose Test Strips (Hyloguard)\r', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRLOT`
--

CREATE TABLE `MEDICALDSRLOT` (
  `LT_ID_A` varchar(50) NOT NULL,
  `LT_PREP_DATE` datetime NOT NULL,
  `LT_DUE_DATE` datetime NOT NULL,
  `LT_COST` double DEFAULT NULL,
  `LT_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRSTOCKMOV`
--

CREATE TABLE `MEDICALDSRSTOCKMOV` (
  `MMV_ID` int(11) NOT NULL,
  `MMV_MDSR_ID` int(11) NOT NULL,
  `MMV_WRD_ID_A` char(1) DEFAULT NULL,
  `MMV_MMVT_ID_A` varchar(10) NOT NULL,
  `MMV_LT_ID_A` varchar(50) DEFAULT NULL,
  `MMV_DATE` datetime NOT NULL,
  `MMV_QTY` float NOT NULL DEFAULT '0',
  `MMV_FROM` int(11) DEFAULT '0',
  `MMV_LOCK` int(11) NOT NULL DEFAULT '0',
  `MMV_REFNO` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRSTOCKMOVTYPE`
--

CREATE TABLE `MEDICALDSRSTOCKMOVTYPE` (
  `MMVT_ID_A` varchar(10) NOT NULL,
  `MMVT_DESC` varchar(50) NOT NULL,
  `MMVT_TYPE` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MEDICALDSRSTOCKMOVTYPE`
--

INSERT INTO `MEDICALDSRSTOCKMOVTYPE` (`MMVT_ID_A`, `MMVT_DESC`, `MMVT_TYPE`) VALUES
('charge', 'Charge', '+\r'),
('discharge', 'Discharge', '-\r');

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRSTOCKMOVWARD`
--

CREATE TABLE `MEDICALDSRSTOCKMOVWARD` (
  `MMVN_ID` int(10) NOT NULL,
  `MMVN_WRD_ID_A` char(1) NOT NULL,
  `MMVN_DATE` datetime NOT NULL,
  `MMVN_IS_PATIENT` tinyint(1) NOT NULL,
  `MMVN_PAT_ID` int(11) DEFAULT NULL,
  `MMVN_PAT_AGE` smallint(6) DEFAULT NULL,
  `MMVN_PAT_WEIGHT` float DEFAULT NULL,
  `MMVN_DESC` varchar(100) NOT NULL,
  `MMVN_MDSR_ID` varchar(100) NOT NULL,
  `MMVN_MDSR_QTY` float NOT NULL,
  `MMVN_MDSR_UNITS` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRTYPE`
--

CREATE TABLE `MEDICALDSRTYPE` (
  `MDSRT_ID_A` char(1) NOT NULL,
  `MDSRT_DESC` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MEDICALDSRTYPE`
--

INSERT INTO `MEDICALDSRTYPE` (`MDSRT_ID_A`, `MDSRT_DESC`) VALUES
('D', 'Drugs\r'),
('K', 'Chemical\r'),
('L', 'Laboratory\r'),
('S', 'Surgery\r');

-- --------------------------------------------------------

--
-- Table structure for table `MEDICALDSRWARD`
--

CREATE TABLE `MEDICALDSRWARD` (
  `MDSRWRD_WRD_ID_A` char(1) NOT NULL,
  `MDSRWRD_MDSR_ID` int(11) NOT NULL,
  `MDSRWRD_IN_QTI` float DEFAULT '0',
  `MDSRWRD_OUT_QTI` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `MENUITEM`
--

CREATE TABLE `MENUITEM` (
  `MNI_ID_A` varchar(50) NOT NULL DEFAULT '',
  `MNI_BTN_LABEL` varchar(50) NOT NULL DEFAULT '',
  `MNI_LABEL` varchar(50) NOT NULL DEFAULT '',
  `MNI_TOOLTIP` varchar(100) DEFAULT NULL,
  `MNI_SHORTCUT` char(1) DEFAULT NULL,
  `MNI_SUBMENU` varchar(50) NOT NULL DEFAULT '',
  `MNI_CLASS` varchar(100) NOT NULL DEFAULT '',
  `MNI_IS_SUBMENU` char(1) NOT NULL DEFAULT 'N',
  `MNI_POSITION` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MENUITEM`
--

INSERT INTO `MENUITEM` (`MNI_ID_A`, `MNI_BTN_LABEL`, `MNI_LABEL`, `MNI_TOOLTIP`, `MNI_SHORTCUT`, `MNI_SUBMENU`, `MNI_CLASS`, `MNI_IS_SUBMENU`, `MNI_POSITION`) VALUES
('admission', 'angal.menu.btn.admission', 'angal.menu.admission', 'x', 'A', 'main', 'org.isf.admission.gui.AdmittedPatientBrowser', 'N', 5),
('admtype', 'angal.menu.btn.admtype', 'angal.menu.admtype', 'x', 'A', 'types', 'org.isf.admtype.gui.AdmissionTypeBrowser', 'N', 0),
('deliverytype', 'angal.menu.btn.deliverytype', 'angal.menu.deliverytype', 'x', 'T', 'types', 'org.isf.dlvrtype.gui.DeliveryTypeBrowser', 'N', 2),
('delresulttype', 'angal.menu.btn.delresulttype', 'angal.menu.delresulttype', 'x', 'R', 'types', 'org.isf.dlvrrestype.gui.DeliveryResultTypeBrowser', 'N', 3),
('disctype', 'angal.menu.btn.disctype', 'angal.menu.disctype', 'x', 'D', 'types', 'org.isf.disctype.gui.DischargeTypeBrowser', 'N', 1),
('disease', 'angal.menu.btn.disease', 'angal.menu.disease', 'x', 'D', 'generaldata', 'org.isf.disease.gui.DiseaseBrowser', 'N', 3),
('diseaselist', 'angal.menu.btn.diseaselist', 'angal.menu.diseaselist', 'x', 'D', 'printing', 'org.isf.stat.gui.DiseasesListLauncher', 'N', 2),
('diseasetype', 'angal.menu.btn.diseasetype', 'angal.menu.diseasetype', 'x', 'D', 'types', 'org.isf.distype.gui.DiseaseTypeBrowser', 'N', 4),
('examlist1', 'angal.menu.btn.examlist1', 'angal.menu.examlist1', 'x', 'E', 'printing', 'org.isf.stat.gui.ExamsList1Launcher', 'N', 1),
('exams', 'angal.menu.btn.exams', 'angal.menu.exams', 'x', 'E', 'generaldata', 'org.isf.exa.gui.ExamBrowser', 'N', 4),
('examtype', 'angal.menu.btn.examtype', 'angal.menu.examtype', 'x', 'E', 'types', 'org.isf.exatype.gui.ExamTypeBrowser', 'N', 5),
('exit', 'angal.menu.btn.exit', 'angal.menu.exit', 'x', 'E', 'file', 'none', 'N', 0),
('file', 'angal.menu.btn.file', 'angal.menu.file', 'x', 'F', 'main', 'none', 'Y', 0),
('generaldata', 'angal.menu.btn.generaldata', 'angal.menu.generaldata', 'x', 'G', 'main', 'none', 'Y', 1),
('groups', 'angal.menu.btn.groups', 'angal.menu.groups', 'x', 'G', 'users', 'org.isf.menu.gui.UserGroupBrowsing', 'N', 1),
('help', 'angal.menu.btn.help', 'angal.menu.help', 'x', 'H', 'main', 'org.isf.help.HelpViewer', 'N', 8),
('hospital', 'angal.menu.btn.hospital', 'angal.menu.hospital', 'x', 'H', 'generaldata', 'org.isf.hospital.gui.HospitalBrowser', 'N', 1),
('laboratory', 'angal.menu.btn.laboratory', 'angal.menu.laboratory', 'x', 'L', 'main', 'org.isf.lab.gui.LabBrowser', 'N', 4),
('medicals', 'angal.menu.btn.medicals', 'angal.menu.medicals', 'x', 'P', 'pharmacy', 'org.isf.medicals.gui.MedicalBrowser', 'N', 0),
('medicalstock', 'angal.menu.btn.medicalstock', 'angal.menu.medicalstock', 'x', 'S', 'pharmacy', 'org.isf.medicalstock.gui.MovStockBrowser', 'N', 1),
('medicalstype', 'angal.menu.btn.medicalstype', 'angal.menu.medicalstype', 'x', 'M', 'types', 'org.isf.medtype.gui.MedicalTypeBrowser', 'N', 7),
('medstockmovtype', 'angal.menu.btn.medstockmovtype', 'angal.menu.medstockmovtype', 'x', 'S', 'types', 'org.isf.medstockmovtype.gui.MedicaldsrstockmovTypeBrowser', 'N', 6),
('opd', 'angal.menu.btn.opd', 'angal.menu.opd', 'x', 'O', 'main', 'org.isf.opd.gui.OpdBrowser', 'N', 2),
('operation', 'angal.menu.btn.operation', 'angal.menu.operation', 'x', 'O', 'generaldata', 'org.isf.operation.gui.OperationBrowser', 'N', 5),
('operationtype', 'angal.menu.btn.operationtype', 'angal.menu.operationtype', 'x', 'O', 'types', 'org.isf.opetype.gui.OperationTypeBrowser', 'N', 8),
('pharmacy', 'angal.menu.btn.pharmacy', 'angal.menu.pharmacy', 'x', 'P', 'main', 'none', 'Y', 3),
('pretreatmenttype', 'angal.menu.btn.pretreatmenttype', 'angal.menu.pretreatmenttype', 'x', 'P', 'types', 'org.isf.pregtreattype.gui.PregnantTreatmentTypeBrowser', 'N', 9),
('printing', 'angal.menu.btn.printing', 'angal.menu.printing', 'x', 'R', 'main', 'none', 'Y', 7),
('statistics', 'angal.menu.btn.statistics', 'angal.menu.statistics', 'x', 'T', 'main', 'org.isf.stat.reportlauncher.gui.ReportLauncher', 'N', 6),
('types', 'angal.menu.btn.types', 'angal.menu.types', 'x', 'T', 'generaldata', 'none', 'Y', 0),
('users', 'angal.menu.btn.users', 'angal.menu.users', 'x', 'U', 'file', 'none', 'Y', 1),
('usersusers', 'angal.menu.btn.usersusers', 'angal.menu.usersusers', 'x', 'U', 'users', 'org.isf.menu.gui.UserBrowsing', 'N', 0),
('vaccine', 'angal.menu.btn.vaccine', 'angal.menu.vaccine', 'x', 'V', 'generaldata', 'org.isf.vaccine.gui.VaccineBrowser', 'N', 6),
('ward', 'angal.menu.btn.ward', 'angal.menu.ward', 'x', 'W', 'generaldata', 'org.isf.ward.gui.WardBrowser', 'N', 2),
('medicalsward', 'angal.menu.btn.medicalsward', 'angal.menu.medicalsward', 'x', 'W', 'pharmacy', 'org.isf.medicalstockward.gui.WardPharmacy', 'N', 2),
('agetype', 'angal.menu.btn.agetype', 'angal.menu.agetype', 'x', 'G', 'types', 'org.isf.agetype.gui.AgeTypeBrowser', 'N', 11),
('priceslists', 'angal.menu.btn.priceslists', 'angal.menu.priceslists', 'x', 'P', 'generaldata', 'org.isf.priceslist.gui.PricesBrowser', 'N', 7),
('otherprices', 'angal.menu.btn.otherprices', 'angal.menu.otherprices', 'x', 'H', 'types', 'org.isf.pricesothers.gui.PricesOthersBrowser', 'N', 10),
('accounting', 'angal.menu.btn.accounting', 'angal.menu.accounting', 'x', 'C', 'main', 'none', 'Y', 5),
('newbill', 'angal.menu.btn.newbill', 'angal.menu.newbill', 'x', 'N', 'accounting', 'org.isf.accounting.gui.PatientBillEdit', 'N', 0),
('billsmanager', 'angal.menu.btn.billsmanager', 'angal.menu.billsmanager', 'x', 'M', 'accounting', 'org.isf.accounting.gui.BillBrowser', 'N', 1),
('data', 'angal.admission.data', 'angal.admission.data', 'x', 'D', 'admission', 'none', 'N', 3),
('btnadmnew', 'angal.admission.newpatient', 'angal.admission.newpatient', 'x', 'N', 'admission', 'none', 'N', 0),
('btnadmedit', 'angal.admission.editpatient', 'angal.admission.editpatient', 'x', 'E', 'admission', 'none', 'N', 1),
('btnadmopd', 'angal.admission.opd', 'angal.admission.admission', 'x', 'O', 'admission', 'none', 'N', 5),
('btnadmadm', 'angal.admission.admission', 'angal.admission.admission', 'x', 'A', 'admission', 'none', 'N', 4),
('btnadmdel', 'angal.admission.deletepatient', 'angal.admission.deletepatient', 'x', 'L', 'admission', 'none', 'N', 2),
('btnadmmer', 'angal.admission.merge', 'angal.admission.merge', 'x', 'L', 'admission', 'none', 'N', 2),
('btndataeditpat', 'angal.admission.editpatient', 'angal.admission.editpatient', 'x', 'P', 'data', 'none', 'N', 0),
('btndatamalnut', 'angal.admission.malnutritioncontrol', 'angal.admission.malnutritioncontrol', 'x', 'M', 'data', 'none', 'N', 3),
('btndatadel', 'angal.common.deletem', 'angal.common.deletem', 'x', 'D', 'data', 'none', 'N', 2),
('btndataedit', 'angal.common.editm', 'angal.common.editm', 'x', 'E', 'data', 'none', 'N', 1),
('btnopddel', 'angal.common.delete', 'angal.common.delete', 'x', 'D', 'opd', 'none', 'N', 2),
('btnopdedit', 'angal.common.edit', 'angal.common.edit', 'x', 'E', 'opd', 'none', 'N', 1),
('btnopdnew', 'angal.common.new', 'angal.common.new', 'x', 'N', 'opd', 'none', 'N', 0),
('btnpharmaceuticaldel', 'angal.common.delete', 'angal.common.delete', 'x', 'D', 'medicals', 'none', 'N', 2),
('btnpharmaceuticaledit', 'angal.common.edit', 'angal.common.edit', 'x', 'E', 'medicals', 'none', 'N', 1),
('btnpharmaceuticalnew', 'angal.common.new', 'angal.common.new', 'x', 'N', 'medicals', 'none', 'N', 0),
('btnlaboratorydel', 'angal.common.delete', 'angal.common.delete', 'x', 'D', 'laboratory', 'none', 'N', 2),
('btnlaboratoryedit', 'angal.common.edit', 'angal.common.edit', 'x', 'E', 'laboratory', 'none', 'N', 1),
('btnlaboratorynew', 'angal.common.new', 'angal.common.new', 'x', 'N', 'laboratory', 'none', 'N', 0),
('btnadmtherapy', 'angal.admission.therapy', 'angal.admission.therapy', 'x', 'T', 'admission', 'none', 'N', 6),
('btnbillnew', 'angal.billbrowser.newbill', 'angal.billbrowser.newbill', 'x', 'N', 'billsmanager', 'none', 'N', 0),
('btnbilledit', 'angal.billbrowser.editbill', 'angal.billbrowser.editbill', 'x', 'N', 'billsmanager', 'none', 'N', 1),
('btnbilldelete', 'angal.billbrowser.deletebill', 'angal.billbrowser.deletebill', 'x', 'N', 'billsmanager', 'none', 'N', 2),
('btnbillreport', 'angal.billbrowser.report', 'angal.billbrowser.report', 'x', 'N', 'billsmanager', 'none', 'N', 3),
('vaccinetype', 'angal.menu.btn.vaccinetype', 'angal.menu.vaccinetype', 'x', 'V', 'types', 'org.isf.vactype.gui.VaccineTypeBrowser', 'N', 12),
('patientvaccine', 'angal.menu.btn.patientvaccine', 'angal.menu.patientvaccine', 'x', 'V', 'main', 'org.isf.patvac.gui.PatVacBrowser', 'N', 5),
('btnpatientvaccinenew', 'angal.common.new', 'angal.common.new', 'x', 'N', 'patientvaccine', 'none', 'N', 0),
('btnpatientvaccineedit', 'angal.common.edit', 'angal.common.edit', 'x', 'E', 'patientvaccine', 'none', 'N', 1),
('btnpatientvaccinedel', 'angal.common.delete', 'angal.common.delete', 'x', 'D', 'patientvaccine', 'none', 'N', 2),
('communication', 'angal.menu.btn.chat', 'angal.menu.chat', 'x', 'm', 'main', 'org.isf.xmpp.gui.CommunicationFrame', 'N', 9),
('btnadmbill', 'angal.menu.btn.btnadmbill', 'angal.menu.btnadmbill', 'x', 'R', 'admission', 'none', 'N', 4),
('btnbillreceipt', 'angal.menu.btn.btnbillreceipt', 'angal.menu.btnbillreceipt', 'x', 'R', 'billsmanager', 'none', 'N', 4),
('btnadmpatientfolder', 'angal.admission.patientfolder', 'angal.admission.patientfolder', 'x', 'P', 'admission', 'none', 'Y', 6),
('btnopdnewexamination', 'angal.opd.examination', 'angal.opd.examination', 'x', 'A', 'btnopdnew', 'none', 'N', 1),
('btnopdeditexamination', 'angal.opd.examination', 'angal.opd.examination', 'x', 'A', 'btnopdedit', 'none', 'N', 1),
('btnadmadmexamination', 'angal.admission.examination', 'angal.admission.examination', 'x', 'A', 'btnadmadm', 'none', 'N', 1),
('btnadmexamination', 'angal.admission.examination', 'angal.admission.examination', 'x', 'A', 'admission', 'none', 'N', 1),
('supplier', 'angal.menu.btn.supplier', 'angal.menu.supplier', 'x', 'S', 'generaldata', 'org.isf.supplier.gui.SupplierBrowser', 'N', 8),
('btnpatfoldopdrpt', 'angal.menu.btn.opdchart', 'angal.menu.opdchart', 'x', 'O', 'btnadmpatientfolder', 'none', 'N', 1),
('btnpatfoldadmrpt', 'angal.menu.btn.admchart', 'angal.menu.admchart', 'x', 'A', 'btnadmpatientfolder', 'none', 'N', 2),
('btnpatfoldpatrpt', 'angal.menu.btn.patreport', 'angal.menu.patreport', 'x', 'R', 'btnadmpatientfolder', 'none', 'N', 3),
('btnpatfolddicom', 'angal.menu.btn.dicom', 'angal.menu.dicom', 'x', 'D', 'btnadmpatientfolder', 'none', 'N', 4),
('smsmanager', 'angal.menu.btn.smsmanager', 'angal.menu.smsmanager', 'x', 'M', 'generaldata', 'org.isf.sms.gui.SmsBrowser', 'N', 9),
('btnpharmstockcharge', 'angal.menu.btn.btnpharmstockcharge', 'angal.menu.btnpharmstockcharge', 'x', 'C', 'medicalstock', 'none', 'N', 1),
('btnpharmstockdischarge', 'angal.menu.btn.btnpharmstockdischarge', 'angal.menu.btnpharmstockdischarge', 'x', 'D', 'medicalstock', 'none', 'N', 2),
('btnmedicalswardreport', 'angal.menu.btn.btnmedicalswardreport', 'angal.menu.btnmedicalswardreport', 'x', 'P', 'medicalsward', 'none', 'N', 2),
('btnmedicalswardexcel', 'angal.menu.btn.btnmedicalswardexcel', 'angal.menu.btnmedicalswardexcel', 'x', 'E', 'medicalsward', 'none', 'N', 3),
('btnmedicalswardrectify', 'angal.menu.btn.btnmedicalswardrectify', 'angal.menu.btnmedicalswardrectify', 'x', 'R', 'medicalsward', 'none', 'N', 1);

-- --------------------------------------------------------

--
-- Table structure for table `OPD`
--

CREATE TABLE `OPD` (
  `OPD_ID` int(11) NOT NULL,
  `OPD_DATE` datetime NOT NULL,
  `OPD_NEW_PAT` char(1) NOT NULL DEFAULT 'N',
  `OPD_DATE_VIS` date NOT NULL,
  `OPD_PROG_YEAR` int(11) NOT NULL,
  `OPD_SEX` char(1) NOT NULL,
  `OPD_AGE` int(11) NOT NULL DEFAULT '0',
  `OPD_DIS_ID_A` varchar(10) DEFAULT NULL,
  `OPD_DIS_ID_A_2` varchar(10) DEFAULT NULL,
  `OPD_DIS_ID_A_3` varchar(10) DEFAULT NULL,
  `OPD_REFERRAL_FROM` varchar(1) DEFAULT NULL,
  `OPD_REFERRAL_TO` varchar(1) DEFAULT NULL,
  `OPD_NOTE` text NOT NULL,
  `OPD_PAT_ID` int(11) DEFAULT NULL,
  `OPD_USR_ID_A` varchar(50) NOT NULL DEFAULT 'admin',
  `OPD_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `OPERATION`
--

CREATE TABLE `OPERATION` (
  `OPE_ID_A` varchar(10) NOT NULL,
  `OPE_OCL_ID_A` char(2) NOT NULL,
  `OPE_DESC` varchar(50) NOT NULL,
  `OPE_STAT` int(11) NOT NULL DEFAULT '0',
  `OPE_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `OPERATION`
--

INSERT INTO `OPERATION` (`OPE_ID_A`, `OPE_OCL_ID_A`, `OPE_DESC`, `OPE_STAT`, `OPE_LOCK`) VALUES
('1', 'OB', 'Caesarian section', 1, 0),
('10', 'GY', 'Acute abdomen', 1, 0),
('11', 'GY', 'Ectopic pregnancy', 1, 0),
('12', 'GY', 'Peritonitis', 1, 0),
('13', 'GY', 'Pelvic abscess', 1, 0),
('14', 'GY', 'Uterine fibroids', 1, 0),
('15', 'GY', 'Ovarian tumours', 1, 0),
('16', 'GY', 'Uterine prolapse', 1, 0),
('17', 'GY', 'Cystocele', 1, 0),
('18', 'MG', 'Circumcision', 1, 0),
('19', 'MG', 'Phimosis', 1, 0),
('2', 'OB', 'Hysterectomy', 1, 0),
('20', 'MG', 'Paraphimosis', 1, 0),
('21', 'MG', 'Dorsal slit-paraphimosis', 1, 0),
('22', 'MG', 'Uretheral stricture-bougienage', 1, 0),
('23', 'MG', 'Hydrocelectomy', 1, 0),
('24', 'MG', 'Testicular tumours', 1, 0),
('25', 'MG', 'Prostatectomy', 1, 0),
('26', 'MG', 'Prostate biopsy', 1, 0),
('27', 'MG', 'Bladder biopsy', 1, 0),
('28', 'AG', 'Hernia (inguinal & femoral)', 1, 0),
('29', 'AG', 'Strangulated', 1, 0),
('3', 'OB', 'Ruptured uterus', 1, 0),
('30', 'AG', 'Non strangulated', 1, 0),
('31', 'AG', 'Epigastrical Hernia', 1, 0),
('32', 'AG', 'Intestinal obstruction', 1, 0),
('33', 'AG', 'Mechanical', 1, 0),
('34', 'AG', 'Volvulus', 1, 0),
('35', 'AG', 'Laparotomy', 1, 0),
('36', 'AG', 'Penetrating abdominal injuries', 1, 0),
('37', 'AG', 'Peritonitis', 1, 0),
('38', 'AG', 'Appendicitis', 1, 0),
('39', 'AG', 'Cholecystitis', 1, 0),
('4', 'OB', 'Injured uterus', 1, 0),
('40', 'AG', 'Abdominal Tumours', 1, 0),
('41', 'OR', 'Reduction of fractures', 1, 0),
('42', 'OR', 'Upper limb', 1, 0),
('43', 'OR', 'Lower limb', 1, 0),
('44', 'OR', 'Osteomyelitis - sequestrectomy', 1, 0),
('45', 'OS', 'Incision & Drainage', 1, 0),
('46', 'OS', 'Debridement', 1, 0),
('47', 'OS', 'Mise -a- plat', 1, 0),
('48', 'OS', 'Surgical toilet  & suture', 1, 0),
('5', 'OB', 'Evacuations', 1, 0),
('6', 'OB', 'Incomplete abortion', 1, 0),
('7', 'OB', 'Septic abortion', 1, 0),
('8', 'OB', 'Dilatation and curettage', 1, 0),
('9', 'OB', 'Repair of vesico-vaginal fistula (vvf)', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `OPERATIONTYPE`
--

CREATE TABLE `OPERATIONTYPE` (
  `OCL_ID_A` char(2) NOT NULL,
  `OCL_DESC` varchar(50) NOT NULL,
  `OCL_TYPE` varchar(20) NOT NULL DEFAULT 'MAJOR'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `OPERATIONTYPE`
--

INSERT INTO `OPERATIONTYPE` (`OCL_ID_A`, `OCL_DESC`, `OCL_TYPE`) VALUES
('AG', 'ABDOMINAL GENERAL SURGERY', 'MAJOR\r'),
('GY', 'GYNECOLOGICAL', 'MAJOR\r'),
('MG', 'MALE GENITOURINARY SYSTEM', 'MAJOR\r'),
('OB', 'OBSTETRICAL', 'MAJOR\r'),
('OR', 'ORTHOPEDICAL', 'MAJOR\r'),
('OS', 'OTHERS: SKIN AND SUBCUTANEOUS', 'MAJOR\r');

-- --------------------------------------------------------

--
-- Table structure for table `PATIENT`
--

CREATE TABLE `PATIENT` (
  `PAT_ID` int(11) NOT NULL,
  `PAT_FNAME` varchar(50) NOT NULL,
  `PAT_SNAME` varchar(50) NOT NULL,
  `PAT_NAME` varchar(100) DEFAULT NULL,
  `PAT_BDATE` date DEFAULT NULL,
  `PAT_AGE` int(11) NOT NULL DEFAULT '0',
  `PAT_AGETYPE` varchar(50) NOT NULL DEFAULT '',
  `PAT_SEX` char(1) NOT NULL,
  `PAT_ADDR` varchar(50) DEFAULT NULL,
  `PAT_CITY` varchar(50) NOT NULL,
  `PAT_NEXT_KIN` varchar(50) DEFAULT NULL,
  `PAT_TELE` varchar(50) DEFAULT NULL,
  `PAT_MOTH_NAME` varchar(50) NOT NULL DEFAULT '',
  `PAT_MOTH` char(1) DEFAULT NULL,
  `PAT_FATH_NAME` varchar(50) NOT NULL DEFAULT '',
  `PAT_FATH` char(1) DEFAULT NULL,
  `PAT_LEDU` char(1) DEFAULT NULL,
  `PAT_ESTA` char(1) DEFAULT NULL,
  `PAT_PTOGE` char(1) DEFAULT NULL,
  `PAT_NOTE` text,
  `PAT_DELETED` char(1) NOT NULL DEFAULT 'N',
  `PAT_LOCK` int(11) NOT NULL DEFAULT '0',
  `PAT_BTYPE` varchar(15) NOT NULL DEFAULT 'Unknown',
  `PAT_PHOTO` blob,
  `PAT_TAXCODE` varchar(30) DEFAULT '',
  `PAT_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PATIENT`
--

INSERT INTO `PATIENT` (`PAT_ID`, `PAT_FNAME`, `PAT_SNAME`, `PAT_NAME`, `PAT_BDATE`, `PAT_AGE`, `PAT_AGETYPE`, `PAT_SEX`, `PAT_ADDR`, `PAT_CITY`, `PAT_NEXT_KIN`, `PAT_TELE`, `PAT_MOTH_NAME`, `PAT_MOTH`, `PAT_FATH_NAME`, `PAT_FATH`, `PAT_LEDU`, `PAT_ESTA`, `PAT_PTOGE`, `PAT_NOTE`, `PAT_DELETED`, `PAT_LOCK`, `PAT_BTYPE`, `PAT_PHOTO`, `PAT_TAXCODE`, `PAT_TIMESTAMP`) VALUES
(0, 'Patient', 'Null', 'Null Patient', NULL, 0, '-', 'U', '-', '-', '-', '-', '-', 'U', '-', 'U', NULL, 'U', 'U', '-', 'Y', 0, '-', NULL, '-', '2018-08-15 15:42:55'),
(1, 'Brian', 'Bet', 'Brian Bet', '2001-08-28', 16, '', 'M', '', '', '', '+39', '', 'U', '', 'U', NULL, 'U', 'U', '', 'N', 0, 'A+', NULL, '30492992', '2018-08-15 16:03:36'),
(2, 'Simon', 'Waweru', 'Simon Waweru', '2010-08-10', 8, '', 'M', '', '', '', '+254717084613', '', 'U', '', 'U', NULL, 'N', 'U', '', 'N', 0, 'Unknown', 0xffd8ffe000104a46494600010200000100010000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc000110800a000a003012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f2413109b768e9d6aaca0f19e6a6b3de253e726315659e0cb063f8d6668563770c76fb401baab19fcc390a7029d25ba349bd5b23d2862029001fca802a4932bca1738addd3182da488dc806b09766d62c39ad7d2e371a64af82431e2982323510a976acbeb9fd6bd0a6b85bfd22ca49530102f5e95e79a944d1c8a58f5cd759a4dd9b8d3628705880303d287b12b736b5d4f32c32180014e31f4ae37cf786d159be602bb86d3cdfdb986593c88d1492e4647a7afd69d67a7db5908d11636f2db6ee78baf4e0fe5d7a723d7140d9e737055edd9d47539cd55964df0e3dabd6f62891e48da4002f4030a3a93c76fceb0e51651ce0cab1b283ced40dd3a820753cd023cd715340a1b77b0aeeaf74ab5beb48e18d1973d1c2018efc7e75852f862ee1dcd6c44ca78c6403f5f7155712325ad3ce45903823a114d9adcc7103b8e3d0d4b124b04af1ba9574382a4f4a73e673b7238a9b94360c20054735a90dd4498620e71cd5101638fa8cd2f94ed1f98a3a75a065f7bd0e7216abb1dd264f5aa9e76dc034f2e48cd202dcb761dc2229c91da9b3594853ccdf81e86a281191f7120b54931925754c9e68023843ab018c8a75c173c28c54b224968bb94ee23b62a97dbe59243940bf853013ecc16262edd6ba3b6558741507b9c8ac06fdea8c9ea6ba49e155d2a3553c6deb4811cfdcdb1d46fadeda1382e7058f451dc9f6aedf49d3e2b4b38e2b71bfe5cb1390c463be07cbffebea6b1747852d2069d9d449271bca8385f94e393df27ebf85755a6c53bdb32bbacb183f2e72db87183f5033f8fd299231ee22b458f7420e4f0318033ea58f1d3d73ed54f52d4d3cb31efc4bdda003ae7a01cfa7be3b9f5d39f47401586fcb13b4ecced23a1cf1d33d7eb59d75a4282bbdce41f9368fa74fd3f2a00c1935199bca1134810ff0016c0013d07407d3bfad42da8ba9197dbbd70e401f2fe3c9ebeb572f2d3cb0cf86057ee804e7f0fa62b1e5b067230473c8c8ebeff005ff1a00b56da899cc619b7465b2327664f4e3001e09f5efd0d6d5b5c1680879238d3a30dbf740e8323bf27b77ae59b4f959542b191b7600c73fa56b695be080c40c4e50956593243124f23f95003f53d2619ac90a9930bb8acd81b8007b8cf231cd7333452da5c34138c38f4e87d3f3aeb1e64f260648d151640ac98de074271f4c9f4c7d09aa1acc265f98c7b2587e561c91db81df1dc7b50346122b3310066adab98e1d9ebd6a3531c0e18b658fbd48eea46e0290cad241e61cf34a9114f94fddf5a97ed046001521259707a9140128b233287c95039cd5954822f98f245462f005d8060015464371311818140ef627b8ba05f013ad55923662188c66924cc0c0bf27a54e1cb632281113aec4240e82b6ed2e8be96c186485e2b2ae9952023ab30e051a379ab6f286e84e006e94c0e9858c4d1d944cf126ddae564fe3600647a5741652031c31267ccdc30cd92739e79c64e060572f75a83adb22c73379e1cf241e463b1031d8f5ad7d2eecaf9133b063b415c745fa526246f5dcb244a91aceca73f39dd804fa74ac6bebb1047b80064ee7aff009156a6bf12c8c4e7f03552fed77461c01c8cd171d8c80ad7519919b680df305f5aafe6c657698c6474c8ab8237504632a7a8355e5b603b7145c762385e27b940e3e4072462b4add12494ac4e4a938667238ed8e79ac465f2e756f435ad15dc716a5390c76903181f9d2158af2ca20b9962fb3a18d8ed52c0ae3dcf1ce3bd54d51fecc7cc942ee9081b473c11efd3a0fc8d6e7daa2112e55768e7e61d07538ac2bbd461bebaba9a44511e4aa13ce48e87d7be79e993d69a11cadc248efbb191e9e9566090a40a18673571d9628ce573c60d558d54a020fe14ee161db896040e2a6de58fe14c5ca9c638a5040248eb4865e821f300652083534b1a40b92d5cc4775347f71c81521ba99bab939ec69d8574684ae27972bce29f1c6ee39e31542190c4338ab1f6e00722801583cba8469182fb7ae2ae4c7ca063fe26238159ab766062f0e779a4b41717ba8a2e199dcf4a00d9c19aedade12af3cb1e38607600727f1ff0ae9228dada308c7276e09ae7f43b756f102aaee7ca957c8031cf22bbd4d362f29ae2e65091a9efc66a5948c54469651818feb5a170fb504679205452788349b7ca8b691b1c6edb5564d6ec2e0ee85b9f4f5a069961a21b78aa374a14559fb4a6320e33546e2ee3ce0f4f5a919973b027d2a187893ad4d218de46f98003a532da3df3808413914c45fb9590424283bbb01f4ae6d3114811f2de536f75c67383938aec5e378dbe6073d79ae3276693509620c41672a31c601a7125933cf19665c1c7638ea29362bae40c536689060822a1fb5220da0e4d5016ca8d807a5422221b75417266e307031d05555ba990e1b247a1a2c221dbb403d69e0ee238a8f07de9e8f83834c44cc320638a6ba1c0e6a511851b9ce7353aa2ba8e9486558f70e147e38ad1d0ed64bdd40a894c44100baf50a0163ffa0e3f1aab2ee44c6dfcab4f46664d36f92da1924bdb951144abe8082df8e08fd6803a9f0ec36b3ea12dd41c8894a727b923ff00af57b5cd52ded884962321539d80f381ed55bc18b0269f75b3225122890118c103a7f3ad0bfb3b6ba6677863791780587f8549462dd6b50cc0158adf61c7f183ce3a74ac2379693dc295b758b278756f94fe35d06adaa5dc974855ee6d4aa8571092524c00012a08e700027be3a5327f23556b2892c5d7c95d85e46cbc9d793c0ee68020972b08c03c8acab9906d20f535dc4fa3c450244eb80a39635c5ea96222701c929bbe60bdc52b14436ba759ca034f74771e76ee02a592ce089d4c0e460f635af2436106891a5bed6ba2ff00bd89e42995f552ac39c8ee7bd65dc69d6c6da030cac6f198b4c3796445eca09e49fd2817c8eaa326e34fb77930641c6ef515c26a1682cef24b891814329057a139aebb4d9a5f261b7939083008eb5cb6b6ed717f7113a2ed8dd4a11dc123269a6268ca594095d49dd83819ef504af1ab92139fad2ca42e4803354c9c924d5a443659378f8c75a412095be65155a9f19f9c53b0265a309008eb5225af426b4d1107245040ed523336443f80aaed2303b46456ab20208aaad080dc75a008e1134ac3731d9deb5f4ed41b4e49a1858a89b00b81f301e80f607fa5510a54803a54bb0e41f4a00e9b439235b7b95b7428721989eac7d6b72d5259222c5be7279cd733a0caa93ca8e7efa71f8107fc6baeb565440e79f6a9ea5a213a2b3be6599b9e838ab314563a6b10eebbc0e4fa7e34d96e72d8e9c7735952dc3db3379768b75211c075c804f7fc3fad031faed9ea2192e2142d049ca94cd601919c98ee8608ee6b69b5ad4c5acb198e40e382b1b3155fa7a8ae79ee3cd0cb34243372afb8139fa75a4069ac056208c15d474c8a531450ae762ae7b0ab50e3eccbbb8e3a566deb0dd8cf4a43d8d2d3664926eb80a0fe15c8ca8f722e6e9a75f315b2549fe007b7bf4ad7b5245bdc1070593603f5aca974d611141282beb549112319dd1a33c106aa9ab93c6b1e4060c6aa118ad110c6d393ef8a6d2afde14c9474f2c7b1460e78a88293df8a7cb2ef02a3da0213baa0b21b8956327906ab24cf21c85a74b86272335246e100e2801e8eca7e6534f330ce052c77710900750455a06090f0839a008adae4c73a38ec6baab1bd390ae723a8ae6cda4647c9906add9bc99f289edc1f4a4ca4ce9a4ba88b7cd8e7a0aa2f75144de6492aa039da33927f0acb9f7bfcb9c1e847a1ad1d16c348b9774be1891b037ccfc2fd326a46321d4cdb4ef224e1d5f831bc7cfe1827f5359ad290eceac8c3393b7a8fc2ba93e13d25dd9239f036160e24e9f9562ea3e1fb2b087cc4bb71229e0ee073415610ddaf94086accb8b80ce4f526a97da1b0c4f4cf7ef4eb646b866724ed1413716f7504b48e38c1cb1f98d653ea8c55860e4fbd69ea16093c591c32f39ac74b2dddfa55ab12ee55dc7249ef4a02f5393569ad79033425a9ee783ed4ee4d8a59f6a178606accb02a0c0eb55b1839a626ac6e3292387a6952380d523c601a859307209a92ac26c6ef4a8a09e4d26e63c678a4c1068014c5963c71562d86d6e878a22e4723153a85c8c0a069131b82abc2f34c8af5925c98f20d4c155c62ad58580babc8a01fc4d827d07ad201fbb612b2e7d03521b449ce1df0bec7ad5ad41104adb7a02474f4ac3964946e11b11ec29145c96c2ca253b2570decc6b32421188c923de9856e5c1fde1c55492299b866245160b84d72656d8878e84d74e90ac5a359111921d4fcebd8e4f5ae5bc9280715bd63aab2e9e2d641c293838f5a6485cc1756ebe64899888e1c720fe3dab116e14dc151c0cd75fa6ddce0ed8626915f82bb7703f856bbf83f4ebf87cfb8b6934e7232ce830bff007c9a62b9c24a89b411cd459c9c015d3dc784ade352d6dadda3007fe5bfeeff002e4d578bc39a8c8e5628a2b903f8a195483f8120fe9401ce928bc6cc9a845b09339e2ba19f46bbb705a5b29940ee50e3f3aa0f09cfddc503b0aca181c9a87ca6cf5a9caf41de90a10681b2b188ad33054f3567193820d32481b3edda82423ce7ad5a4c3719fd6ad69fe1ebcbac34c16d21ff009e93b6dcfd0753fcbdeba3b2f0ee968a48d52ce6941c00ecdb7f203fc450330ac34f9af988857e51cb487855fa9ad432c5a4a18e03bd8024bf42c71fc856dcba25e4b1612fad1d0740b20503f038a81bc32ab6a43de43e6ff1306dd48673eac2e200c49e6b3b62fda9949c7d6b6aef4d5b1511c771bf2339c57393a4c6e99b903f9d160b9ab22471c58dc3a5516111ea47150c892b27cd211558c0ec793c5160b924ac99c2e0fbd112c7d649028f61934cf282f4e4d46c9939a2c237a1f10359db8834f8fcbe3e6958e18fe558d70d77753191e792563c9dcc4ff5a8c4641dc0e315a962e8461bad009199e4bee05f35d3699afdae9d0846859b1d7181519822938c535ec2365c1c0145c2c6b9f1cdb31c796e9e9cf159d78d69abb99ed99629f1f7380adf8fad615e592237cb54e3924b73c3118e940b634980671484609ef526422827ad205ddcd055875b5b9b899234525dce00adfb9822d236a4457cd039908049fa7a0fa56759ea3068d6ed70144978e08427a20ffebd625deab7179319247249a6496750be9256c190b13d4939cd2d8e9b2decabb6465efc566428f7370a839c9af48d12c23b5b60cea338ef40189fd9b7b180a2e2423d0e69ec6e2d971e631faf4ae925b88d8e3200fa5665d35bb839238f5a0660cd732bb65d989aaecc42e48e6addd5c5bc6709cd5466f3141a0081f9e2a32840abc8881738e6a29c67181480cf65e7ad30820f5cd592871c8aaee393400c66e288e7dae0d46dd69854e6811b505ef3ff00d7a9a4bb6dbd7f5ae7d5d91873c55c67263041a2c34c74b3333f26a19141434aa0b73d696418534ba81fffd9, '32111254', '2018-08-15 16:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `PATIENTEXAMINATION`
--

CREATE TABLE `PATIENTEXAMINATION` (
  `PEX_ID` int(11) NOT NULL,
  `PEX_DATE` datetime NOT NULL,
  `PEX_PAT_ID` int(11) NOT NULL,
  `PEX_HEIGHT` double DEFAULT '0',
  `PEX_WEIGHT` int(11) DEFAULT '0',
  `PEX_PA_MIN` int(11) DEFAULT '0',
  `PEX_PA_MAX` int(11) DEFAULT '0',
  `PEX_FC` int(11) DEFAULT '0',
  `PEX_TEMP` double DEFAULT '0',
  `PEX_SAT` double DEFAULT '0',
  `PEX_NOTE` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PATIENTVACCINE`
--

CREATE TABLE `PATIENTVACCINE` (
  `PAV_ID` int(11) NOT NULL,
  `PAV_YPROG` int(11) NOT NULL,
  `PAV_DATE` datetime NOT NULL,
  `PAV_PAT_ID` int(11) NOT NULL,
  `PAV_VAC_ID_A` varchar(10) NOT NULL,
  `PAV_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PREGNANTTREATMENTTYPE`
--

CREATE TABLE `PREGNANTTREATMENTTYPE` (
  `PTT_ID_A` varchar(10) NOT NULL,
  `PTT_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PREGNANTTREATMENTTYPE`
--

INSERT INTO `PREGNANTTREATMENTTYPE` (`PTT_ID_A`, `PTT_DESC`) VALUES
('A', 'ANC RE-ATTENDANCE\r'),
('I1', 'IMMUNISATION 1\r'),
('I2', 'IMMUNISATION 2\r'),
('I3', 'IMMUNISATION 3\r'),
('N', 'NEW ANC ATTENDANCE\r'),
('S1', 'FIRST DOSE WITH SP\r'),
('S2', 'SECOND DOSE WITH SP\r');

-- --------------------------------------------------------

--
-- Table structure for table `PRICELISTS`
--

CREATE TABLE `PRICELISTS` (
  `LST_ID` int(11) NOT NULL,
  `LST_CODE` varchar(7) NOT NULL,
  `LST_NAME` varchar(50) NOT NULL,
  `LST_DESC` varchar(100) NOT NULL,
  `LST_CURRENCY` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PRICELISTS`
--

INSERT INTO `PRICELISTS` (`LST_ID`, `LST_CODE`, `LST_NAME`, `LST_DESC`, `LST_CURRENCY`) VALUES
(1, 'LIST001', 'Basic', 'Basic price list', '');

-- --------------------------------------------------------

--
-- Table structure for table `PRICES`
--

CREATE TABLE `PRICES` (
  `PRC_ID` int(11) NOT NULL,
  `PRC_LST_ID` int(11) NOT NULL,
  `PRC_GRP` char(3) NOT NULL,
  `PRC_ITEM` varchar(10) NOT NULL,
  `PRC_DESC` varchar(100) NOT NULL,
  `PRC_PRICE` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PRICES`
--

INSERT INTO `PRICES` (`PRC_ID`, `PRC_LST_ID`, `PRC_GRP`, `PRC_ITEM`, `PRC_DESC`, `PRC_PRICE`) VALUES
(1, 1, 'EXA', '01.01', '1.1 HB', 0),
(2, 1, 'EXA', '01.02', '1.2 WBC Count', 0),
(3, 1, 'EXA', '01.03', '1.3 Differential ', 0),
(4, 1, 'EXA', '01.04', '1.4 Film Comment', 0),
(5, 1, 'EXA', '01.05', '1.5 ESR', 0),
(6, 1, 'EXA', '01.06', '1.6 Sickling Test', 0),
(7, 1, 'EXA', '02.01', '2.1 Grouping', 0),
(8, 1, 'EXA', '02.02', '2.2 Comb''s Test', 0),
(9, 1, 'EXA', '03.01', '3.1 Blood Slide (Malaria)', 0),
(10, 1, 'EXA', '03.02', '3.2 Blood Slide (OTHERS, E.G. TRIUPHANOSOMIAS, MICRIFILARIA, LEISHMANIA, BORRELIA)', 0),
(11, 1, 'EXA', '03.02.1', '3.21 Trypanosomiasis', 0),
(12, 1, 'EXA', '03.02.2', '3.22 MICROFILARIA', 0),
(13, 1, 'EXA', '03.02.3', '3.23 LEISHMANIA', 0),
(14, 1, 'EXA', '03.02.4', '3.24 BORRELIA', 0),
(15, 1, 'EXA', '03.03', '3.3 STOOL MICROSCOPY', 0),
(16, 1, 'EXA', '03.04', '3.4 URINE MICROSCOPY', 0),
(17, 1, 'EXA', '03.05', '3.5 TISSUE MICROSCOPY', 0),
(18, 1, 'EXA', '03.06', '3.6 CSF WET PREP', 0),
(19, 1, 'EXA', '04.01', '4.1 CULTURE AND SENSITIVITY (C&S) FOR HAEMOPHILUS INFUENZA TYPE B', 0),
(20, 1, 'EXA', '04.02', '4.2 C&S FOR SALMONELA TYPHI', 0),
(21, 1, 'EXA', '04.03', '4.3 C&S FOR VIBRO CHOLERA', 0),
(22, 1, 'EXA', '04.04', '4.4 C&S FOR SHIGELLA DYSENTERIAE', 0),
(23, 1, 'EXA', '04.05', '4.5 C&S FOR NEISSERIA MENINGITIDES', 0),
(24, 1, 'EXA', '04.06', '4.6 OTHER C&S ', 0),
(25, 1, 'EXA', '05.01', '5.1 WET PREP', 0),
(26, 1, 'EXA', '05.02', '5.2 GRAM STAIN', 0),
(27, 1, 'EXA', '05.03', '5.3 INDIA INK', 0),
(28, 1, 'EXA', '05.04', '5.4 LEISMANIA', 0),
(29, 1, 'EXA', '05.05', '5.5 ZN', 0),
(30, 1, 'EXA', '05.06', '5.6 WAYSON', 0),
(31, 1, 'EXA', '05.07', '5.7 PAP SMEAR', 0),
(32, 1, 'EXA', '06.01', '6.1 VDRL/RPR', 0),
(33, 1, 'EXA', '06.02', '6.2 TPHA', 0),
(34, 1, 'EXA', '06.03', '6.3 HIV', 0),
(35, 1, 'EXA', '06.04', '6.4 HEPATITIS', 0),
(36, 1, 'EXA', '06.05', '6.5 OTHERS E.G BRUCELLA, RHEUMATOID FACTOR, WEIL FELIX', 0),
(37, 1, 'EXA', '06.06', '6.6 PREGANCY TEST', 0),
(38, 1, 'EXA', '07.01', '7.1 PROTEIN', 0),
(39, 1, 'EXA', '07.02', '7.2 SUGAR', 0),
(40, 1, 'EXA', '07.03', '7.3 LFTS', 0),
(41, 1, 'EXA', '07.03.1', '7.3.1 BILIRUBIN TOTAL', 0),
(42, 1, 'EXA', '07.03.2', '7.3.2 BILIRUBIN DIRECT', 0),
(43, 1, 'EXA', '07.03.3', '7.3.3 GOT', 0),
(44, 1, 'EXA', '07.03.4', '7.3.4 ALT/GPT', 0),
(45, 1, 'EXA', '07.04', '7.4 RFTS', 0),
(46, 1, 'EXA', '07.04.1', '7.4.1 CREATININA', 0),
(47, 1, 'EXA', '07.04.2', '7.4.2 UREA', 0),
(48, 1, 'EXA', '08.01', '8.1 OCCULT BLOOD', 0),
(49, 1, 'EXA', '09.01', '9.1 URINALYSIS', 0),
(50, 1, 'OPE', '40', 'Abdominal Tumours', 0),
(51, 1, 'OPE', '10', 'Acute abdomen', 0),
(52, 1, 'OPE', '38', 'Appendicitis', 0),
(53, 1, 'OPE', '27', 'Bladder biopsy', 0),
(54, 1, 'OPE', '1', 'Caesarian section', 0),
(55, 1, 'OPE', '39', 'Cholecystitis', 0),
(56, 1, 'OPE', '18', 'Circumcision', 0),
(57, 1, 'OPE', '17', 'Cystocele', 0),
(58, 1, 'OPE', '46', 'Debridement', 0),
(59, 1, 'OPE', '8', 'Dilatation and curettage', 0),
(60, 1, 'OPE', '21', 'Dorsal slit-paraphimosis', 0),
(61, 1, 'OPE', '11', 'Ectopic pregnancy', 0),
(62, 1, 'OPE', '31', 'Epigastrical Hernia', 0),
(63, 1, 'OPE', '5', 'Evacuations', 0),
(64, 1, 'OPE', '28', 'Hernia (inguinal & femoral)', 0),
(65, 1, 'OPE', '23', 'Hydrocelectomy', 0),
(66, 1, 'OPE', '2', 'Hysterectomy', 0),
(67, 1, 'OPE', '45', 'Incision & Drainage', 0),
(68, 1, 'OPE', '6', 'Incomplete abortion', 0),
(69, 1, 'OPE', '4', 'Injured uterus', 0),
(70, 1, 'OPE', '32', 'Intestinal obstruction', 0),
(71, 1, 'OPE', '35', 'Laparotomy', 0),
(72, 1, 'OPE', '43', 'Lower limb', 0),
(73, 1, 'OPE', '33', 'Mechanical', 0),
(74, 1, 'OPE', '47', 'Mise -a- plat', 0),
(75, 1, 'OPE', '30', 'Non strangulated', 0),
(76, 1, 'OPE', '44', 'Osteomyelitis - sequestrectomy', 0),
(77, 1, 'OPE', '15', 'Ovarian tumours', 0),
(78, 1, 'OPE', '20', 'Paraphimosis', 0),
(79, 1, 'OPE', '13', 'Pelvic abscess', 0),
(80, 1, 'OPE', '36', 'Penetrating abdominal injuries', 0),
(81, 1, 'OPE', '37', 'Peritonitis', 0),
(82, 1, 'OPE', '12', 'Peritonitis', 0),
(83, 1, 'OPE', '19', 'Phimosis', 0),
(84, 1, 'OPE', '26', 'Prostate biopsy', 0),
(85, 1, 'OPE', '25', 'Prostatectomy', 0),
(86, 1, 'OPE', '41', 'Reduction of fractures', 0),
(87, 1, 'OPE', '9', 'Repair of vesico-vaginal fistula (vvf)', 0),
(88, 1, 'OPE', '3', 'Ruptured uterus', 0),
(89, 1, 'OPE', '7', 'Septic abortion', 0),
(90, 1, 'OPE', '29', 'Strangulated', 0),
(91, 1, 'OPE', '48', 'Surgical toilet  & suture', 0),
(92, 1, 'OPE', '24', 'Testicular tumours', 0),
(93, 1, 'OPE', '42', 'Upper limb', 0),
(94, 1, 'OPE', '22', 'Uretheral stricture-bougienage', 0),
(95, 1, 'OPE', '14', 'Uterine fibroids', 0),
(96, 1, 'OPE', '16', 'Uterine prolapse', 0),
(97, 1, 'OPE', '34', 'Volvulus', 0),
(98, 1, 'MED', '24', '4 Dimethyl Aminobenzaldelyde\r', 0),
(99, 1, 'MED', '98', 'Acetazolamide 250mg Tab\r', 0),
(100, 1, 'MED', '2', 'Acetic Acid Glacial 1 ltr\r', 0),
(101, 1, 'MED', '3', 'Aceton 99% 1ltr\r', 0),
(102, 1, 'MED', '100', 'Aciclovir\r', 0),
(103, 1, 'MED', '99', 'Acyclovir 200mg Tab\r', 0),
(104, 1, 'MED', '258', 'Adhesive Tape 2.5cm x 5m\r', 0),
(105, 1, 'MED', '259', 'Adhesive Tape 7.5cm x 5m\r', 0),
(106, 1, 'MED', '40', 'Adrenaline 1mg/ml 1ml Amp\r', 0),
(107, 1, 'MED', '311', 'Air ring set 43x15cm, rubber with pump\r', 0),
(108, 1, 'MED', '253', 'Airway Guedel Size 0\r', 0),
(109, 1, 'MED', '252', 'Airway Guedel Size 00\r', 0),
(110, 1, 'MED', '254', 'Airway Guedel Size 1\r', 0),
(111, 1, 'MED', '255', 'Airway Guedel Size 2\r', 0),
(112, 1, 'MED', '256', 'Airway Guedel Size 3\r', 0),
(113, 1, 'MED', '103', 'Albendazole 200mg Tab\r', 0),
(114, 1, 'MED', '102', 'Albendazole 400mg Tab\r', 0),
(115, 1, 'MED', '178', 'Alcohol 95% not denatured 20Ltrs\r', 0),
(116, 1, 'MED', '101', 'Aminophylline 100mg Tab\r', 0),
(117, 1, 'MED', '41', 'Aminophylline 25mg/ml,10ml Amp\r', 0),
(118, 1, 'MED', '104', 'Amitriptyline 25mg Tab\r', 0),
(119, 1, 'MED', '23', 'Ammonium Oxalate\r', 0),
(120, 1, 'MED', '106', 'Amoxicillin /Clavulanate 375mg Tab\r', 0),
(121, 1, 'MED', '207', 'Amoxicillin 125mg/5ml Powd. Susp 100ml\r', 0),
(122, 1, 'MED', '105', 'Amoxicillin 250mg Caps\r', 0),
(123, 1, 'MED', '42', 'Amphotericin B 50mg Vial\r', 0),
(124, 1, 'MED', '43', 'Ampicillin 500mg Vial\r', 0),
(125, 1, 'MED', '223', 'Anti Serum A 10ml\r', 0),
(126, 1, 'MED', '393', 'Anti Serum AB 10ml\r', 0),
(127, 1, 'MED', '392', 'Anti Serum B 10ml\r', 0),
(128, 1, 'MED', '394', 'Anti Serum D 10ml\r', 0),
(129, 1, 'MED', '204', 'Antihaemorrhoid suppositories\r', 0),
(130, 1, 'MED', '333', 'Apron Plastic Re-usable\r', 0),
(131, 1, 'MED', '334', 'Apron Plastic Re-usable local\r', 0),
(132, 1, 'MED', '335', 'Apron Polythene Disp Non Sterile\r', 0),
(133, 1, 'MED', '107', 'Ascorbic Acid 100mg tab\r', 0),
(134, 1, 'MED', '108', 'Aspirin 300mg Tab\r', 0),
(135, 1, 'MED', '110', 'Atenolol 100mg Tab\r', 0),
(136, 1, 'MED', '109', 'Atenolol 50mg Tab\r', 0),
(137, 1, 'MED', '44', 'Atropine 1mg/ml 1ml Amp\r', 0),
(138, 1, 'MED', '390', 'Barium Sulphate for X-Ray 1kg\r', 0),
(139, 1, 'MED', '197', 'Beclomethasone 50mcg Inhaler\r', 0),
(140, 1, 'MED', '305', 'Bedpan Plastic Autoclavable\r', 0),
(141, 1, 'MED', '306', 'Bedpan Stainless Steel\r', 0),
(142, 1, 'MED', '111', 'Bendrofluazide 5mg Tab\r', 0),
(143, 1, 'MED', '45', 'Benzathine Penicillin 2.4 MIU Vial\r', 0),
(144, 1, 'MED', '112', 'Benzhexol 2mg Tab\r', 0),
(145, 1, 'MED', '113', 'Benzhexol 5mg Tab\r', 0),
(146, 1, 'MED', '46', 'Benzyl Penicillin 1 MIU Vial\r', 0),
(147, 1, 'MED', '47', 'Benzyl Penicillin 5 MIU Vial\r', 0),
(148, 1, 'MED', '190', 'Betamethasone 0.1% eye/ear/nose drops\r', 0),
(149, 1, 'MED', '191', 'Betamethasone 0.1% Neomycin 0.35 %eye drops 7.5ml\r', 0),
(150, 1, 'MED', '114', 'Bisacodyl 5mg Tab\r', 0),
(151, 1, 'MED', '224', 'Blood giving set Disposable\r', 0),
(152, 1, 'MED', '225', 'Blood Transfer Bag 300ml\r', 0),
(153, 1, 'MED', '307', 'Body Bag 70 x 215cm  (Adult)\r', 0),
(154, 1, 'MED', '310', 'Bowl Without Lid 10 x 24cm stainless steel\r', 0),
(155, 1, 'MED', '308', 'Bowl Without Lid 7 x 12cm stainless steel\r', 0),
(156, 1, 'MED', '309', 'Bowl Without Lid 8 x 16cm stainless steel\r', 0),
(157, 1, 'MED', '22', 'Brilliant Cresyl Blue\r', 0),
(158, 1, 'MED', '115', 'Calcium Lactate 300mg Tab\r', 0),
(159, 1, 'MED', '116', 'Carbamazepine 200mg Tab\r', 0),
(160, 1, 'MED', '117', 'Carbimazole 5mg Tab\r', 0),
(161, 1, 'MED', '273', 'Catheter Foley CH10 3 Way Balloon\r', 0),
(162, 1, 'MED', '274', 'Catheter Foley CH12 3 Way Balloon\r', 0),
(163, 1, 'MED', '275', 'Catheter Foley CH14 3 Way Balloon\r', 0),
(164, 1, 'MED', '276', 'Catheter Foley CH16 3 Way Balloon\r', 0),
(165, 1, 'MED', '277', 'Catheter Foley CH18 3 Way Balloon\r', 0),
(166, 1, 'MED', '271', 'Catheter Foley CH20 3 Way Balloon\r', 0),
(167, 1, 'MED', '272', 'Catheter Foley CH8 3 Way Balloon\r', 0),
(168, 1, 'MED', '278', 'Catheter Stopper for All sizes\r', 0),
(169, 1, 'MED', '118', 'Charcoal 250mg Tab\r', 0),
(170, 1, 'MED', '192', 'Chloramphenicol 0.5% Eye Drops 10ml\r', 0),
(171, 1, 'MED', '209', 'Chloramphenicol 125mg/5ml Susp 100ml\r', 0),
(172, 1, 'MED', '208', 'Chloramphenicol 125mg/5ml Susp 3Ltr\r', 0),
(173, 1, 'MED', '48', 'Chloramphenicol 1g Vial\r', 0),
(174, 1, 'MED', '119', 'Chloramphenicol 250mg Caps\r', 0),
(175, 1, 'MED', '179', 'Chlorhexidine/Cetrimide 1.5/15% 1Ltr\r', 0),
(176, 1, 'MED', '180', 'Chlorhexidine/Cetrimide 1.5/15% 5Ltr\r', 0),
(177, 1, 'MED', '49', 'Chloroquine 40mg Base/ml 5ml Amp\r', 0),
(178, 1, 'MED', '121', 'Chloroquine Coated 150mg Tab\r', 0),
(179, 1, 'MED', '120', 'Chloroquine Uncoated 150mg Tab\r', 0),
(180, 1, 'MED', '123', 'Chlorphenimine 4mg Tab\r', 0),
(181, 1, 'MED', '124', 'Chlorpromazine 100mg Tab\r', 0),
(182, 1, 'MED', '125', 'Chlorpromazine 25mg Tab\r', 0),
(183, 1, 'MED', '50', 'Chlorpromazine 25mg/ml/2ml Amp\r', 0),
(184, 1, 'MED', '126', 'Cimetidine 200mg Tab\r', 0),
(185, 1, 'MED', '127', 'Cimetidine 400mg Tab\r', 0),
(186, 1, 'MED', '129', 'Ciprofloxacine 250mg Tab\r', 0),
(187, 1, 'MED', '128', 'Ciprofloxacine 500mg Tab\r', 0),
(188, 1, 'MED', '193', 'Cloramphenicol 1% Eye Ointment 3.5g\r', 0),
(189, 1, 'MED', '202', 'Clotrimazole 100mg Pessaries\r', 0),
(190, 1, 'MED', '201', 'Clotrimazole 500mg Pessaries\r', 0),
(191, 1, 'MED', '130', 'Cloxacillin 250mg Tab\r', 0),
(192, 1, 'MED', '51', 'Cloxacillin 500mg Vial\r', 0),
(193, 1, 'MED', '131', 'Codein Phosphate 30mg Tab\r', 0),
(194, 1, 'MED', '312', 'Colostomy Bag closed 30mm size 2\r', 0),
(195, 1, 'MED', '313', 'Colostomy Bag closed 30mm size 3\r', 0),
(196, 1, 'MED', '314', 'Colostomy Bag open re-usable 35mm\r', 0),
(197, 1, 'MED', '4', 'Copper 11 Sulphate 500g\r', 0),
(198, 1, 'MED', '132', 'Cotrimoxazole 100/20mg Paed Tab\r', 0),
(199, 1, 'MED', '211', 'Cotrimoxazole 200+40mg/5ml Susp 100ml\r', 0),
(200, 1, 'MED', '210', 'Cotrimoxazole 200+40mg/5ml Susp 3Ltr\r', 0),
(201, 1, 'MED', '133', 'Cotrimoxazole 400/80mg Tab\r', 0),
(202, 1, 'MED', '261', 'Cotton Wool 200G\r', 0),
(203, 1, 'MED', '260', 'cotton Wool 500G\r', 0),
(204, 1, 'MED', '395', 'Creatinine 200ml (Calorimetric)\r', 0),
(205, 1, 'MED', '29', 'Crystal Violet\r', 0),
(206, 1, 'MED', '52', 'Cyclophosphamide 200mg Vial\r', 0),
(207, 1, 'MED', '53', 'Cyclophosphamide 500mg Vial\r', 0),
(208, 1, 'MED', '134', 'Darrows Half Strength 500ml\r', 0),
(209, 1, 'MED', '140', 'Dexamethasone 0.5mg Tab\r', 0),
(210, 1, 'MED', '136', 'Dexamethasone 4mg/ml 1ml Amp\r', 0),
(211, 1, 'MED', '135', 'Dexamethasone 4mg/ml 2ml Amp\r', 0),
(212, 1, 'MED', '138', 'Dextrose 30% IV 100ml\r', 0),
(213, 1, 'MED', '137', 'Dextrose 5% IV 500ml\r', 0),
(214, 1, 'MED', '139', 'Dextrose 50% IV 100ml\r', 0),
(215, 1, 'MED', '206', 'Dextrose Monohydrate Apyrogen 25Kg\r', 0),
(216, 1, 'MED', '391', 'Diatrizoate Meglumin Sod 76% 20ml Amp\r', 0),
(217, 1, 'MED', '203', 'Diazepam 2mg/ml 2.5ml Rectal Tube\r', 0),
(218, 1, 'MED', '54', 'Diazepam 5mg / ml 2ml Amp\r', 0),
(219, 1, 'MED', '141', 'Diazepam 5mg Tab\r', 0),
(220, 1, 'MED', '55', 'Diclofenac 25mg/ml 3ml Amp\r', 0),
(221, 1, 'MED', '142', 'Diclofenac 50mg Tab\r', 0),
(222, 1, 'MED', '143', 'Diethylcarbamazine 50mg Tab\r', 0),
(223, 1, 'MED', '144', 'Digoxin 0.25 mg Tab\r', 0),
(224, 1, 'MED', '56', 'Digoxin 0.25 mg/ml 2ml Amp\r', 0),
(225, 1, 'MED', '145', 'Doxycycline 100mg Tab\r', 0),
(226, 1, 'MED', '315', 'Ear syringe rubber 60ml\r', 0),
(227, 1, 'MED', '5', 'EDTA Di- sodium salt 100g\r', 0),
(228, 1, 'MED', '262', 'Elastic Bandage 10cm x 4.5m\r', 0),
(229, 1, 'MED', '263', 'Elastic Bandage 7.5cm x 4.5m\r', 0),
(230, 1, 'MED', '221', 'Eosin Yellowish\r', 0),
(231, 1, 'MED', '146', 'Ephedrine 30mg Tab\r', 0),
(232, 1, 'MED', '147', 'Erythromycin 250mg Tab\r', 0),
(233, 1, 'MED', '6', 'Ethanol Absolute 1ltr\r', 0),
(234, 1, 'MED', '257', 'Eye Pad Sterile\r', 0),
(235, 1, 'MED', '149', 'Fansidar 500/25mg Tab\r', 0),
(236, 1, 'MED', '148', 'Fansidar 500/25mg Tab (50dosesx3)\r', 0),
(237, 1, 'MED', '150', 'Ferrous Sulphate 200mg Tab\r', 0),
(238, 1, 'MED', '218', 'Field stain A and B\r', 0),
(239, 1, 'MED', '316', 'First Aid kit\r', 0),
(240, 1, 'MED', '152', 'Fluconazole 100mg 24 Caps\r', 0),
(241, 1, 'MED', '151', 'Fluconazole 100mg Tab\r', 0),
(242, 1, 'MED', '156', 'Folic Acid 15mg Tab\r', 0),
(243, 1, 'MED', '153', 'Folic Acid 1mg Tab\r', 0),
(244, 1, 'MED', '154', 'Folic Acid 5mg Tab\r', 0),
(245, 1, 'MED', '155', 'Folic Acid/Ferrous Sulp 0.5/200mg Tab\r', 0),
(246, 1, 'MED', '7', 'Formaldehyde solution 35-38% 1ltr\r', 0),
(247, 1, 'MED', '157', 'Frusemide 40mg Tab\r', 0),
(248, 1, 'MED', '57', 'Furosemide 10mg/ml 2ml Amp\r', 0),
(249, 1, 'MED', '318', 'Gallipot stainless steel 200ml/10cm\r', 0),
(250, 1, 'MED', '317', 'Gallipot stainless steel 300ml/15cm\r', 0),
(251, 1, 'MED', '265', 'Gauze Bandage 10cm x 4m\r', 0),
(252, 1, 'MED', '264', 'Gauze Bandage 7.5cm x 3.65-4m\r', 0),
(253, 1, 'MED', '268', 'Gauze Hydrophylic 90cm x 91cm\r', 0),
(254, 1, 'MED', '267', 'Gauze Pads  Sterile 10cm x 10cm\r', 0),
(255, 1, 'MED', '266', 'Gauze Pads Non Sterile 10cm x 10cm\r', 0),
(256, 1, 'MED', '219', 'Genitian Violet\r', 0),
(257, 1, 'MED', '194', 'Gentamicin 0.3% eye/ear drops 10ml\r', 0),
(258, 1, 'MED', '58', 'Gentamicin 40mg/ml 2ml\r', 0),
(259, 1, 'MED', '181', 'Gentian Violet 25g Tin\r', 0),
(260, 1, 'MED', '222', 'Giemsa Stain\r', 0),
(261, 1, 'MED', '158', 'Glibenclamide 5mg Tab\r', 0),
(262, 1, 'MED', '293', 'Gloves Domestic\r', 0),
(263, 1, 'MED', '295', 'Gloves Gynaecological 7.5-8\r', 0),
(264, 1, 'MED', '294', 'Gloves High risk non sterile Medium\r', 0),
(265, 1, 'MED', '297', 'Gloves Non Sterile Large Disposable\r', 0),
(266, 1, 'MED', '296', 'Gloves Non Sterile Medium Disposable\r', 0),
(267, 1, 'MED', '298', 'Gloves Surgical Sterile 6\r', 0),
(268, 1, 'MED', '299', 'Gloves Surgical Sterile 6.5\r', 0),
(269, 1, 'MED', '300', 'Gloves Surgical Sterile 7\r', 0),
(270, 1, 'MED', '301', 'Gloves Surgical Sterile 7.5\r', 0),
(271, 1, 'MED', '302', 'Gloves Surgical Sterile 8\r', 0),
(272, 1, 'MED', '303', 'Gloves Surgical Sterile 8.5\r', 0),
(273, 1, 'MED', '396', 'Glucose GOD PAD  6 x 100ml (Colorimetric)\r', 0),
(274, 1, 'MED', '1', 'Glucose Test Strip\r', 0),
(275, 1, 'MED', '397', 'Glucose Test Strips (Hyloguard)\r', 0),
(276, 1, 'MED', '32', 'GOT ( AST) 200ml (Calorimetric) AS 147\r', 0),
(277, 1, 'MED', '31', 'GOT ( AST) 200ml has no NAOH) AS 101\r', 0),
(278, 1, 'MED', '30', 'GPT (ALT) 200ml ( Does not have NAOH)\r', 0),
(279, 1, 'MED', '159', 'Griseofulvin 500mg Tab\r', 0),
(280, 1, 'MED', '161', 'Haloperidol 5mg\r', 0),
(281, 1, 'MED', '160', 'Haloperidol 5mg Tab\r', 0),
(282, 1, 'MED', '59', 'Haloperidol 5mg/ml 1ml Amp\r', 0),
(283, 1, 'MED', '60', 'Haloperidol Decanoate 50mg/ml 1ml Amp\r', 0),
(284, 1, 'MED', '359', 'Handle for surgical blade No 3\r', 0),
(285, 1, 'MED', '360', 'Handle for surgical blade No 4\r', 0),
(286, 1, 'MED', '33', 'HIV 1/2 Capillus Kit 100Tests\r', 0),
(287, 1, 'MED', '34', 'HIV Buffer for determine Kit\r', 0),
(288, 1, 'MED', '35', 'HIV Determine 1/11 (Abbott) 100Tests\r', 0),
(289, 1, 'MED', '36', 'HIV UNIGOLD 1/11 Test Kits 20 Tests\r', 0),
(290, 1, 'MED', '319', 'Hot water Bottle 2Ltr\r', 0),
(291, 1, 'MED', '61', 'Hydralazine 20mg Vial\r', 0),
(292, 1, 'MED', '162', 'Hydralazine 25mg Tab\r', 0),
(293, 1, 'MED', '8', 'Hydrochloric Acid 30-33% 1ltr\r', 0),
(294, 1, 'MED', '195', 'Hydrocortisone 1% eye drops 5ml\r', 0),
(295, 1, 'MED', '62', 'Hydrocortisone 100mg Vial\r', 0),
(296, 1, 'MED', '182', 'Hydrogen Peroxide 6% 250ml\r', 0),
(297, 1, 'MED', '163', 'Hyoscine 10mg Tab\r', 0),
(298, 1, 'MED', '63', 'Hyoscine Butyl Bromide 20mg/ml/ Amp\r', 0),
(299, 1, 'MED', '164', 'Ibuprofen 200mg Tab\r', 0),
(300, 1, 'MED', '165', 'Imipramine 25mg Tab\r', 0),
(301, 1, 'MED', '166', 'Indomethacin 25mg Tab\r', 0),
(302, 1, 'MED', '351', 'Insectcide Spray 400g\r', 0),
(303, 1, 'MED', '320', 'Instrument Box With Lid 20x10x5cm\r', 0),
(304, 1, 'MED', '321', 'Instrument Tray 30 x 20 x 2cm\r', 0),
(305, 1, 'MED', '65', 'Insulin Isophane 100IU/ml 10ml Vial\r', 0),
(306, 1, 'MED', '68', 'Insulin Isophane 40IU/ml 10ml\r', 0),
(307, 1, 'MED', '66', 'Insulin Mixtard 30/70 100IU/ml 10ml Vial\r', 0),
(308, 1, 'MED', '67', 'Insulin Mixtard 30/70 100IU/ml 5x3ml catridges\r', 0),
(309, 1, 'MED', '64', 'Insulin Soluble 100IU/ml 10ml Vial\r', 0),
(310, 1, 'MED', '226', 'Insulin Syringe 100IU with Needle G26/29\r', 0),
(311, 1, 'MED', '9', 'Iodine Crystal 100g\r', 0),
(312, 1, 'MED', '183', 'Iodine Solution 2% 500ml\r', 0),
(313, 1, 'MED', '69', 'Iron Dextran 10mg/ml 2ml Vial\r', 0),
(314, 1, 'MED', '322', 'Irrigation can with accessories\r', 0),
(315, 1, 'MED', '167', 'Isoniazid 300mg Tab\r', 0),
(316, 1, 'MED', '227', 'IV Cannula G16 with Port\r', 0),
(317, 1, 'MED', '228', 'IV Cannula G18 with Port\r', 0),
(318, 1, 'MED', '229', 'IV Cannula G20 with Port\r', 0),
(319, 1, 'MED', '230', 'IV Cannula G22 with Port\r', 0),
(320, 1, 'MED', '232', 'IV Cannula G24 with Port\r', 0),
(321, 1, 'MED', '231', 'IV Cannula G24 without Port\r', 0),
(322, 1, 'MED', '233', 'IV Giving set Disposable\r', 0),
(323, 1, 'MED', '71', 'Ketamine 10mg/ml 10ml Vial\r', 0),
(324, 1, 'MED', '70', 'Ketamine 10mg/ml 20ml Vial\r', 0),
(325, 1, 'MED', '168', 'Ketoconazole 200mg Tab\r', 0),
(326, 1, 'MED', '325', 'Kidney Dish Polypropylene 24cm\r', 0),
(327, 1, 'MED', '324', 'Kidney Dish stainless Steel 20cm\r', 0),
(328, 1, 'MED', '323', 'Kidney Dish stainless Steel 24cm\r', 0),
(329, 1, 'MED', '376', 'Lead Apron 100cmx60cm\r', 0),
(330, 1, 'MED', '72', 'Lignocaine 2% 20ml Vial\r', 0),
(331, 1, 'MED', '73', 'Lignocaine 2% Adrenaline Dent.cartridges\r', 0),
(332, 1, 'MED', '74', 'Lignocaine spinal 50mg/ml 2ml Amp\r', 0),
(333, 1, 'MED', '187', 'Liquid detergent 20Ltr\r', 0),
(334, 1, 'MED', '185', 'liquid detergent 5Ltr Perfumed\r', 0),
(335, 1, 'MED', '326', 'Mackintosh Plastic (Apron) per 1m\r', 0),
(336, 1, 'MED', '327', 'Mackintosh Rubber Brown (sheeting) per 1m\r', 0),
(337, 1, 'MED', '328', 'Measuring Cup Graduated 25ml\r', 0),
(338, 1, 'MED', '10', 'Methanol 99% 1ltr\r', 0),
(339, 1, 'MED', '75', 'Methylergomeatrine 0.2mg/ml 1ml Amp\r', 0),
(340, 1, 'MED', '76', 'Methylergomeatrine 0.5mg/ml 1ml Amp\r', 0),
(341, 1, 'MED', '77', 'Metoclopramide 5mg/ml 100ml Amp\r', 0),
(342, 1, 'MED', '78', 'Metronidazole 5mg/ml 2ml IV\r', 0),
(343, 1, 'MED', '79', 'Morphine 15mg/ml 1ml Amp\r', 0),
(344, 1, 'MED', '353', 'Mosquito Net Impregnated Large\r', 0),
(345, 1, 'MED', '352', 'Mosquito Net Impregnated Medium\r', 0),
(346, 1, 'MED', '357', 'Mosquito Net Impregnation Liquid 500ml\r', 0),
(347, 1, 'MED', '356', 'Mosquito Net Impregnation Tablet\r', 0),
(348, 1, 'MED', '355', 'Mosquito Net Non Impregnated Large\r', 0),
(349, 1, 'MED', '354', 'Mosquito Net Non Impregnated Medium\r', 0),
(350, 1, 'MED', '358', 'Mosquito Wall spray Powder 80g\r', 0),
(351, 1, 'MED', '282', 'Nasogastric Tube G10 (Children)\r', 0),
(352, 1, 'MED', '283', 'Nasogastric Tube G14 (Children)\r', 0),
(353, 1, 'MED', '284', 'Nasogastric Tube G16 (Children)\r', 0),
(354, 1, 'MED', '279', 'Nasogastric Tube G5 (Children)\r', 0),
(355, 1, 'MED', '281', 'Nasogastric Tube G6 (Children)\r', 0),
(356, 1, 'MED', '280', 'Nasogastric Tube G8 (Children)\r', 0),
(357, 1, 'MED', '331', 'Neck Support Large\r', 0),
(358, 1, 'MED', '330', 'Neck Support Medium\r', 0),
(359, 1, 'MED', '329', 'Neck Support Small\r', 0),
(360, 1, 'MED', '234', 'Needle container disposable of contaminated\r', 0),
(361, 1, 'MED', '366', 'Needle suture No 5 cutting\r', 0),
(362, 1, 'MED', '365', 'Needle suture No 5 round\r', 0),
(363, 1, 'MED', '367', 'Needle suture No 6 Round\r', 0),
(364, 1, 'MED', '235', 'Needles Luer G20 Disposable\r', 0),
(365, 1, 'MED', '236', 'Needles Luer G21 Disposable\r', 0),
(366, 1, 'MED', '237', 'Needles Luer G22 Disposable\r', 0),
(367, 1, 'MED', '238', 'Needles Luer G23 Disposable\r', 0),
(368, 1, 'MED', '239', 'Needles Spinal G20x75-120mm\r', 0),
(369, 1, 'MED', '242', 'Needles Spinal G22x40mm\r', 0),
(370, 1, 'MED', '240', 'Needles Spinal G22x75-120mm\r', 0),
(371, 1, 'MED', '241', 'Needles Spinal G25x75-120mm\r', 0),
(372, 1, 'MED', '220', 'Neutral Red\r', 0),
(373, 1, 'MED', '26', 'Non 111 Chloride (Ferric chloride)\r', 0),
(374, 1, 'MED', '205', 'Nystatin 100.000 IU Pessaries\r', 0),
(375, 1, 'MED', '212', 'Nystatin 500.000IU/ Susp/ Drops\r', 0),
(376, 1, 'MED', '174', 'Oral Rehydration Salt (ORS)\r', 0),
(377, 1, 'MED', '80', 'Oxytocin 10 IU/ml 1ml Amp\r', 0),
(378, 1, 'MED', '176', 'Paracetamol 120mg/5ml 100ml\r', 0),
(379, 1, 'MED', '175', 'Paracetamol 120mg/5ml Syrup\r', 0),
(380, 1, 'MED', '81', 'Pethidine 100mg/ml 2ml Amp\r', 0),
(381, 1, 'MED', '82', 'Pethidine 50mg/ml 1ml Amp\r', 0),
(382, 1, 'MED', '83', 'Phenobarbital 100mg/ml 2ml Amp\r', 0),
(383, 1, 'MED', '11', 'Phenol crystals 1kg\r', 0),
(384, 1, 'MED', '84', 'Phytomenadione 10mg/ml 1ml Amp\r', 0),
(385, 1, 'MED', '85', 'Phytomenadione 1mg/ml 1ml Amp\r', 0),
(386, 1, 'MED', '269', 'Plaster of Paris 10cm\r', 0),
(387, 1, 'MED', '270', 'Plaster of Paris 15cm\r', 0),
(388, 1, 'MED', '12', 'Potassium iodide 100g\r', 0),
(389, 1, 'MED', '21', 'Potassium Oxalate\r', 0),
(390, 1, 'MED', '37', 'Pregnacy ( HGG Latex) 50 tests\r', 0),
(391, 1, 'MED', '86', 'Procaine Penicillin Fortified 4 MIU Vial\r', 0),
(392, 1, 'MED', '87', 'Promethazine 25mg/ml 2ml Amp\r', 0),
(393, 1, 'MED', '213', 'Pyridoxine 50mg Tab\r', 0),
(394, 1, 'MED', '177', 'Quinine 100mg/5ml Syrup 100ml\r', 0),
(395, 1, 'MED', '214', 'Quinine 300mg Tab\r', 0),
(396, 1, 'MED', '88', 'Quinine Di-HCI 300mg/ml 2ml Amp\r', 0),
(397, 1, 'MED', '215', 'Ranitidine 150mg Tab\r', 0),
(398, 1, 'MED', '89', 'Ranitidine 25mg/ml 2ml Amp\r', 0),
(399, 1, 'MED', '336', 'Razor Blades Disposable 5pc\r', 0),
(400, 1, 'MED', '285', 'Rectal Tube CH24\r', 0),
(401, 1, 'MED', '286', 'Rectal Tube CH26\r', 0),
(402, 1, 'MED', '216', 'Rifampicin/Isoniazid 150/100 Tab\r', 0),
(403, 1, 'MED', '39', 'RPR ( VDRL Carbon ) Antigen 5ml\r', 0),
(404, 1, 'MED', '38', 'RPR 125mm x 75mm\r', 0),
(405, 1, 'MED', '169', 'Salbutamol 4mg Tab\r', 0),
(406, 1, 'MED', '200', 'Salbutamol Inhaler 10ml\r', 0),
(407, 1, 'MED', '199', 'Salbutamol solution for inhalation 5ml\r', 0),
(408, 1, 'MED', '243', 'Scalp Vein G19 Infusion set\r', 0),
(409, 1, 'MED', '244', 'Scalp Vein G21 Infusion set\r', 0),
(410, 1, 'MED', '245', 'Scalp Vein G23 Infusion set\r', 0),
(411, 1, 'MED', '246', 'Scalp Vein G25 Infusion set\r', 0),
(412, 1, 'MED', '186', 'Soap Blue Bar 550g\r', 0),
(413, 1, 'MED', '188', 'Soap Powder Hand wash 5kg\r', 0),
(414, 1, 'MED', '27', 'Sodium Carbonate Anhydrous\r', 0),
(415, 1, 'MED', '13', 'Sodium Carbonate Anhydrous 500g\r', 0),
(416, 1, 'MED', '96', 'Sodium Chloride 0.9% IV 500ml\r', 0),
(417, 1, 'MED', '217', 'Sodium Chloride Apyrogen 50kg\r', 0),
(418, 1, 'MED', '14', 'Sodium Citrate 100g\r', 0),
(419, 1, 'MED', '20', 'Sodium Fluoride\r', 0),
(420, 1, 'MED', '184', 'Sodium Hypochlorite solution 0.75 Ltr\r', 0),
(421, 1, 'MED', '189', 'Sodium Hypochlorite solution 5Ltr\r', 0),
(422, 1, 'MED', '97', 'Sodium Lactate Compound IV 500ml\r', 0),
(423, 1, 'MED', '16', 'Sodium Nitrate 25g\r', 0),
(424, 1, 'MED', '15', 'Sodium Sulphate 500g\r', 0),
(425, 1, 'MED', '170', 'Spironolactone 25mg Tab\r', 0),
(426, 1, 'MED', '332', 'Spoon Medicine 5ml\r', 0),
(427, 1, 'MED', '337', 'Stethoscope Foetal Metal\r', 0),
(428, 1, 'MED', '338', 'Stethoscope Foetal Wood\r', 0),
(429, 1, 'MED', '90', 'Streptomycin 1g Vial\r', 0),
(430, 1, 'MED', '292', 'Suction Catheter Size 10 Disposable\r', 0),
(431, 1, 'MED', '290', 'Suction Catheter Size 12 Disposable\r', 0),
(432, 1, 'MED', '291', 'Suction Catheter Size 14 Disposable\r', 0),
(433, 1, 'MED', '289', 'Suction Catheter Size 16 Disposable\r', 0),
(434, 1, 'MED', '287', 'Suction Catheter Size 6 Disposable\r', 0),
(435, 1, 'MED', '288', 'Suction Catheter Size 8 Disposable\r', 0),
(436, 1, 'MED', '17', 'Sulphosalicylic Acid 500g\r', 0),
(437, 1, 'MED', '18', 'Sulphuric Acid Conc 1ltr\r', 0),
(438, 1, 'MED', '361', 'Surgical Blades No 20\r', 0),
(439, 1, 'MED', '362', 'Surgical Blades No 21\r', 0),
(440, 1, 'MED', '363', 'Surgical Blades No 22\r', 0),
(441, 1, 'MED', '364', 'Surgical Blades No 23\r', 0),
(442, 1, 'MED', '339', 'Surgical Brush (Scrubbing)\r', 0),
(443, 1, 'MED', '340', 'Surgical Mop 12 x 15\r', 0),
(444, 1, 'MED', '368', 'Suture Cutgut Chromic (0)\r', 0),
(445, 1, 'MED', '369', 'Suture Cutgut Chromic (2) RN22240TH\r', 0),
(446, 1, 'MED', '370', 'Suture Cutgut Chromic (2/0) RN22230TH\r', 0),
(447, 1, 'MED', '371', 'Suture Cutgut Chromic (3/0) RN2325TF\r', 0),
(448, 1, 'MED', '372', 'Suture Cutgut Plain (2/0) RN1230TF\r', 0),
(449, 1, 'MED', '375', 'Suture PGA (3/0) RN3330TF\r', 0),
(450, 1, 'MED', '373', 'Suture Silk (1) S595\r', 0),
(451, 1, 'MED', '374', 'Suture Silk (2/0) RN5230TF\r', 0),
(452, 1, 'MED', '91', 'Suxamethonium 500mg Vial\r', 0),
(453, 1, 'MED', '92', 'Suxamethonium 500mg/ml 2ml Amp\r', 0),
(454, 1, 'MED', '247', 'Syringe Feeding/Irrigation 50/60ml\r', 0),
(455, 1, 'MED', '249', 'Syringe Luer 10ml With Needle Disposable\r', 0),
(456, 1, 'MED', '250', 'Syringe Luer 20ml With Needle Disposable\r', 0),
(457, 1, 'MED', '248', 'Syringe Luer 2ml With Needle Disposable\r', 0),
(458, 1, 'MED', '251', 'Syringe Luer 5ml With Needle Disposable\r', 0),
(459, 1, 'MED', '341', 'Tablet Counting Tray\r', 0),
(460, 1, 'MED', '93', 'tetanus Antitoxin 1500 IU 1ml Amp\r', 0),
(461, 1, 'MED', '196', 'Tetracycline eye ointment 1% 3.5g\r', 0),
(462, 1, 'MED', '345', 'Thermometer Clinical Flat Type\r', 0),
(463, 1, 'MED', '346', 'Thermometer Clinical Prismatic Type\r', 0),
(464, 1, 'MED', '94', 'Thiopental Sodium 500mg Vial\r', 0),
(465, 1, 'MED', '342', 'Toilet Paper Rolls\r', 0),
(466, 1, 'MED', '171', 'Tolbutamide 500mg tab\r', 0),
(467, 1, 'MED', '304', 'Tongue depressor Disposable\r', 0),
(468, 1, 'MED', '344', 'Traction Kit Adult\r', 0),
(469, 1, 'MED', '343', 'Traction Kit Children\r', 0),
(470, 1, 'MED', '25', 'Trichloro acetic Acid\r', 0),
(471, 1, 'MED', '28', 'Trisodium Citrate\r', 0),
(472, 1, 'MED', '347', 'Umbilical Cord Tie non sterile 100m\r', 0),
(473, 1, 'MED', '348', 'Umbilical Cord Tie sterile 22m\r', 0),
(474, 1, 'MED', '122', 'UREA Calorimetric 300 Tests\r', 0),
(475, 1, 'MED', '349', 'Urinal 1Ltr / 2Ltr\r', 0),
(476, 1, 'MED', '350', 'Urine Collecting Bag sterile 2Ltr\r', 0),
(477, 1, 'MED', '198', 'Urine Test Strips 3 Parameters 100 tests\r', 0),
(478, 1, 'MED', '172', 'Vitamin A 200.000 IU Caps\r', 0),
(479, 1, 'MED', '173', 'Vitamin B Complex Tab\r', 0),
(480, 1, 'MED', '95', 'Water for Injection 10ml Vial\r', 0),
(481, 1, 'MED', '377', 'X-Ray Developer 2.6kg for 22.5Ltr\r', 0),
(482, 1, 'MED', '378', 'X-Ray Film 18x24cm\r', 0),
(483, 1, 'MED', '379', 'X-Ray Film 20x40cm\r', 0),
(484, 1, 'MED', '380', 'X-Ray Film 24x30cm\r', 0),
(485, 1, 'MED', '381', 'X-Ray Film 30x40cm\r', 0),
(486, 1, 'MED', '382', 'X-Ray Film 35x35cm\r', 0),
(487, 1, 'MED', '383', 'X-Ray Film 43x35cm\r', 0),
(488, 1, 'MED', '384', 'X-Ray Film Cassette  18x24cm with screen\r', 0),
(489, 1, 'MED', '385', 'X-Ray Film Cassette  24x30cm with screen\r', 0),
(490, 1, 'MED', '386', 'X-Ray Film Cassette  30x40cm with screen\r', 0),
(491, 1, 'MED', '387', 'X-Ray Film Cassette  35x35cm with screen\r', 0),
(492, 1, 'MED', '388', 'X-Ray Film Cassette  35x43cm with screen\r', 0),
(493, 1, 'MED', '389', 'X-Ray Fixer 3.3kg for 22.5 Ltr\r', 0),
(494, 1, 'MED', '19', 'Xylene 2.5 ltrs\r', 0),
(495, 1, 'OTH', '1', 'Amount per day', 0);

-- --------------------------------------------------------

--
-- Table structure for table `PRICESOTHERS`
--

CREATE TABLE `PRICESOTHERS` (
  `OTH_ID` int(11) NOT NULL,
  `OTH_CODE` varchar(10) NOT NULL,
  `OTH_DESC` varchar(100) NOT NULL,
  `OTH_OPD_INCLUDE` int(11) NOT NULL DEFAULT '0',
  `OTH_IPD_INCLUDE` int(11) NOT NULL DEFAULT '0',
  `OTH_DAILY` int(11) NOT NULL DEFAULT '0',
  `OTH_DISCHARGE` int(11) DEFAULT '0',
  `OTH_UNDEFINED` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PRICESOTHERS`
--

INSERT INTO `PRICESOTHERS` (`OTH_ID`, `OTH_CODE`, `OTH_DESC`, `OTH_OPD_INCLUDE`, `OTH_IPD_INCLUDE`, `OTH_DAILY`, `OTH_DISCHARGE`, `OTH_UNDEFINED`) VALUES
(1, 'OTH001', 'Amount per day', 0, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `SMS`
--

CREATE TABLE `SMS` (
  `SMS_ID` int(11) NOT NULL,
  `SMS_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SMS_DATE_SCHED` datetime NOT NULL,
  `SMS_NUMBER` varchar(45) NOT NULL,
  `SMS_TEXT` varchar(160) NOT NULL,
  `SMS_DATE_SENT` datetime DEFAULT NULL,
  `SMS_USER` varchar(50) NOT NULL DEFAULT 'admin',
  `SMS_MOD` varchar(45) NOT NULL DEFAULT 'smsmanager',
  `SMS_MOD_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `SUPPLIER`
--

CREATE TABLE `SUPPLIER` (
  `SUP_ID` int(11) NOT NULL,
  `SUP_NAME` varchar(100) NOT NULL,
  `SUP_ADDRESS` varchar(150) DEFAULT NULL,
  `SUP_TAXCODE` varchar(50) DEFAULT NULL,
  `SUP_PHONE` varchar(20) DEFAULT NULL,
  `SUP_FAX` varchar(20) DEFAULT NULL,
  `SUP_EMAIL` varchar(100) DEFAULT NULL,
  `SUP_NOTE` varchar(200) DEFAULT NULL,
  `SUP_DELETED` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `THERAPIES`
--

CREATE TABLE `THERAPIES` (
  `THR_ID` int(11) NOT NULL,
  `THR_PAT_ID` int(11) NOT NULL,
  `THR_STARTDATE` datetime NOT NULL,
  `THR_ENDDATE` datetime NOT NULL,
  `THR_MDSR_ID` int(11) NOT NULL,
  `THR_QTY` double NOT NULL,
  `THR_UNT_ID` int(11) NOT NULL,
  `THR_FREQINDAY` int(11) NOT NULL,
  `THR_FREQINPRD` int(11) NOT NULL,
  `THR_NOTE` text,
  `THR_NOTIFY` tinyint(1) NOT NULL DEFAULT '0',
  `THR_SMS` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE `USER` (
  `US_ID_A` varchar(50) NOT NULL DEFAULT '',
  `US_UG_ID_A` varchar(50) NOT NULL DEFAULT '',
  `US_PASSWD` varchar(60) NOT NULL DEFAULT '',
  `US_DESC` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USER`
--

INSERT INTO `USER` (`US_ID_A`, `US_UG_ID_A`, `US_PASSWD`, `US_DESC`) VALUES
('admin', 'admin', '$2a$10$FI/PMO0oSHHosF2PX8l3QuB0DJepVfnynbLZ9Zm2711bF2ch8db2S', 'administrator'),
('guest', 'guest', '$2a$10$b0WlANdaNV7Ukn/klFGt3.euZ7PaHuJI6TtBSM2vdxkavvkUDbpo2', 'guest');

-- --------------------------------------------------------

--
-- Table structure for table `USERGROUP`
--

CREATE TABLE `USERGROUP` (
  `UG_ID_A` varchar(50) NOT NULL DEFAULT '',
  `UG_DESC` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `USERGROUP`
--

INSERT INTO `USERGROUP` (`UG_ID_A`, `UG_DESC`) VALUES
('admin', 'User with all the privileges'),
('guest', 'Read Only Users');

-- --------------------------------------------------------

--
-- Table structure for table `VACCINE`
--

CREATE TABLE `VACCINE` (
  `VAC_ID_A` varchar(10) NOT NULL,
  `VAC_DESC` varchar(50) NOT NULL,
  `VAC_VACT_ID_A` char(1) NOT NULL,
  `VAC_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `VACCINE`
--

INSERT INTO `VACCINE` (`VAC_ID_A`, `VAC_DESC`, `VAC_VACT_ID_A`, `VAC_LOCK`) VALUES
('1', 'BCG', 'C', 0),
('10', 'TT VACCINE DOSE 1', 'P', 0),
('11', 'TT VACCINE DOSE 2', 'P', 0),
('12', 'TT VACCINE DOSE 3', 'P', 0),
('13', 'TT VACCINE DOSE 4', 'P', 0),
('14', 'TT VACCINE DOSE 5', 'P', 0),
('15', 'TT VACCINE DOSE 2', 'N', 0),
('16', 'TT VACCINE DOSE 3', 'N', 0),
('17', 'TT VACCINE DOSE 4', 'N', 0),
('18', 'TT VACCINE DOSE 5', 'N', 0),
('2', 'POLIO 0 C', 'C', 0),
('3', 'POLIO 1 C', 'C', 0),
('4', 'POLIO 2 C', 'C', 0),
('5', 'POLIO 3 C', 'C', 0),
('6', 'DPT 1 - HepB + Hib 1', 'C', 0),
('7', 'DPT 2 - HepB + Hib 1', 'C', 0),
('8', 'DPT 3 - HepB + Hib 1', 'C', 0),
('9', 'MEASLES', 'C', 0);

-- --------------------------------------------------------

--
-- Table structure for table `VACCINETYPE`
--

CREATE TABLE `VACCINETYPE` (
  `VACT_ID_A` char(1) NOT NULL,
  `VACT_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `VACCINETYPE`
--

INSERT INTO `VACCINETYPE` (`VACT_ID_A`, `VACT_DESC`) VALUES
('C', 'Child\r'),
('N', 'No pregnant\r'),
('P', 'Pregnant\r');

-- --------------------------------------------------------

--
-- Table structure for table `VERSION`
--

CREATE TABLE `VERSION` (
  `VER_MAJOR` int(11) NOT NULL,
  `VER_MINOR` int(11) NOT NULL,
  `VER_SOURCE` longblob,
  `VER_DATE` datetime NOT NULL,
  `VER_CURRENT` char(1) NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `VISITS`
--

CREATE TABLE `VISITS` (
  `VST_ID` int(11) NOT NULL,
  `VST_PAT_ID` int(11) NOT NULL,
  `VST_DATE` datetime NOT NULL,
  `VST_NOTE` text,
  `VST_SMS` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `WARD`
--

CREATE TABLE `WARD` (
  `WRD_ID_A` char(1) NOT NULL,
  `WRD_NAME` varchar(50) NOT NULL,
  `WRD_TELE` varchar(50) DEFAULT NULL,
  `WRD_FAX` varchar(50) DEFAULT NULL,
  `WRD_EMAIL` varchar(50) DEFAULT NULL,
  `WRD_NBEDS` int(11) NOT NULL,
  `WRD_NQUA_NURS` int(11) NOT NULL,
  `WRD_NDOC` int(11) NOT NULL,
  `WRD_IS_PHARMACY` tinyint(1) NOT NULL DEFAULT '1',
  `WRD_IS_MALE` tinyint(1) NOT NULL DEFAULT '1',
  `WRD_IS_FEMALE` tinyint(1) NOT NULL DEFAULT '1',
  `WRD_LOCK` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `WARD`
--

INSERT INTO `WARD` (`WRD_ID_A`, `WRD_NAME`, `WRD_TELE`, `WRD_FAX`, `WRD_EMAIL`, `WRD_NBEDS`, `WRD_NQUA_NURS`, `WRD_NDOC`, `WRD_IS_PHARMACY`, `WRD_IS_MALE`, `WRD_IS_FEMALE`, `WRD_LOCK`) VALUES
('I', 'INTERNAL MEDICINE', '234/52544', '54324/5424', 'internal.medicine@stluke.org', 20, 3, 2, 1, 1, 1, 1),
('M', 'MATERNITY', '234/52544', '54324/5424', 'maternity@stluke.org', 20, 3, 2, 1, 0, 1, 1),
('N', 'NURSERY', '234/52544', '54324/5424', 'nursery@stluke.org', 20, 3, 2, 1, 1, 1, 1),
('S', 'SURGERY', '234/52544', '54324/5424', 'surgery@stluke.org', 20, 3, 2, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ADMISSION`
--
ALTER TABLE `ADMISSION`
  ADD PRIMARY KEY (`ADM_ID`),
  ADD KEY `FK_ADMISSION_DISCHARGETYPE` (`ADM_DIST_ID_A`),
  ADD KEY `FK_ADMISSION_DELIVERYTYPE` (`ADM_PRG_DLT_ID_A`),
  ADD KEY `FK_ADMISSION_DELIVERYRESULTTYPE` (`ADM_PRG_DRT_ID_A`),
  ADD KEY `FK_ADMISSION_ADMISSIONTYPE` (`ADM_ADMT_ID_A_ADM`),
  ADD KEY `FK_ADMISSION_OPERATION` (`ADM_OPE_ID_A`),
  ADD KEY `FK_ADMISSION_WARD` (`ADM_WRD_ID_A`),
  ADD KEY `FK_ADMISSION_PREGNANTTREATMENTTYPE` (`ADM_PRG_PTT_ID_A`),
  ADD KEY `FK_ADMISSION_IN_DISEASE` (`ADM_IN_DIS_ID_A`),
  ADD KEY `FK_ADMISSION_OUT_DISEASE1` (`ADM_OUT_DIS_ID_A`),
  ADD KEY `FK_ADMISSION_OUT_DISEASE2` (`ADM_OUT_DIS_ID_A_2`),
  ADD KEY `FK_ADMISSION_OUT_DISEASE3` (`ADM_OUT_DIS_ID_A_3`),
  ADD KEY `FK_ADMISSION_PATIENT` (`ADM_PAT_ID`);

--
-- Indexes for table `ADMISSIONTYPE`
--
ALTER TABLE `ADMISSIONTYPE`
  ADD PRIMARY KEY (`ADMT_ID_A`);

--
-- Indexes for table `AGETYPE`
--
ALTER TABLE `AGETYPE`
  ADD PRIMARY KEY (`AT_CODE`);

--
-- Indexes for table `BILLITEMS`
--
ALTER TABLE `BILLITEMS`
  ADD PRIMARY KEY (`BLI_ID`),
  ADD KEY `FK_BILLITEMS_BILLS` (`BLI_ID_BILL`);

--
-- Indexes for table `BILLPAYMENTS`
--
ALTER TABLE `BILLPAYMENTS`
  ADD PRIMARY KEY (`BLP_ID`),
  ADD KEY `FK_BILLPAYMENTS_BILLS` (`BLP_ID_BILL`);

--
-- Indexes for table `BILLS`
--
ALTER TABLE `BILLS`
  ADD PRIMARY KEY (`BLL_ID`),
  ADD KEY `FK_BILLS_PATIENT` (`BLL_ID_PAT`),
  ADD KEY `FK_BILLS_PRICELISTS` (`BLL_ID_LST`);

--
-- Indexes for table `DELIVERYRESULTTYPE`
--
ALTER TABLE `DELIVERYRESULTTYPE`
  ADD PRIMARY KEY (`DRT_ID_A`);

--
-- Indexes for table `DELIVERYTYPE`
--
ALTER TABLE `DELIVERYTYPE`
  ADD PRIMARY KEY (`DLT_ID_A`);

--
-- Indexes for table `DICOM`
--
ALTER TABLE `DICOM`
  ADD PRIMARY KEY (`DM_FILE_ID`);

--
-- Indexes for table `DISCHARGETYPE`
--
ALTER TABLE `DISCHARGETYPE`
  ADD PRIMARY KEY (`DIST_ID_A`);

--
-- Indexes for table `DISEASE`
--
ALTER TABLE `DISEASE`
  ADD PRIMARY KEY (`DIS_ID_A`),
  ADD KEY `FK_DISEASE_DISEASETYPE` (`DIS_DCL_ID_A`);

--
-- Indexes for table `DISEASETYPE`
--
ALTER TABLE `DISEASETYPE`
  ADD PRIMARY KEY (`DCL_ID_A`);

--
-- Indexes for table `EXAM`
--
ALTER TABLE `EXAM`
  ADD PRIMARY KEY (`EXA_ID_A`),
  ADD KEY `FK_EXAM_EXAMTYPE` (`EXA_EXC_ID_A`);

--
-- Indexes for table `EXAMROW`
--
ALTER TABLE `EXAMROW`
  ADD PRIMARY KEY (`EXR_ID`),
  ADD KEY `FK_EXAMROW_EXAM` (`EXR_EXA_ID_A`);

--
-- Indexes for table `EXAMTYPE`
--
ALTER TABLE `EXAMTYPE`
  ADD PRIMARY KEY (`EXC_ID_A`);

--
-- Indexes for table `GROUPMENU`
--
ALTER TABLE `GROUPMENU`
  ADD PRIMARY KEY (`GM_ID`);

--
-- Indexes for table `HELP`
--
ALTER TABLE `HELP`
  ADD PRIMARY KEY (`HL_ID`);

--
-- Indexes for table `HOSPITAL`
--
ALTER TABLE `HOSPITAL`
  ADD PRIMARY KEY (`HOS_ID_A`);

--
-- Indexes for table `LABORATORY`
--
ALTER TABLE `LABORATORY`
  ADD PRIMARY KEY (`LAB_ID`),
  ADD KEY `FK_LABORATORY_EXAM` (`LAB_EXA_ID_A`),
  ADD KEY `FK_LABORATORY_PATIENT` (`LAB_PAT_ID`);

--
-- Indexes for table `LABORATORYROW`
--
ALTER TABLE `LABORATORYROW`
  ADD PRIMARY KEY (`LABR_ID`),
  ADD KEY `FK_LABORATORYROW_LABORATORY` (`LABR_LAB_ID`);

--
-- Indexes for table `LOG`
--
ALTER TABLE `LOG`
  ADD PRIMARY KEY (`LOG_ID`);

--
-- Indexes for table `MALNUTRITIONCONTROL`
--
ALTER TABLE `MALNUTRITIONCONTROL`
  ADD PRIMARY KEY (`MLN_ID`);

--
-- Indexes for table `MEDICALDSR`
--
ALTER TABLE `MEDICALDSR`
  ADD PRIMARY KEY (`MDSR_ID`),
  ADD UNIQUE KEY `MDSR_MDSRT_ID_A` (`MDSR_MDSRT_ID_A`,`MDSR_DESC`);

--
-- Indexes for table `MEDICALDSRLOT`
--
ALTER TABLE `MEDICALDSRLOT`
  ADD PRIMARY KEY (`LT_ID_A`);

--
-- Indexes for table `MEDICALDSRSTOCKMOV`
--
ALTER TABLE `MEDICALDSRSTOCKMOV`
  ADD PRIMARY KEY (`MMV_ID`),
  ADD KEY `FK_MEDICALDSRSTOCKMOV_MEDICALDSR` (`MMV_MDSR_ID`),
  ADD KEY `FK_MEDICALDSRSTOCKMOV_MEDICALDSRSTOCKMOVTYPE` (`MMV_MMVT_ID_A`),
  ADD KEY `FK_MEDICALDSRSTOCKMOV_WARD` (`MMV_WRD_ID_A`);

--
-- Indexes for table `MEDICALDSRSTOCKMOVTYPE`
--
ALTER TABLE `MEDICALDSRSTOCKMOVTYPE`
  ADD PRIMARY KEY (`MMVT_ID_A`);

--
-- Indexes for table `MEDICALDSRSTOCKMOVWARD`
--
ALTER TABLE `MEDICALDSRSTOCKMOVWARD`
  ADD PRIMARY KEY (`MMVN_ID`) USING BTREE,
  ADD KEY `FK_MEDICALDSRSTOCKMOVWARD_WARD` (`MMVN_WRD_ID_A`),
  ADD KEY `FK_MEDICALDSRSTOCKMOVWARD_PATIENT` (`MMVN_PAT_ID`);

--
-- Indexes for table `MEDICALDSRTYPE`
--
ALTER TABLE `MEDICALDSRTYPE`
  ADD PRIMARY KEY (`MDSRT_ID_A`);

--
-- Indexes for table `MEDICALDSRWARD`
--
ALTER TABLE `MEDICALDSRWARD`
  ADD PRIMARY KEY (`MDSRWRD_WRD_ID_A`,`MDSRWRD_MDSR_ID`),
  ADD KEY `FK_MEDICALDSRWARD_MEDICALDSR` (`MDSRWRD_MDSR_ID`);

--
-- Indexes for table `MENUITEM`
--
ALTER TABLE `MENUITEM`
  ADD PRIMARY KEY (`MNI_ID_A`);

--
-- Indexes for table `OPD`
--
ALTER TABLE `OPD`
  ADD PRIMARY KEY (`OPD_ID`),
  ADD KEY `FK_OPD_DISEASE` (`OPD_DIS_ID_A`),
  ADD KEY `FK_OPD_DISEASE_2` (`OPD_DIS_ID_A_2`),
  ADD KEY `FK_OPD_DISEASE_3` (`OPD_DIS_ID_A_3`),
  ADD KEY `FK_OPD_PATIENT` (`OPD_PAT_ID`);

--
-- Indexes for table `OPERATION`
--
ALTER TABLE `OPERATION`
  ADD PRIMARY KEY (`OPE_ID_A`),
  ADD KEY `FK_OPERATION_OPERATIONTYPE` (`OPE_OCL_ID_A`);

--
-- Indexes for table `OPERATIONTYPE`
--
ALTER TABLE `OPERATIONTYPE`
  ADD PRIMARY KEY (`OCL_ID_A`);

--
-- Indexes for table `PATIENT`
--
ALTER TABLE `PATIENT`
  ADD PRIMARY KEY (`PAT_ID`);

--
-- Indexes for table `PATIENTEXAMINATION`
--
ALTER TABLE `PATIENTEXAMINATION`
  ADD PRIMARY KEY (`PEX_ID`),
  ADD KEY `PEX_PAT_ID` (`PEX_PAT_ID`);

--
-- Indexes for table `PATIENTVACCINE`
--
ALTER TABLE `PATIENTVACCINE`
  ADD PRIMARY KEY (`PAV_ID`),
  ADD KEY `FK_PATIENTVACCINE_PATIENT` (`PAV_PAT_ID`),
  ADD KEY `FK_PATIENTVACCINE_VACCINE` (`PAV_VAC_ID_A`);

--
-- Indexes for table `PREGNANTTREATMENTTYPE`
--
ALTER TABLE `PREGNANTTREATMENTTYPE`
  ADD PRIMARY KEY (`PTT_ID_A`);

--
-- Indexes for table `PRICELISTS`
--
ALTER TABLE `PRICELISTS`
  ADD PRIMARY KEY (`LST_ID`);

--
-- Indexes for table `PRICES`
--
ALTER TABLE `PRICES`
  ADD PRIMARY KEY (`PRC_ID`),
  ADD KEY `FK_PRICES_PRICELISTS` (`PRC_LST_ID`);

--
-- Indexes for table `PRICESOTHERS`
--
ALTER TABLE `PRICESOTHERS`
  ADD PRIMARY KEY (`OTH_ID`);

--
-- Indexes for table `SMS`
--
ALTER TABLE `SMS`
  ADD PRIMARY KEY (`SMS_ID`);

--
-- Indexes for table `SUPPLIER`
--
ALTER TABLE `SUPPLIER`
  ADD PRIMARY KEY (`SUP_ID`);

--
-- Indexes for table `THERAPIES`
--
ALTER TABLE `THERAPIES`
  ADD PRIMARY KEY (`THR_ID`),
  ADD KEY `FK_THERAPIES_PATIENT` (`THR_PAT_ID`),
  ADD KEY `FK_THERAPIES_MDSR` (`THR_MDSR_ID`);

--
-- Indexes for table `USER`
--
ALTER TABLE `USER`
  ADD PRIMARY KEY (`US_ID_A`),
  ADD KEY `FK_USER_USERGROUP` (`US_UG_ID_A`);

--
-- Indexes for table `USERGROUP`
--
ALTER TABLE `USERGROUP`
  ADD PRIMARY KEY (`UG_ID_A`);

--
-- Indexes for table `VACCINE`
--
ALTER TABLE `VACCINE`
  ADD PRIMARY KEY (`VAC_ID_A`),
  ADD KEY `FK_VACCINE_VACCINETYPE` (`VAC_VACT_ID_A`);

--
-- Indexes for table `VACCINETYPE`
--
ALTER TABLE `VACCINETYPE`
  ADD PRIMARY KEY (`VACT_ID_A`);

--
-- Indexes for table `VERSION`
--
ALTER TABLE `VERSION`
  ADD PRIMARY KEY (`VER_MAJOR`,`VER_MINOR`);

--
-- Indexes for table `VISITS`
--
ALTER TABLE `VISITS`
  ADD PRIMARY KEY (`VST_ID`),
  ADD KEY `FK_VISITS_PATIENT` (`VST_PAT_ID`);

--
-- Indexes for table `WARD`
--
ALTER TABLE `WARD`
  ADD PRIMARY KEY (`WRD_ID_A`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ADMISSION`
--
ALTER TABLE `ADMISSION`
  MODIFY `ADM_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `BILLITEMS`
--
ALTER TABLE `BILLITEMS`
  MODIFY `BLI_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `BILLPAYMENTS`
--
ALTER TABLE `BILLPAYMENTS`
  MODIFY `BLP_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `BILLS`
--
ALTER TABLE `BILLS`
  MODIFY `BLL_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `DICOM`
--
ALTER TABLE `DICOM`
  MODIFY `DM_FILE_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `EXAMROW`
--
ALTER TABLE `EXAMROW`
  MODIFY `EXR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `GROUPMENU`
--
ALTER TABLE `GROUPMENU`
  MODIFY `GM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `HELP`
--
ALTER TABLE `HELP`
  MODIFY `HL_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `LABORATORY`
--
ALTER TABLE `LABORATORY`
  MODIFY `LAB_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `LABORATORYROW`
--
ALTER TABLE `LABORATORYROW`
  MODIFY `LABR_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `LOG`
--
ALTER TABLE `LOG`
  MODIFY `LOG_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MALNUTRITIONCONTROL`
--
ALTER TABLE `MALNUTRITIONCONTROL`
  MODIFY `MLN_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MEDICALDSR`
--
ALTER TABLE `MEDICALDSR`
  MODIFY `MDSR_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;
--
-- AUTO_INCREMENT for table `MEDICALDSRSTOCKMOV`
--
ALTER TABLE `MEDICALDSRSTOCKMOV`
  MODIFY `MMV_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MEDICALDSRSTOCKMOVWARD`
--
ALTER TABLE `MEDICALDSRSTOCKMOVWARD`
  MODIFY `MMVN_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `OPD`
--
ALTER TABLE `OPD`
  MODIFY `OPD_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PATIENT`
--
ALTER TABLE `PATIENT`
  MODIFY `PAT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `PATIENTEXAMINATION`
--
ALTER TABLE `PATIENTEXAMINATION`
  MODIFY `PEX_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PATIENTVACCINE`
--
ALTER TABLE `PATIENTVACCINE`
  MODIFY `PAV_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PRICELISTS`
--
ALTER TABLE `PRICELISTS`
  MODIFY `LST_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `PRICES`
--
ALTER TABLE `PRICES`
  MODIFY `PRC_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=496;
--
-- AUTO_INCREMENT for table `PRICESOTHERS`
--
ALTER TABLE `PRICESOTHERS`
  MODIFY `OTH_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `SMS`
--
ALTER TABLE `SMS`
  MODIFY `SMS_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SUPPLIER`
--
ALTER TABLE `SUPPLIER`
  MODIFY `SUP_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `THERAPIES`
--
ALTER TABLE `THERAPIES`
  MODIFY `THR_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `VISITS`
--
ALTER TABLE `VISITS`
  MODIFY `VST_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ADMISSION`
--
ALTER TABLE `ADMISSION`
  ADD CONSTRAINT `FK_ADMISSION_ADMISSIONTYPE` FOREIGN KEY (`ADM_ADMT_ID_A_ADM`) REFERENCES `ADMISSIONTYPE` (`ADMT_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_DELIVERYRESULTTYPE` FOREIGN KEY (`ADM_PRG_DRT_ID_A`) REFERENCES `DELIVERYRESULTTYPE` (`DRT_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_DELIVERYTYPE` FOREIGN KEY (`ADM_PRG_DLT_ID_A`) REFERENCES `DELIVERYTYPE` (`DLT_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_DISCHARGETYPE` FOREIGN KEY (`ADM_DIST_ID_A`) REFERENCES `DISCHARGETYPE` (`DIST_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_IN_DISEASE` FOREIGN KEY (`ADM_IN_DIS_ID_A`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_OPERATION` FOREIGN KEY (`ADM_OPE_ID_A`) REFERENCES `OPERATION` (`OPE_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_OUT_DISEASE1` FOREIGN KEY (`ADM_OUT_DIS_ID_A`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_OUT_DISEASE2` FOREIGN KEY (`ADM_OUT_DIS_ID_A_2`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_OUT_DISEASE3` FOREIGN KEY (`ADM_OUT_DIS_ID_A_3`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_PATIENT` FOREIGN KEY (`ADM_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ADMISSION_PREGNANTTREATMENTTYPE` FOREIGN KEY (`ADM_PRG_PTT_ID_A`) REFERENCES `PREGNANTTREATMENTTYPE` (`PTT_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ADMISSION_WARD` FOREIGN KEY (`ADM_WRD_ID_A`) REFERENCES `WARD` (`WRD_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `BILLITEMS`
--
ALTER TABLE `BILLITEMS`
  ADD CONSTRAINT `FK_BILLITEMS_BILLS` FOREIGN KEY (`BLI_ID_BILL`) REFERENCES `BILLS` (`BLL_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `BILLPAYMENTS`
--
ALTER TABLE `BILLPAYMENTS`
  ADD CONSTRAINT `FK_BILLPAYMENTS_BILLS` FOREIGN KEY (`BLP_ID_BILL`) REFERENCES `BILLS` (`BLL_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `BILLS`
--
ALTER TABLE `BILLS`
  ADD CONSTRAINT `FK_BILLS_PATIENT` FOREIGN KEY (`BLL_ID_PAT`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BILLS_PRICELISTS` FOREIGN KEY (`BLL_ID_LST`) REFERENCES `PRICELISTS` (`LST_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `DISEASE`
--
ALTER TABLE `DISEASE`
  ADD CONSTRAINT `FK_DISEASE_DISEASETYPE` FOREIGN KEY (`DIS_DCL_ID_A`) REFERENCES `DISEASETYPE` (`DCL_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `EXAM`
--
ALTER TABLE `EXAM`
  ADD CONSTRAINT `FK_EXAM_EXAMTYPE` FOREIGN KEY (`EXA_EXC_ID_A`) REFERENCES `EXAMTYPE` (`EXC_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `EXAMROW`
--
ALTER TABLE `EXAMROW`
  ADD CONSTRAINT `FK_EXAMROW_EXAM` FOREIGN KEY (`EXR_EXA_ID_A`) REFERENCES `EXAM` (`EXA_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `LABORATORY`
--
ALTER TABLE `LABORATORY`
  ADD CONSTRAINT `FK_LABORATORY_EXAM` FOREIGN KEY (`LAB_EXA_ID_A`) REFERENCES `EXAM` (`EXA_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_LABORATORY_PATIENT` FOREIGN KEY (`LAB_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `LABORATORYROW`
--
ALTER TABLE `LABORATORYROW`
  ADD CONSTRAINT `FK_LABORATORYROW_LABORATORY` FOREIGN KEY (`LABR_LAB_ID`) REFERENCES `LABORATORY` (`LAB_ID`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `MEDICALDSR`
--
ALTER TABLE `MEDICALDSR`
  ADD CONSTRAINT `FK_MEDICALDSR_MEDICALDSRTYPE` FOREIGN KEY (`MDSR_MDSRT_ID_A`) REFERENCES `MEDICALDSRTYPE` (`MDSRT_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `MEDICALDSRSTOCKMOV`
--
ALTER TABLE `MEDICALDSRSTOCKMOV`
  ADD CONSTRAINT `FK_MEDICALDSRSTOCKMOV_MEDICALDSR` FOREIGN KEY (`MMV_MDSR_ID`) REFERENCES `MEDICALDSR` (`MDSR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_MEDICALDSRSTOCKMOV_MEDICALDSRSTOCKMOVTYPE` FOREIGN KEY (`MMV_MMVT_ID_A`) REFERENCES `MEDICALDSRSTOCKMOVTYPE` (`MMVT_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_MEDICALDSRSTOCKMOV_WARD` FOREIGN KEY (`MMV_WRD_ID_A`) REFERENCES `WARD` (`WRD_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MEDICALDSRSTOCKMOVWARD`
--
ALTER TABLE `MEDICALDSRSTOCKMOVWARD`
  ADD CONSTRAINT `FK_MEDICALDSRSTOCKMOVWARD_PATIENT` FOREIGN KEY (`MMVN_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MEDICALDSRSTOCKMOVWARD_WARD` FOREIGN KEY (`MMVN_WRD_ID_A`) REFERENCES `WARD` (`WRD_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MEDICALDSRWARD`
--
ALTER TABLE `MEDICALDSRWARD`
  ADD CONSTRAINT `FK_MEDICALDSRWARD_MEDICALDSR` FOREIGN KEY (`MDSRWRD_MDSR_ID`) REFERENCES `MEDICALDSR` (`MDSR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_MEDICALDSRWARD_WARD` FOREIGN KEY (`MDSRWRD_WRD_ID_A`) REFERENCES `WARD` (`WRD_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `OPD`
--
ALTER TABLE `OPD`
  ADD CONSTRAINT `FK_OPD_DISEASE` FOREIGN KEY (`OPD_DIS_ID_A`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_OPD_DISEASE_2` FOREIGN KEY (`OPD_DIS_ID_A_2`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_OPD_DISEASE_3` FOREIGN KEY (`OPD_DIS_ID_A_3`) REFERENCES `DISEASE` (`DIS_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_OPD_PATIENT` FOREIGN KEY (`OPD_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `OPERATION`
--
ALTER TABLE `OPERATION`
  ADD CONSTRAINT `FK_OPERATION_OPERATIONTYPE` FOREIGN KEY (`OPE_OCL_ID_A`) REFERENCES `OPERATIONTYPE` (`OCL_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `PATIENTEXAMINATION`
--
ALTER TABLE `PATIENTEXAMINATION`
  ADD CONSTRAINT `FK_PATIENTEXAMINATION_PATIENT` FOREIGN KEY (`PEX_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `PATIENTVACCINE`
--
ALTER TABLE `PATIENTVACCINE`
  ADD CONSTRAINT `FK_PATIENTVACCINE_PATIENT` FOREIGN KEY (`PAV_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PATIENTVACCINE_VACCINE` FOREIGN KEY (`PAV_VAC_ID_A`) REFERENCES `VACCINE` (`VAC_ID_A`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `PRICES`
--
ALTER TABLE `PRICES`
  ADD CONSTRAINT `FK_PRICES_PRICELISTS` FOREIGN KEY (`PRC_LST_ID`) REFERENCES `PRICELISTS` (`LST_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `THERAPIES`
--
ALTER TABLE `THERAPIES`
  ADD CONSTRAINT `FK_THERAPIES_MDSR` FOREIGN KEY (`THR_MDSR_ID`) REFERENCES `MEDICALDSR` (`MDSR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_THERAPIES_PATIENT` FOREIGN KEY (`THR_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `USER`
--
ALTER TABLE `USER`
  ADD CONSTRAINT `FK_USER_USERGROUP` FOREIGN KEY (`US_UG_ID_A`) REFERENCES `USERGROUP` (`UG_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `VACCINE`
--
ALTER TABLE `VACCINE`
  ADD CONSTRAINT `FK_VACCINE_VACCINETYPE` FOREIGN KEY (`VAC_VACT_ID_A`) REFERENCES `VACCINETYPE` (`VACT_ID_A`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `VISITS`
--
ALTER TABLE `VISITS`
  ADD CONSTRAINT `FK_VISITS_PATIENT` FOREIGN KEY (`VST_PAT_ID`) REFERENCES `PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
